//
//  Path.swift
//  MinBar
//
//  Created by Nguyen Van Dung on 1/25/18.
//  Copyright © 2018 Pentor. All rights reserved.
//

import Foundation
let rootURL = "http://api.globalmsm.org.ng/api/v1/"

enum Path {

    case getInterest
    case setInterest(Int)
    case SignUpEmail
    case authen
    case industryLookup
    case countryLookup
    case stateLookup
    case yellowPageCategoryLookup
    case suggestionNetwork
    case memberSlim(Int)
    case updateProfile
    case feedList
    case feedComments(Int)
    case feedCommentsAll(Int)
    case forgotPassword
    case Network(Int)
    case networkRequest(Int)
    case networkInvitation
    case fullProfile(requestId: Int, otherId: Int)
    case updateNetworkInvitation(networkId: Int, option: String, memberId: Int)
    case feedLike(feedId: Int)
    case fetchOffers(memberID: Int)
    case storeDeviceToken
    case getAmount
    case initialiseTransaction
    case verityTransaction(referenceCode: String, email: String)
    case getPublicKey
    case event
    case addEventToCalendar
    case getYellowPagesSearch(searchText: String)
    case getYellowPages
    case createPost
    case searchMember(memId: Int, text: String, industry: String?)
    case getYellowPage(id: Int)
    case addExperience(id: Int)
    case addSkills(id: Int)
    case welcome
    case eventDetail(String)
    case offerDetail(String)
    case filterYellowPage
    case pnaDaily
    case initializeEventTicketTransaction
    case initializeEventTableTransaction
    case verifyEventTicketTransaction(referenceCode: String, email: String)
    case verifyEventTableTransaction(referenceCode: String, email: String)
    case redeem(offerId: String)

    //member/{userId}/industry/interest

    var relativePath2: String {
        switch self {
        case .getInterest:
            return "lookup/industry/all"
        case .setInterest(let userid):
            return "member/\(userid)/industry/interest"
        case .fetchOffers(let memberID):
            return "offer?memberid-id=\(memberID)&status=active"
        case .feedCommentsAll(let id):
            return "member/feed/\(id)/comment/all"
        case .feedComments(let id):
            return "member/feed/\(id)/comment"
        case .feedList:
            return "member/feed/stripped-content"
        case .updateProfile:
            return "member/profile"
        case .SignUpEmail:
            return "member"
        case .authen:
            return "authenticate"
        case .industryLookup:
            return "lookup/industry/all"
        case .countryLookup:
            return "lookup/country/all"
        case .stateLookup:
            return "lookup/states/"
        case .suggestionNetwork:
            return "member/network/suggestion"
        case .memberSlim(let memberId):
            return "member/slim?id=\(memberId)"
        case .forgotPassword:
            return "member/reset-password"
        case .Network(let memberId):
            return "member/\(memberId)/network"
        case .networkRequest(let memberId):
            return "member/\(memberId)/network/request"
        case .networkInvitation:
            return  "member/network"
        case .fullProfile(let requestId, let otherId):
            return "member/\(requestId)/network/profile/full/\(otherId)"
        case .updateNetworkInvitation(let networkId, let option, let memberId):
            return "member/network/request/\(networkId)/\(option)?member-id=\(memberId)"
        case .feedLike(let feedId):
            return "member/feed/\(feedId)/like"
        case .storeDeviceToken:
            return "member/device-token"
        case .getAmount:
            return "premium-subscription/amount"
        case .initialiseTransaction:
            return "member/transaction/premium-membership-subscription"
        case .verityTransaction(let referenceCode, let email):
            return "member/transaction/premium-membership-subscription/\(referenceCode)?email=\(email)"
        case .getPublicKey:
            return "payments/pk"
        case .event:
            return "event"
        case .addEventToCalendar:
            return "event/rsvp"
        case .getYellowPagesSearch(let searchText):
            return "yellow-pages/search/\(searchText)"
        case .getYellowPages:
            return "yellow-pages"
        case .createPost:
            let memberID = AppDelegate.shared().loggedUser?.id ?? 0
            return "member/\(memberID)/feed"
        case .searchMember(let memId, let text, let industry):
            var tempUri = "member/\(memId)/search/\(text)"
            if let industry = industry {
                tempUri = "member/\(memId)/search/\(industry)"
            }
            return tempUri
        case .getYellowPage(let id):
            return "yellow-pages/\(id)"
        case .addExperience(let id):
            return "member/\(id)/experience"
        case .addSkills(let id):
            return "member/\(id)/skill"
        case .welcome:
            return "home-screen-content"
        case .eventDetail(let eventId):
            return "event/\(eventId)"
        case .offerDetail(let offerId):
            return "offer/\(offerId)"
        case .filterYellowPage:
            return "yellow-pages/filter"
        case .yellowPageCategoryLookup:
            return "yellow-pages/category/all"
        case .pnaDaily:
            return "pna-daily"
        case .initializeEventTicketTransaction:
            return "member/transaction/event-ticket"
        case .initializeEventTableTransaction:
            return "member/transaction/event-table"
        case .verifyEventTicketTransaction(let referenceCode, let email):
            return "member/transaction/event-ticket/\(referenceCode)?email=\(email)"
        case .verifyEventTableTransaction(let referenceCode, let email):
            return "member/transaction/event-table/\(referenceCode)?email=\(email)"
        case .redeem(let offerId):
            return "offer/\(offerId)/redeem"
        }
    }

    var path: String {
        let urlstr = (rootURL as NSString).appendingPathComponent(self.relativePath2)
        let url = URL(string: urlstr)
        return url?.absoluteString ?? ""
    }
}
