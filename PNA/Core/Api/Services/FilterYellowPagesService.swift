//
//  FilterYellowPagesService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class FilterYellowPagesService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var yellowPages = [YellowPageObject]()
        if let root = response as? [String : Any] {
            if let yellowDicts = root["yellowPages"] as? [[String : Any]] {
                for yellowPageDict in yellowDicts {
                    if let yellowPage = YellowPageObject(JSON: yellowPageDict) {
                        yellowPages.append(yellowPage)
                    }
                }
            }
        }
        super.onFinish(yellowPages, error: error, completion: completion)
    }
}
