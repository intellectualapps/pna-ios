//
//  CreatePostService.swift
//  PNA
//
//  Created by MTQ on 3/17/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class CreatePostService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var post = FeedInfo()
        if let root = response as? [String : Any] {
            if let feedDict = root["feed"] as? [String : Any] {
                post = FeedInfo(dict: feedDict)
            }
        }
        super.onFinish(post, error: error, completion: completion)
    }
}
