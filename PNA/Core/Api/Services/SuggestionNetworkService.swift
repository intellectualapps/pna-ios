//
//  SuggestionNetworkService.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SuggestionNetworkService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var suggestions = [SuggestionUser]()
        if let root = response as? [String: Any] {
            if let dicts = root["network_suggestions"] as? [[String: Any]] {
                for dict in dicts {
                    let user = SuggestionUser(dict: dict)
                    suggestions.append(user)
                }
            }
        }
        super.onFinish(suggestions, error: error, completion: completion)
    }
}
