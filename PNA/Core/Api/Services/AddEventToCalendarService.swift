//
//  AddEventToCalendarService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/13/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class AddEventToCalendarService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var responseEvent = Event()
        if let root = response as? [String : Any] {
            if let eventDict = root["event"] as? [String : Any] {
                if let event = Event(JSON: eventDict) {
                    responseEvent = event
                }
            }
        }
        super.onFinish(responseEvent, error: error, completion: completion)
    }
}
