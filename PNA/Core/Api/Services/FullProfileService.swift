//
//  FullProfileService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/3/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class FullProfileService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var user: User?
        if let root = response as? [String: Any] {
            if let dict = root["otherMemberProfile"] as? [String: Any] {
                user = User(dict: dict)
            }
        }
        super.onFinish(user, error: error, completion: completion)
    }
}
