//
//  SearchMemberService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/15/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SearchMemberService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var members = [SearchProfile]()
        if let root = response as? [String : Any] {
            if let memberDicts = root["members"] as? [[String : Any]] {
                for memberDict in memberDicts {
                    let member = SearchProfile(dict: memberDict)
                    members.append(member)
                }
            }
        }
        super.onFinish(members, error: error, completion: completion)
    }
}
