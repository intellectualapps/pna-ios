//
//  FetchOffersService.swift
//  PNA
//
//  Created by MTQ on 2/22/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class FetchOffersService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var offers = [Offer]()
        if let root = response as? [String: Any] {
            if let offersDict = root["offers"] as? [[String: Any]] {
                for offerDict in offersDict {
                    if let offerModel = Offer(JSON: offerDict) {
                        offers.append(offerModel)
                    }
                }
            }
        }
        super.onFinish(offers, error: error, completion: completion)
    }
}
