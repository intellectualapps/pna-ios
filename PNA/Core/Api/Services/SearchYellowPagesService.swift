//
//  YellowPagesService.swift
//  PNA
//
//  Created by hien.bui on 3/15/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SearchYellowPagesService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var yellowPages = [YellowPageObject]()
        if let root = response as? [String : Any] {
            if let eventDicts = root["yellowPages"] as? [[String : Any]] {
                for yellowPageDict in eventDicts {
                    if let yellowPage = YellowPageObject(JSON: yellowPageDict) {
                        yellowPages.append(yellowPage)
                    }
                }
            }
        }
        super.onFinish(yellowPages, error: error, completion: completion)
    }
}
