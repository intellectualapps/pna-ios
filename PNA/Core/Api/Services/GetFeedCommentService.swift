//
//  GetFeedCommentService.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class GetFeedCommentService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var comments = [FeedCommentInfo]()
        if let root = response as? [String: Any] {
            if let commentdicts = root["feedComments"] as? [[String: Any]] {
                for dict in commentdicts {
                    let info = FeedCommentInfo(dict: dict)
                    comments.append(info)
                }
            }
        }
        super.onFinish(comments, error: error, completion: completion)
    }
}
