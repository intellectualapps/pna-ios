//
//  StoreDeviceTokenService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/12/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class StoreDeviceTokenService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        let result = ApiResponseBasicModel()
        if let root = response as? [String : Any] {
            if let errorMsg = root.errorFromApiResponse() {
                result.message = errorMsg
                result.isError = true
            } else {
                print("success \(root)")
            }
        }
        super.onFinish(result, error: error, completion: completion)
    }
}
