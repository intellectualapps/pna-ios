//
//  ProfileDetailService.swift
//  PNA
//
//  Created by center12 on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class ProfileDetailService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var detailUsers: ProfileDetailUser?
        if let root = response as? [String: Any] {
            if let dict = root["otherMemberProfile"] as? [String: Any] {
                detailUsers = ProfileDetailUser(dict: dict)
            }
        }
        super.onFinish(detailUsers, error: error, completion: completion)
    }
}
