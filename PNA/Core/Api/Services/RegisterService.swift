//
//  RegisterService.swift
//  MinBar
//
//  Created by Admin on 1/20/18.
//  Copyright © 2018 Pentor. All rights reserved.
//

import Foundation

class RegisterService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        let result = ApiResponseBasicModel()
        if let root = response as? [String: Any] {
            if let erroMsg = root.errorFromApiResponse() {
                result.message = erroMsg
                result.isError = true
            } else {
                let token = root.stringOrEmptyForKey(key: "authToken")
                let user = User.convert(userJson: root)
                user.authToken = token
                AppDelegate.shared().loggedUser = user
                result.content = user
            }
        }
        super.onFinish(result, error: error, completion: completion)
    }
}
