//
//  VerifyUsernameService.swift
//  MinBar
//
//  Created by Admin on 2/3/18.
//  Copyright © 2018 Pentor. All rights reserved.
//

import Foundation

class UpdateProfileService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        let result = ApiResponseBasicModel()
        if let root = response as? [String: Any] {
            if let errorMessage = root.errorFromApiResponse() {
                result.message = errorMessage
                result.isError = true
            } else {
                let user  = User()
                if let userData = root["updatedMemberProfile"] as? [String: Any] {
                    user.parseContentFromDict(dict: userData)
                }
                user.authToken = AppDelegate.shared().loggedUser?.authToken
                AppDelegate.shared().loggedUser = user
                result.content = user
            }
        }
        super.onFinish(result, error: error, completion: completion)
    }
}


