//
//  GetInteresetService.swift
//  PNA
//
//  Created by Nguyen Van Dung on 3/15/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class GetInteresetService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var infos = [InterestInfo]()
        if let root = response as? [String: Any] {
            if let industries = root["industries"] as? [[String: Any]] {
                industries.forEach { (dict) in
                    let info = InterestInfo(dict: dict)
                    infos.append(info)
                }
            }
        }
        super.onFinish(infos, error: error, completion: completion)
    }

    class func getInterest(completion: @escaping NetworkServiceCompletion) {
        let path = Path.getInterest.path

        let service = GetInteresetService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
