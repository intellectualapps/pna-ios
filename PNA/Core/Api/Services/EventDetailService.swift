//
//  EventDetailService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class EventDetailService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var event: Event?
        if let root = response as? [String : Any] {
            if let eventDict = root["event"] as? [String : Any] {
                event = Event(JSON: eventDict)
            }
        }
        super.onFinish(event, error: error, completion: completion)
    }
}
