//
//  PostCommentService.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

class PostCommentService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var commentInfo: FeedCommentInfo?
        if let root = response as? [String: Any] {
            commentInfo = FeedCommentInfo()
            commentInfo?.userId = root.intForKey(key: "userId")
            commentInfo?.content = root.stringOrEmptyForKey(key: "comment")
            commentInfo?.feedId = root.intForKey(key: "feedId")
            commentInfo?.commentId = root.intForKey(key: "commentId")
        }
        super.onFinish(commentInfo, error: error, completion: completion)
    }

    class func postComment(comments: String, email: String, postid: Int, completion: @escaping NetworkServiceCompletion) {
        let path = Path.feedComments(postid).path
        let params = RequestParams()
        params.setValue(postid, forKey: "Feed-id")
        params.setValue(email, forKey: "email")
        params.setValue(comments, forKey: "comment")

        let service = PostCommentService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
