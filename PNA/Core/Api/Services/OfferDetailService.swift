//
//  OfferDetailService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class OfferDetailService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var offer: Offer?
        if let root = response as? [String : Any] {
            if let offerDict = root["offer"] as? [String : Any] {
                offer = Offer(JSON: offerDict)
            }
        }
        super.onFinish(offer, error: error, completion: completion)
    }
}
