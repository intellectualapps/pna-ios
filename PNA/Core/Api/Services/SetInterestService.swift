//
//  SetInterestService.swift
//  PNA
//
//  Created by Nguyen Van Dung on 3/15/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class SetInterestService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        super.onFinish(response, error: error, completion: completion)
    }

    class func setInterest(interestids: [String], completion: @escaping NetworkServiceCompletion) {
        let userid = AppDelegate.shared().loggedUser?.id ?? 0
        let path = Path.setInterest(userid).path
        let params = RequestParams()
        let str = interestids.joined(separator: ",")
        params.setValue(str, forKey: "industry-ids")

        let service = SetInterestService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
