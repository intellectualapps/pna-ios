//
//  WelcomeService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class WelcomeService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var pnaDaily: PNADaily = PNADaily()
        var event: Event = Event()
        var offer: Offer = Offer()
        var video: Video = Video()
        var welcomeUsers: [WelcomeUser] = []
        var welcomeObj: WelcomeObject?
        if let root = response as? [String: Any] {
            if let dict = root["pnaDaily"] as? [String: Any] {
                pnaDaily = PNADaily(JSON: dict) ?? PNADaily()
            }
            if let dict = root["event"] as? [String : Any] {
                event = Event(JSON: dict) ?? Event()
            }
            if let dict = root["offer"] as? [String : Any] {
                offer = Offer(JSON: dict) ?? Offer()
            }
            if let dict = root["video"] as? [String : Any] {
                video = Video(JSON: dict) ?? Video()
            }
            if let dicts = root["memberPhotos"] as? [[String : Any]] {
                for dict in dicts {
                    let user = WelcomeUser(dict: dict)
                    welcomeUsers.append(user)
                }
            }
            welcomeObj = WelcomeObject(pnaDaily: pnaDaily, event: event, offer: offer, video: video, welcomeUser: welcomeUsers)
        }
        super.onFinish(welcomeObj, error: error, completion: completion)
    }
}
