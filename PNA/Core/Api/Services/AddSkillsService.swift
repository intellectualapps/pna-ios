//
//  AddSkillsService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/2/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class AddSkillsService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        let result = ApiResponseBasicModel()
        if let root = response as? [String : Any] {
            result.message = root.stringForKey(key: "message") ?? ""
        }
        super.onFinish(result, error: error, completion: completion)
    }
}
