//
//  VerifyEventTableTransactionService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/13/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class VerifyEventTableTransactionService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var result: ApiResponseBasicModel?
        if error == nil {
            result = ApiResponseBasicModel()
        }
        super.onFinish(result, error: error, completion: completion)
    }
}
