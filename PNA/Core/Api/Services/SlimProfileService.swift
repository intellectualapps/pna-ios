//
//  SlimProfileService.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SlimProfileService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var slimuser: SlimProfile?
        if let root = response as? [String: Any] {
            slimuser = SlimProfile(dict: root)
        }
        super.onFinish(slimuser, error: error, completion: completion)
    }
}
