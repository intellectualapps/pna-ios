//
//  EventService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class EventService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var events = [Event]()
        if let root = response as? [String : Any] {
            if let eventDicts = root["events"] as? [[String : Any]] {
                for eventDict in eventDicts {
                    if let event = Event(JSON: eventDict) {
                        events.append(event)
                    }
                }
            }
        }
        super.onFinish(events, error: error, completion: completion)
    }
}
