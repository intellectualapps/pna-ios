//
//  GetFeedListService.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class GetFeedListService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var feeds = [FeedInfo]()
        if let root = response as? [String: Any] {
            if let feeddicts = root["feed"] as? [[String: Any]] {
                for dict in feeddicts {
                    let info = FeedInfo(dict: dict)
                    feeds.append(info)
                }
            }
        }
        super.onFinish(feeds, error: error, completion: completion)
    }
}
