//
//  NetworkInvitationService.swift
//  PNA
//
//  Created by center12 on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class NetworkInvitationService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var networkInvitationUsers = [NetworkUser]()
        if let root = response as? [String: Any] {
            if let dicts = root["networkInvitation"] as? [[String: Any]] {
                for dict in dicts {
                    let user = NetworkUser(dict: dict)
                    networkInvitationUsers.append(user)
                }
            }
        }
        super.onFinish(networkInvitationUsers, error: error, completion: completion)
    }
}
