//
//  UpdateNetworkInvitationService.swift
//  PNA
//
//  Created by center12 on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class UpdateNetworkInvitationService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var user: NetworkUser?
        if let root = response as? [String: Any] {
            if let dict = root["network"] as? [String: Any] {
                user = NetworkUser(dict: dict)
            } else {
                if let devMessage = root["developerMessage"] as? String {
                    error?.message = devMessage
                }
            }
        }
        super.onFinish(user, error: error, completion: completion)
    }
}
