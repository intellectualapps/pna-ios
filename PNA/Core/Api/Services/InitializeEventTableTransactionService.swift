//
//  InitializeEventTableTransactionService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/13/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class InitializeEventTableTransactionService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var transactionModel: Transaction?
        if let responseDict = response as? [String: Any] {
            if let transactionDict = responseDict["transaction"] as? [String: Any] {
                if let transaction = Transaction(JSON: transactionDict) {
                    transactionModel = transaction
                }
            }
        }
        super.onFinish(transactionModel, error: error, completion: completion)
    }
}
