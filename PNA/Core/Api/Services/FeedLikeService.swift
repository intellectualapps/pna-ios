//
//  FeedLikeService.swift
//  PNA
//
//  Created by Admin on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class FeedLikeService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var feed: FeedInfo?
        if let dict = response as? [String: Any] {
            feed = FeedInfo(dict: dict)
        }
        super.onFinish(feed, error: error, completion: completion)
    }
}
