//
//  NetworkRequestService.swift
//  PNA
//
//  Created by center12 on 2/22/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class NetworkRequestService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var networkRequestUsers = [NetworkUser]()
        if let root = response as? [String: Any] {
            if let dicts = root["network"] as? [[String: Any]] {
                for dict in dicts {
                    let user = NetworkUser(dict: dict)
                    networkRequestUsers.append(user)
                }
            }
        }
        super.onFinish(networkRequestUsers, error: error, completion: completion)
    }
}
