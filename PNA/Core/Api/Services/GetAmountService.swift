//
//  GetAmountService.swift
//  PNA
//
//  Created by hien.bui on 3/5/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class GetAmountService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var amountToString = ""
        if let responseDict = response as? [String: Any] {
            if let amount = responseDict["amount"] as? Int {
                amountToString = amountToComma(amount)
            }
        }
        super.onFinish(amountToString, error: error, completion: completion)
    }
    
    private func amountToComma(_ amount: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSize = 3
        return numberFormatter.string(from: amount as NSNumber)?.replacingOccurrences(of: ".", with: ",") ?? amount.toString()
    }
}
