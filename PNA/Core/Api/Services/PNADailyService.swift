//
//  PNADailyService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/2/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class PNADailyService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var pnaDailyContent: String?
        if let root = response as? [String: Any] {
            if let htmlStr = root["content"] as? String {
                pnaDailyContent = htmlStr
            }
        }
        super.onFinish(pnaDailyContent, error: error, completion: completion)
    }
}
