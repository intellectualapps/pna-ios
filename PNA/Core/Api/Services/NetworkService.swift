//
//  NetworkService.swift
//  PNA
//
//  Created by center12 on 2/21/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class NetworkService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var networkUsers = [NetworkUser]()
        if let root = response as? [String: Any] {
            if let dicts = root["network"] as? [[String: Any]] {
                for dict in dicts {
                    let user = NetworkUser(dict: dict)
                    networkUsers.append(user)
                }
            }
        }
        super.onFinish(networkUsers, error: error, completion: completion)
    }
}
