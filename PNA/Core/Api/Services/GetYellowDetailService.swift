//
//  GetYellowDetailService.swift
//  PNA
//
//  Created by bui.duy.hien on 3/19/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class GetYellowDetailService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var yellowPageObject = YellowPageObject()
        if let root = response as? [String : Any] {
            if let yellowPagesDict = root["yellowPage"] as? [String : Any] {
                if let yellowPage = YellowPageObject(JSON: yellowPagesDict) {
                    yellowPageObject = yellowPage
                }
            }
        }
        super.onFinish(yellowPageObject, error: error, completion: completion)
    }
}
