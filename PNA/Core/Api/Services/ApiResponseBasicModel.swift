//
//  ApiResponseBasicModel.swift
//  MinBar
//
//  Created by Nguyen Van Dung on 1/25/18.
//  Copyright © 2018 Pentor. All rights reserved.
//

import Foundation

class Model: NSObject {
    override init() {
        super.init()
    }

    convenience init(dict: [String: Any]) {
        self.init()
        parseContentFromDict(dict: dict)
    }

    func parseContentFromDict(dict: [String: Any]) {

    }
}

class ApiResponseBasicModel: Model {
    var message = ""
    var result = ""
    var messageTitle = ""
    var isError = false
    var content: Any?
}

