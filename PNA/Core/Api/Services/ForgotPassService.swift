//
//  ForgotPassService.swift
//  PNA
//
//  Created by Quang Ly Hoang on 2/24/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class ForgotPassService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        let result = ApiResponseBasicModel()
        if let root = response as? [String : Any] {
            if let errorMessage = root.errorFromApiResponse() {
                result.message = errorMessage
                result.isError = true
            } else {
                result.message = root.stringForKey(key: "message") ?? ""
            }
        }
        super.onFinish(result, error: error, completion: completion)
    }
}
