//
//  PublicKeyService.swift
//  PNA
//
//  Created by MTQ on 3/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class PublicKeyService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var publicKey: String?
        if let responseDict = response as? [String: Any] {
            if let publicKeyDict = responseDict["pk"] as? String {
                publicKey = publicKeyDict
            }
        }
        super.onFinish(publicKey, error: error, completion: completion)
    }
}
