//
//  QRCodeViewModel.swift
//  PNA
//
//  Created by MTQ on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class QRCodeViewModel: BaseViewModel {
    func requestAPI(scannedToken: String, offer: Offer, completion: @escaping (Bool) -> ()) {
        //let path = Path.feedCommentsAll(feedid).path
        let path = Path.redeem(offerId: offer.id ?? "").path
        let params = RequestParams()
        let scannedToken1 = scannedToken.trimmingCharacters(in: .whitespaces)
        params.setValue(offer.id, forKey: "offer-id")
        if let user = User.archivedUser() {
            params.setValue(user.id, forKey: "member-id")
        }
        params.setValue(scannedToken1, forKey: "token")
        let service = RedeemOfferService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            guard let responseDict = response as? [String: Any] else {
                return
            }
            guard let _ = responseDict["redeemedOffer"] as? [String: Any] else {
                if let message = responseDict["message"] as? String {
                    Alert.showAlertWithErrorMessage(message, title: "", completion: {
                        completion(false)
                    })
                }
                return
            }
            completion(true)
        }
    }
}
