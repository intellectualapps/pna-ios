//
//  EventViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

class EventViewModel: BaseViewModel {
    var events = Variable([Event]())
    var weekEvents: [Event] = []
    var monthEvents: [Event] = []
    var upcomingEvents: [Event] = []
    var myWeekEvents: [Event] = []
    var myMonthEvents: [Event] = []
    var myUpcomingEvents: [Event] = []
    
    var memberId = AppDelegate.shared().loggedUser?.id ?? 0
    var pageSize = 3
    var start = 0
    
    func getEvents() {
        let params = RequestParams()
        params.setValue(memberId, forKey: "member-id")
        params.setValue(start, forKey: "start")
        //params.setValue(pageSize, forKey: "page-size")
        let path = Path.event.path
        let service = EventService(apiPath: path,
                                   method: .get,
                                   requestParam: params,
                                   paramEncoding: Encoding.forMethod(method: .get),
                                   retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { [weak self](response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            guard let `self` = self else { return }
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
            } else {
                let events = response as? [Event] ?? []
                self.weekEvents = []
                self.monthEvents = []
                self.upcomingEvents = []
                self.myWeekEvents = []
                self.myMonthEvents = []
                self.myUpcomingEvents = []
                for event in events {
                    guard let type = EventGroupType(rawValue: event.eventGroup) else { return }
                    switch type {
                    case .thisWeek:
                        self.weekEvents.append(event)
                        if event.hasRSVP {
                            self.myWeekEvents.append(event)
                        }
                    case .thisMonth:
                        self.monthEvents.append(event)
                        if event.hasRSVP {
                            self.myMonthEvents.append(event)
                        }
                    case .upcoming:
                        self.upcomingEvents.append(event)
                        if event.hasRSVP {
                            self.myUpcomingEvents.append(event)
                        }
                    }
                }
                self.events.value = events
            }
        }
    }
    
    func addOrRemoveToCalendar(eventId: String, completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(memberId, forKey: "member-id")
        params.setValue(eventId, forKey: "event-id")
        let path = Path.addEventToCalendar.path
        let service = AddEventToCalendarService(apiPath: path,
                                                method: .post,
                                                requestParam: params,
                                                paramEncoding: Encoding.forMethod(method: .post),
                                                retryCount: 1)
        service.doExecute(completion)
    }
}
