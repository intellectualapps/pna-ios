//
//  LoginViewModel.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class LoginViewModel: BaseViewModel {
    //MARK: Properties
    var email = ""
    var password = ""
    var auth_type = ""
    
    func login(completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(self.email, forKey: "email")
        params.setValue(self.password, forKey: "password")
        params.setValue(self.auth_type, forKey: "auth-type")
        
        let loginPath = Path.authen.path
        let encoding = Encoding.forMethod(method: .post)
        let service = LoginService(apiPath: loginPath, method: .post, requestParam: params, paramEncoding: encoding, retryCount: 1)
        service.doExecute(completion)
    }
    
    func storeDeviceToken(completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(self.email, forKey: "email")
        params.setValue((Defaults.deviceToken.get() as? String) ?? "", forKey: "token")
        params.setValue("ios", forKey: "platform")
        params.setValue(UIDevice.current.identifierForVendor!.uuidString, forKey: "uuid")
        
        let tokenPath = Path.storeDeviceToken.path
        let encoding = Encoding.forMethod(method: .post)
        let service = StoreDeviceTokenService(apiPath: tokenPath, method: .post, requestParam: params, paramEncoding: encoding, retryCount: 1)
        service.doExecute(completion)
    }
    
    func forgotPassword(completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(self.email, forKey: "email")
        
        let forgotPasswordPath = Path.forgotPassword.path
        let encoding = Encoding.forMethod(method: .post)
        let service = ForgotPassService(apiPath: forgotPasswordPath,
                                        method: .post,
                                        requestParam: params,
                                        paramEncoding: encoding,
                                        retryCount: 1)
        service.doExecute(completion)
    }
}
