//
//  YellowDetailViewModel.swift
//  PNA
//
//  Created by bui.duy.hien on 3/19/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

struct Location {
    let lat: Double
    let long: Double
}

class YellowDetailViewModel: BaseViewModel {
    func getYellowPage(withID id: Int, completion: @escaping ((YellowPageObject) -> ())) {
        let path = Path.getYellowPage(id: id).path
        let params = RequestParams()
        let memberID = AppDelegate.shared().loggedUser?.id ?? 0
        params.setValue(memberID, forKey: "member-id")
        let service = GetYellowDetailService(apiPath: path,
                                             method: .get,
                                             requestParam: params,
                                             paramEncoding: Encoding.forMethod(method: .get),
                                             retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let response = response as? YellowPageObject {
                completion(response)
            }
        }
    }
    
    func getAddress(_ address: String, completion: @escaping ((Location) -> Void)) {
        let postParameters = [ "address": address,"key": GoogleMap.apiKey]
        let url = "https://maps.googleapis.com/maps/api/geocode/json"
        Alamofire.request(url, method: .get, parameters: postParameters, encoding: URLEncoding.default, headers: nil).responseJSON {  response in
            
            if let receivedResults = response.result.value as? [String: Any]
            {
                if let results = receivedResults["results"] as? [[String: Any]] {
                    print(results)
                    guard let results = results.first else { return }
                    
                    if let geometry = results["geometry"] as? [String: Any] {
                        if let location = geometry["location"] as? [String: Any] {
                            print(location)
                            let lat = location["lat"] as! Double
                            let long = location["lng"] as! Double
                            let location = Location(lat: lat, long: long)
                            completion(location)
                        }
                    }
                }
            }
        }
    }
}
