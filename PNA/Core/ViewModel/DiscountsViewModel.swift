//
//  DiscountsViewModel.swift
//  PNA
//
//  Created by MTQ on 2/22/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

class DiscountsViewModel: BaseViewModel {
    
    var offers: [Offer] = []
    
    func getOffers(completion: @escaping ([Offer]) -> ()) {
        var path = Path.fetchOffers(memberID: 0).path
        let params = RequestParams()
        if let userid = AppDelegate.shared().loggedUser?.id {
            path = Path.fetchOffers(memberID: userid).path
            params.setValue(userid, forKey: "member-id")
        }
        let service = FetchOffersService(apiPath: path, method: .get,
                                         requestParam: params,
                                         paramEncoding: Encoding.forMethod(method: .get),
                                         retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let offers = response as? [Offer] {
                    completion(offers)
                    self.offers = offers
                }
            }
        }
    }
}
