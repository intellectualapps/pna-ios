//
//  UpdateExp_SkillsViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/26/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class UpdateExp_SkillsViewModel: BaseViewModel {
    var id = AppDelegate.shared().loggedUser?.id ?? 0
    var email = AppDelegate.shared().loggedUser?.email ?? ""
    var twitter = ""
    var facebook = ""
    var instagram = ""
    var website = ""
    var experience = Variable([[String : Any]]())
    var skills = Variable("")
    
    func fillFromUser(user: User) {
        id = user.id ?? 0
        email = user.email ?? ""
        twitter = user.twitter ?? ""
        facebook = user.facebook ?? ""
        instagram = user.instagram ?? ""
        website = user.website ?? ""
        var expDict = [[String : Any]]()
        for exp in user.experience {
            expDict.append(exp)
        }
        experience.value = expDict
        skills.value = (user.skills ?? "").replacingOccurrences(of: ",", with: "\n")
    }
    
    func addExp(position: String, company: String, state: String, country: String, startDate: String, endDate: String) {
        let params = RequestParams()
        params.setValue(position, forKey: "position")
        params.setValue(company, forKey: "employer")
        params.setValue("\(state), \(country)", forKey: "location")
        params.setValue("\(startDate) - \(endDate)", forKey: "period")
        let path = Path.addExperience(id: id).path
        let service = AddExperienceService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
            } else {
                Alert.showAlertWithErrorMessage("Experience added successfully")
                self.updateLoggedUser(completion: { (user, error) in
                    if let user = user as? User {
                        AppDelegate.shared().loggedUser?.experience = user.experience
                        self.fillFromUser(user: user)
                    }
                })
            }
        }
    }
    
    func addSkill(skills: String) {
        let params = RequestParams()
        params.setValue(skills, forKey: "skills")
        let path = Path.addSkills(id: id).path
        let service = AddSkillsService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
            } else {
                Alert.showAlertWithErrorMessage("Skill(s) added successfully")
                self.updateLoggedUser(completion: { (user, error) in
                    if let user = user as? User {
                        AppDelegate.shared().loggedUser?.skills = user.skills
                        self.fillFromUser(user: user)
                    }
                })
            }
        }
    }
    
    func updateSocialNetwork(insta: String, twitter: String, fb: String, web: String, completion: @escaping ()->()) {
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        if !insta.isEmpty {
            params.setValue(insta, forKey: "instagram")
        }
        if !twitter.isEmpty {
            params.setValue(twitter, forKey: "twitter")
        }
        if !fb.isEmpty {
            params.setValue(fb, forKey: "facebook")
        }
        if !web.isEmpty {
            params.setValue(web, forKey: "website")
        }
        let path = Path.updateProfile.path
        let service = UpdateProfileService(apiPath: path, method: .put, requestParam: params, paramEncoding: Encoding.forMethod(method: .put), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            }else{
                Alert.showAlertWithErrorMessage("Profile updated")
                self.updateLoggedUser(completion: { (user, error) in
                    if let user = user as? User {
                        AppDelegate.shared().loggedUser?.instagram = user.instagram
                        AppDelegate.shared().loggedUser?.twitter = user.twitter
                        AppDelegate.shared().loggedUser?.facebook = user.facebook
                        AppDelegate.shared().loggedUser?.website = user.website
                        self.fillFromUser(user: user)
                        completion()
                    }
                })
            }
        }
    }
    
    func updateLoggedUser(completion: @escaping NetworkServiceCompletion) {
        let path = Path.fullProfile(requestId: id, otherId: id).path
        let service = FullProfileService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
