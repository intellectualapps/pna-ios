//
//  WelcomeViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

enum SectionID: Int {
    case PNADaily = 0
    case event
    case offer
    case ourMembers
    case video
}

class WelcomeViewModel: BaseViewModel {
    var pnaDaily = Variable(PNADaily())
    var event = Variable(Event())
    var offer = Variable(Offer())
    var video = Variable(Video())
    var ourMembers = Variable([WelcomeUser]())
    var sections = Variable([SectionInfo]())
    
    func getWelcomeSections() {
        let param = RequestParams()
        param.setValue(AppDelegate.shared().loggedUser?.id ?? 0, forKey: "member-id")
        let path = Path.welcome.path
        let service = WelcomeService(apiPath: path, method: .get, requestParam: param, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecuteFromActor { (welcomeObj, error) in
            UIViewController.topMostViewController()?.hideProgress()
            guard let object = welcomeObj as? WelcomeObject else { return }
            self.pnaDaily.value = object.pnaDaily
            self.event.value = object.event
            self.offer.value = object.offer
            self.video.value = object.video
            self.ourMembers.value = object.welcomeUser
            self.didReceiveWelcomeObj(obj: object)
        }
    }

    func didReceiveWelcomeObj(obj: WelcomeObject) {
        let section = SectionInfo(indentify: "", title: "", height: 0, index: nil) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            if obj.pnaDaily.hasContent {
                let cell = CellInfo(indentify: "WelcomeScreenTableViewCell", aCellId: 0, height: 0, content: obj.pnaDaily, title: nil, cellColor: UIColor.clear, keyboardType: UIKeyboardType.default, capitalized: false)
                cells.append(cell)
            }
            if obj.event.hasContent {
                let cell = CellInfo(indentify: "WelcomeScreenTableViewCell", aCellId: 0, height: 0, content: obj.event, title: nil, cellColor: UIColor.clear, keyboardType: UIKeyboardType.default, capitalized: false)
                cells.append(cell)
            }
            if obj.offer.hasContent {
                let cell = CellInfo(indentify: "WelcomeScreenTableViewCell", aCellId: 0, height: 0, content: obj.offer, title: nil, cellColor: UIColor.clear, keyboardType: UIKeyboardType.default, capitalized: false)
                cells.append(cell)
            }

            if obj.welcomeUser.count > 0 {
                let cell = CellInfo(indentify: "ActiveMemberTableViewCell", aCellId: SectionID.ourMembers.rawValue, height: 0, content: obj.welcomeUser, title: nil, cellColor: UIColor.clear, keyboardType: UIKeyboardType.default, capitalized: false)
                cells.append(cell)
            }
            
            if obj.video.hasContent {
                let cell = CellInfo(indentify: "WelcomeScreenTableViewCell", aCellId: 0, height: 0, content: obj.video, title: nil, cellColor: UIColor.clear, keyboardType: UIKeyboardType.default, capitalized: false)
                cells.append(cell)
            }


            return cells
        }
        self.sections.value = [section]
    }
    
    func getEventDetail(completion: @escaping (Event?)->()) {
        let memberId = AppDelegate.shared().loggedUser?.id ?? 0
        let params = RequestParams()
        params.setValue(memberId, forKey: "member-id")
        let path = Path.eventDetail(event.value.id ?? "").path
        let service = EventDetailService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (event, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
                completion(nil)
            } else {
                if let event = event as? Event {
                    completion(event)
                }
            }
        }
    }
    
    func getOfferDetail(completion: @escaping (Offer?)->()) {
        let memberId = AppDelegate.shared().loggedUser?.id ?? 0
        let params = RequestParams()
        params.setValue(memberId, forKey: "member-id")
        let path = Path.offerDetail(offer.value.id ?? "").path
        let service = OfferDetailService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (offer, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
                completion(nil)
            } else {
                if let offer = offer as? Offer {
                    completion(offer)
                }
            }
        }
    }
    
    func getProfile(id: Int, completion: @escaping (User?)->()) {
        let selfId = AppDelegate.shared().loggedUser?.id ?? 0
        let path = Path.fullProfile(requestId: id, otherId: id).path
        let service = FullProfileService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (profile, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
                completion(nil)
            } else {
                if let profile = profile as? User {
                    completion(profile)
                }
            }
        }
    }
}
