//
//  FeedDetailViewModel.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

extension Array {
    func isCommentExisted(commentid: Int) -> Bool {
        if let temp = self as? [FeedCommentInfo] {
            for info in temp {
                if info.commentId == commentid {
                    return true
                }
            }
        }
        return false
    }
}

class FeedDetailViewModel: FeedBaseViewModel {
    var feedInfo = Variable(FeedInfo())
    var comments = Variable([FeedCommentInfo]())

    var pageSize = 20

    func startPage() -> Int {
        return comments.value.count / pageSize + 1
    }

    func getFeedComments(email: String,
                         feedid: Int,
                         isLoadMore: Bool,
                         start: Int,
                         pageSize: Int = 20,
                         isNeedProcess: Bool) {
//        let feedid = feedInfo?.feedid ?? 0
        let feedid = feedInfo.value.feedid
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(start, forKey: "start")
        params.setValue(pageSize, forKey: "page-size")
        params.setValue(pageSize, forKey: "Feed-id")
        let path = Path.feedCommentsAll(feedid).path
        let service = GetFeedCommentService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        if isNeedProcess {
            UIViewController.topMostViewController()?.showProgress()
        }
        service.doExecute {[weak self] (response, error) in
            if isNeedProcess {
                UIViewController.topMostViewController()?.hideProgress()
            }
            guard let strongself = self else {
                return
            }
            if let list = response as? [FeedCommentInfo] {
                var temp = [FeedCommentInfo]()
                if !isLoadMore {
                    temp.append(contentsOf: list)
                } else {
                    temp.append(contentsOf: strongself.comments.value)
                    for info in list {
                        if !temp.isCommentExisted(commentid: info.commentId) {
                            temp.append(info)
                        }
                    }
                }
                //sorting
                temp.sort(by: { (fd1, fd2) -> Bool in
                    let t1 = fd1.commentDate?.timeIntervalSince1970 ?? 0
                    let t2 = fd2.commentDate?.timeIntervalSince1970 ?? 0
                    return t1 > t2
                })
                strongself.comments.value = temp
            }
        }
    }
    
    func changeHasUserLikedForFeed(feed: FeedInfo?) -> Observable<Void> {
        return Observable<Void>.create { ob in
            if let feedIf = feed {
                self.likeFeed(feed: feedIf) { [weak self] (feedInfo) in
                    guard let _ = self?.feedInfo.value.likes else { return }
                    if feedInfo.hasUserLiked {
                        self?.feedInfo.value.likes += 1
                    } else {
                        self?.feedInfo.value.likes -= 1
                    }
                    self?.feedInfo.value.hasUserLiked = feedInfo.hasUserLiked
                    ob.onNext(())
                }
            }
            return Disposables.create()
        }
    }
}
