//
//  PaymentSubscriptionViewModel.swift
//  PNA
//
//  Created by hien.bui on 3/5/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class PaymentSubscriptionViewModel: BaseViewModel {
    func requestAPIGetAmount(completion: @escaping ((String) -> ())) {
        let path = Path.getAmount.path
        let service = GetAmountService(apiPath: path, method: .get, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let amountToString = response as? String {
                completion(amountToString)
            }
        }
    }
    
    func requestAPIInitialiseTransaction(email: String, amount: Int, completion: @escaping ((Transaction) -> ())) {
        let path = Path.initialiseTransaction.path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(amount, forKey: "amount")
        let service = InitialiseTransactionService(apiPath: path,
                                                   method: .post,
                                                   requestParam: params,
                                                   paramEncoding: Encoding.forMethod(method: .post),
                                                   retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let transaction = response as? Transaction {
                completion(transaction)
            }
        }
    }
}
