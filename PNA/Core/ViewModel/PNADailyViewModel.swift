//
//  PNADailyViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/3/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class PNADailyViewModel: BaseViewModel {
    
    var htmlContent = Variable(String())
    
    func getPNADailyContent() {
        let path = Path.pnaDaily.path
        let service = PNADailyService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (content, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
            } else {
                if let content = content as? String {
                    let fixedContent = content.handleHtmlString()
                    self?.htmlContent.value = fixedContent
                }
            }
        }
    }
}
