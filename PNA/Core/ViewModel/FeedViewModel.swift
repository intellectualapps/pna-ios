//
//  FeedViewModel.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

private extension Array {
    func isFeedExisted(feedid: Int) -> Bool {
        if let temp = self as? [FeedInfo] {
            for info in temp {
                if info.feedid == feedid {
                    return true
                }
            }
        }
        return false
    }
}

class FeedViewModel: FeedBaseViewModel {

    var feeds = Variable([FeedInfo]())
    var pageSize = 20
    var imageURL = ""
    var postText = ""
    var canPost: Bool {
        get {
            return !postText.isEmpty || canUploadImage
        }
    }
    var canUploadImage = false
    let bag = DisposeBag()

    func startPage() -> Int {
        return feeds.value.count / pageSize + 1
    }

    func getFeedList(isLoadMore: Bool, start: Int, pageSize: Int = 20) {
        let email = AppDelegate.shared().loggedUser?.email ?? ""
        let path = Path.feedList.path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(start, forKey: "start")
        params.setValue(pageSize, forKey: "page-size")
        let service = GetFeedListService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (resppnse , error) in
            UIViewController.topMostViewController()?.hideProgress()
            guard let strongself = self else {
                return
            }
            if let list = resppnse as? [FeedInfo] {
                var temp = [FeedInfo]()
                if !isLoadMore {
                    temp.append(contentsOf: list)
                } else {
                    temp.append(contentsOf: strongself.feeds.value)
                    for info in list {
                        if !temp.isFeedExisted(feedid: info.feedid) {
                            temp.append(info)
                        }
                    }
                }
                //sorting
                temp.sort(by: { (fd1, fd2) -> Bool in
                    let t1 = fd1.feedDate?.timeIntervalSince1970 ?? 0
                    let t2 = fd2.feedDate?.timeIntervalSince1970 ?? 0
                    return t1 > t2
                })
                strongself.feeds.value = temp
            }
        }
    }
    
    func updateLikeForFeed(feedInfo: FeedInfo) -> Observable<Void> {
        return Observable<Void>.create { ob in
            let listFeed = self.feeds.value
            for index in 0..<listFeed.count {
                if listFeed[index].feedid == feedInfo.feedid {
                    if feedInfo.hasUserLiked {
                        self.feeds.value[index].likes += 1
                    } else {
                        self.feeds.value[index].likes -= 1
                    }
                    self.feeds.value[index].hasUserLiked = feedInfo.hasUserLiked
                }
            }
            ob.onNext(())
            return Disposables.create()
        }
    }
    
    func changeHasUserLikedForFeed(feed: FeedInfo?) -> Observable<Void> {
        return Observable<Void>.create { ob in
            if let feedIf = feed {
                self.likeFeed(feed: feedIf) { [weak self] (feedInfo) in
                    guard let `self` = self else { return }
                    self.updateLikeForFeed(feedInfo: feedInfo).subscribe({_ in
                        ob.onNext(())
                    }).disposed(by: self.bag)
                }
            }
            return Disposables.create()
        }
    }
    
    func createPost(completion: ((FeedInfo) -> ())?) {
        let path = Path.createPost.path
        let params = RequestParams()
        params.setValue(self.postText, forKey: "text")
        if !imageURL.isEmpty {
            params.setValue(self.imageURL, forKey: "pic-url")
        }
        let service = CreatePostService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {(resppnse , error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
            } else {
                if let resppnse = resppnse as? FeedInfo {
                    completion?(resppnse)
                }
            }
        }
    }
}
