//
//  UpdateProfileViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/21/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class UpdateProfileViewModel: BaseViewModel {
    var email = AppDelegate.shared().loggedUser?.email ?? ""
    var id = AppDelegate.shared().loggedUser?.id ?? 0
    var firstname = ""
    var lastname = ""
    var phoneNumber = ""
    var jobTitle = ""
    var company = ""
    var imageURL = ""
    var isChangeAvatar = false
    var currentIndustryId = -1
    var currentCountryId  = -1
    var currentStateId    = -1
    var industry = -1 {
        didSet {
            self.selectedIndustry = industies.value[industry]
        }
    }
    
    var state = -1 {
        didSet {
            selectedState = states.value[state]
        }
    }
    //country
    var country = -1 {
        didSet {
            selectedCountry = countries.value[country]
        }
    }
    
    var selectedCountry: CountryInfo?
    var countries = Variable([CountryInfo]())
    var countrynames: [String] {
        return countries.value.map({$0.name})
    }
    
    //industry
    var selectedIndustry: IndustryInfo?
    var industies = Variable([IndustryInfo]())
    var indestriesNames: [String] {
        return industies.value.map({$0.name})
    }
    
    var selectedState: StateInfo?
    var states = Variable([StateInfo]())
    var stateNames: [String] {
        return states.value.map({$0.name})
    }
    
    func fillFromUser(user: User) {
        email = user.email ?? ""
        firstname = user.firstName ?? ""
        lastname = user.lastName ?? ""
        phoneNumber = user.phoneNumber ?? ""
        jobTitle = user.jobTitle ?? ""
        company = user.company ?? ""
        imageURL = user.pic ?? ""
        currentIndustryId = user.industry ?? -1
        currentCountryId = user.country ?? -1
        currentStateId = user.state ?? -1
    }
    
    func updateProfie(completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(firstname, forKey: "first-name")
        params.setValue(lastname, forKey: "last-name")
        params.setValue(phoneNumber, forKey: "phone-number")
        params.setValue(jobTitle, forKey: "job-title")
        params.setValue(company, forKey: "company")
        params.setValue(self.selectedIndustry?.id ?? "", forKey: "industry")
        params.setValue(self.selectedCountry?.sortName ?? "", forKey: "country-code")
        params.setValue(self.selectedState?.id ?? "", forKey: "state")
        params.setValue(self.imageURL, forKey: "photo-url")
        let path = Path.updateProfile.path
        let service = UpdateProfileService(apiPath: path, method: .put, requestParam: params, paramEncoding: Encoding.forMethod(method: .put), retryCount: 1)
        service.doExecute(completion)
    }
    
    func updateLoggedUser(completion: @escaping NetworkServiceCompletion) {
        let path = Path.fullProfile(requestId: id, otherId: id).path
        let service = FullProfileService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
