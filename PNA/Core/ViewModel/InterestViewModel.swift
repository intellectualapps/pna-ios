//
//  InterestViewModel.swift
//  PNA
//
//  Created by Nguyen Van Dung on 3/15/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class InterestViewModel: BaseViewModel {
    var interest = [InterestInfo]()

    func getInterest(completion: @escaping NetworkServiceCompletion) {
        GetInteresetService.getInterest {[weak self] (response, error) in
            if let infos = response as? [InterestInfo] {
                self?.interest = infos
            }
            completion(response, error)
        }
    }
}
