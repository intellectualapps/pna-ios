//
//  YellowPagesViewModel.swift
//  PNA
//
//  Created by hien.bui on 3/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

private extension Array {
    func isYellowPageObjectExisted(yellowPageID: Int) -> Bool {
        if let temp = self as? [YellowPageObject] {
            for info in temp {
                if info.id == yellowPageID {
                    return true
                }
            }
        }
        return false
    }
}

class YellowPagesViewModel: BaseViewModel {
    var yellowPages = Variable([YellowPageObject]())
    private var pageSize = 20
    
    var states = Variable([StateInfo]())
    var categories = Variable([YellowPageCategory]())
    
    var selectedStates = Variable([StateInfo]())
    var selectedCategories = Variable([YellowPageCategory]())
    
    func startPage() -> Int {
        return yellowPages.value.count / pageSize + 1
    }
    
    func getYellowPages(isLoadMore: Bool, start: Int, pageSize: Int = 20) {
        let path = Path.getYellowPages.path
        let params = RequestParams()
        let memberID = AppDelegate.shared().loggedUser?.id ?? 0
        params.setValue(memberID, forKey: "member-id")
        params.setValue(start, forKey: "start")
        params.setValue(pageSize, forKey: "page-size")
        let service = GetYellowPagesService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (resppnse , error) in
            UIViewController.topMostViewController()?.hideProgress()
            guard let `self` = self else { return }
            if let list = resppnse as? [YellowPageObject] {
                var temp = [YellowPageObject]()
                if !isLoadMore {
                    temp.append(contentsOf: list)
                } else {
                    temp.append(contentsOf: self.yellowPages.value)
                    for info in list {
                        if !temp.isYellowPageObjectExisted(yellowPageID: info.id) {
                            temp.append(info)
                        }
                    }
                }
                self.yellowPages.value = temp
            }
        }
    }
    
    var yellowPagesSearch = Variable([YellowPageObject]())
    private var pageSizeSearch = 20
    func startPageSearchYellow() -> Int {
        return yellowPagesSearch.value.count / pageSizeSearch + 1
    }
    
    func getYellowPagesSearch(searchText: String, isLoadMore: Bool, start: Int, pageSize: Int = 20) {
        let validSearchText = searchText.trimmingCharacters(in: .whitespaces)
        guard validSearchText != "" else { return }
        let path = Path.getYellowPagesSearch(searchText: searchText).path
        let params = RequestParams()
        params.setValue(validSearchText, forKey: "search-term")
        params.setValue(start, forKey: "start")
        params.setValue(pageSize, forKey: "page-size")
        let service = SearchYellowPagesService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute {[weak self] (resppnse , error) in
            guard let `self` = self else { return }
            if let list = resppnse as? [YellowPageObject] {
                var temp = [YellowPageObject]()
                if !isLoadMore {
                    temp.append(contentsOf: list)
                } else {
                    temp.append(contentsOf: self.yellowPagesSearch.value)
                    for info in list {
                        if !temp.isYellowPageObjectExisted(yellowPageID: info.id) {
                            temp.append(info)
                        }
                    }
                }
                self.yellowPagesSearch.value = temp
                if temp.isEmpty {
                    Alert.showAlertWithErrorMessage("No results found")
                }
            }
        }
    }
    
    func filterYellowPages(isLoadMore: Bool) {
        if selectedStates.value.isEmpty && selectedCategories.value.isEmpty {
            self.getYellowPages(isLoadMore: true, start: 0)
            return
        }
        var statesId: [Int] = []
        var categoriesId: [Int] = []
        for state in selectedStates.value {
            statesId.append(state.id)
        }
        for category in selectedCategories.value {
            categoriesId.append(category.categoryId)
        }
        let statesString = statesId.map({"\($0)"}).joined(separator: ",")
        let categoriesString = categoriesId.map({"\($0)"}).joined(separator: ",")
        let params = RequestParams()
        let memberID = AppDelegate.shared().loggedUser?.id ?? 0
        params.setValue(memberID, forKey: "member-id")
        params.setValue(statesString, forKey: "states")
        params.setValue(categoriesString, forKey: "categories")
        let path = Path.filterYellowPage.path
        let service = FilterYellowPagesService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (resppnse, error) in
            UIViewController.topMostViewController()?.hideProgress()
            guard let `self` = self else { return }
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
            }
            if let list = resppnse as? [YellowPageObject] {
                self.yellowPages.value = list
            }
        }
    }
    
    func getState() {
        let path = Path.stateLookup.path + "/\(AppDelegate.shared().loggedUser?.country ?? -1)"
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute({[weak self] (result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let stateResult = result as? [String: Any] {
                    if let stateDict = stateResult["states"] as? [[String: Any]] {
                        var temp = [StateInfo]()
                        for dict in stateDict {
                            let info = StateInfo(dict: dict)
                            temp.append(info)
                        }
                        self?.states.value = temp
                        self?.selectedStates.value = []
                    }
                    
                }
            }
            
        })
    }
    
    func getCategories() {
        let path = Path.yellowPageCategoryLookup.path
        let params = RequestParams()
        let id = AppDelegate.shared().loggedUser?.id ?? 0
        params.setValue(id, forKey: "member-id")
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute({[weak self] (result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let stateResult = result as? [String: Any] {
                    if let stateDict = stateResult["category"] as? [[String: Any]] {
                        var temp = [YellowPageCategory]()
                        for dict in stateDict {
                            let info = YellowPageCategory(dict: dict)
                            temp.append(info)
                        }
                        self?.categories.value = temp
                        self?.selectedCategories.value = []
                    }

                }
            }
            
        })
    }
}
