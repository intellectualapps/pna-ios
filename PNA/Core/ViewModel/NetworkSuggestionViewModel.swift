//
//  NetworkSuggestionViewModel.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/30/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

class NetworkSuggestionViewModel: BaseViewModel {
    var suggestionUsers = Variable([SuggestionUser]())
    var selectedUsers = Variable([SuggestionUser]())
    var selectedEmails: [String] {
        let emails = selectedUsers.value.map { $0.email  }
        return emails
    }

    func getSuggestionList() {

        let email = AppDelegate.shared().loggedUser?.email ?? ""
        let path = Path.suggestionNetwork.path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        let service = SuggestionNetworkService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let users = response as? [SuggestionUser] {
                self?.suggestionUsers.value = users
            }
        }
    }

    func addUser(user: SuggestionUser) {
        var listUsers = selectedUsers.value
        var isExisted = false
        for info in listUsers {
            if info.id == user.id {
                isExisted = true
                break
            }
        }
        if !isExisted {
            listUsers.append(user)
            selectedUsers.value = listUsers
        }
    }

    func removeUser(email: String) {
        var listUsers = selectedUsers.value
        var index = listUsers.count - 1
        for info in listUsers.reversed() {
            if info.email == email {
                listUsers.remove(at: index)
                break
            }
            index -= 1
        }
        selectedUsers.value = listUsers
    }
    
    func addUserSlectedToMyNetwrok() {
        let email = AppDelegate.shared().loggedUser?.email ?? ""
        let memberIds = selectedUsersToMemberIds(users: selectedUsers.value)
        let path = Path.networkInvitation.path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(memberIds, forKey: "member-ids")
        let service = NetworkInvitationService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let users = response as? [NetworkUser] {
                //do something with users
                //show home screen
                self?.showHomeScreen()
            }
        }
    }
    
    func showHomeScreen(_ animated:Bool = true){
        DispatchQueue.main.async {
            AppDelegate.shared().setupFeedMenu()
        }
    }
    
    func selectedUsersToMemberIds(users: [SuggestionUser]) -> String {
        var ids = [Int]()
        for user in users {
            ids.append(user.id)
        }
        
        return ids.map{ String($0) }.joined(separator: ",")
    }
}
