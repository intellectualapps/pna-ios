//
//  SignUpViewModel.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

class SignUpViewModel: BaseViewModel {
    //MARK: Properties
    var firstName = ""
    var lastName = ""
    var email = ""
    var phone = ""
    var password = ""
    
    func excuteRegister(completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(self.firstName, forKey: "first-name")
        params.setValue(self.lastName, forKey: "last-name")
        params.setValue(self.email, forKey: "email")
        params.setValue(self.password, forKey: "password")
        params.setValue(self.phone, forKey: "phone-number")

        let registerPath = Path.SignUpEmail.path
        let encoding = Encoding.forMethod(method: .post)
        let service = RegisterService(apiPath: registerPath, method: .post, requestParam: params, paramEncoding: encoding, retryCount: 1)
        service.doExecute(completion)
    }
}
