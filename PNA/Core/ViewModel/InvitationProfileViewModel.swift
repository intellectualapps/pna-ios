//
//  InvitationProfileViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/4/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class InvitationProfileViewModel: BaseViewModel {
    var slimProfile = Variable(SlimProfile())
    var requestUserID = 0
    var networkRequestID = 0
    
    func getSlimProfile() {
        let path = Path.memberSlim(requestUserID).path
        let service = SlimProfileService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let user = response as? SlimProfile {
                self?.slimProfile.value = user
            }
        }
    }
    
    func updateNetworkInvitation(accepted: Bool = false, _ success: @escaping ()->()) {
        let memberId = AppDelegate.shared().loggedUser?.id ?? 0
        let option = accepted ? "true" : "false"
        let path = Path.updateNetworkInvitation(networkId: networkRequestID, option: option, memberId: memberId).path
        let service = UpdateNetworkInvitationService(apiPath: path, method: .put, requestParam: nil, paramEncoding: Encoding.forMethod(method: .put), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {(response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let _ = response as? NetworkUser {
                success()
            }
        }
    }
    
}
