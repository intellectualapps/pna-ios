//
//  TicketAndTableEventViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/13/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class TicketAndTableViewModel: BaseViewModel {
    
    var quantity: Int = 1
    var purchaseType = Variable(EventPuchaseType.ticket)
    var priceType = Variable(EventPriceType.regular)
    var price = Variable("")
    
    func requestInitializeEventTicketTransaction(eventId: String, ticketType: String, completion: @escaping ((Transaction) -> ())) {
        let path = Path.initializeEventTicketTransaction.path
        let params = RequestParams()
        params.setValue(AppDelegate.shared().loggedUser?.email ?? "", forKey: "email")
        params.setValue(eventId, forKey: "event-id")
        params.setValue(ticketType, forKey: "ticket-type")
        params.setValue(quantity, forKey: "quantity")
        let service = InitializeEventTicketTransactionService(apiPath: path,
                                                   method: .post,
                                                   requestParam: params,
                                                   paramEncoding: Encoding.forMethod(method: .post),
                                                   retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let transaction = response as? Transaction {
                completion(transaction)
            }
        }
    }
    
    func requestInitializeEventTableTransaction(eventId: String, tableType: String, completion: @escaping ((Transaction) -> ())) {
        let path = Path.initializeEventTableTransaction.path
        let params = RequestParams()
        params.setValue(AppDelegate.shared().loggedUser?.email ?? "", forKey: "email")
        params.setValue(eventId, forKey: "event-id")
        params.setValue(tableType, forKey: "table-type")
        params.setValue(quantity, forKey: "quantity")
        let service = InitializeEventTableTransactionService(apiPath: path,
                                                              method: .post,
                                                              requestParam: params,
                                                              paramEncoding: Encoding.forMethod(method: .post),
                                                              retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let transaction = response as? Transaction {
                completion(transaction)
            }
        }
    }
    
    func requestVerifyEventTicketTransaction(referenceCode: String, completion: @escaping (() -> ())) {
        let path = Path.verifyEventTicketTransaction(referenceCode: referenceCode, email: AppDelegate.shared().loggedUser?.email ?? "").path
        let service = VerifyEventTicketTransactionService(apiPath: path,
                                                          method: .get,
                                                          paramEncoding: Encoding.forMethod(method: .get),
                                                          retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let _ = response as? ApiResponseBasicModel {
                completion()
            }
        }
    }
    
    func requestVerifyEventTableTransaction(referenceCode: String, completion: @escaping (() -> ())) {
        let path = Path.verifyEventTableTransaction(referenceCode: referenceCode, email: AppDelegate.shared().loggedUser?.email ?? "").path
        let service = VerifyEventTableTransactionService(apiPath: path,
                                                         method: .get,
                                                         paramEncoding: Encoding.forMethod(method: .get),
                                                         retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let _ = response as? ApiResponseBasicModel {
                completion()
            }
        }
    }
}
