//
//  ProfileDetailViewModel.swift
//  PNA
//
//  Created by center12 on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

class ProfileDetailViewModel: BaseViewModel {
    var detailUser = Variable(User())
    var memberIdOfMyNetwork = 0
    var country = Variable("")
    var state = Variable("")
    var industry = Variable("")
    
    func getDetailUser() {
        if detailUser.value.id == 0 {
            return
        }
        let memberId = AppDelegate.shared().loggedUser?.id ?? 0
        let path = Path.fullProfile(requestId: memberId, otherId: memberIdOfMyNetwork).path
        let service = ProfileDetailService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let user = response as? ProfileDetailUser {
                self?.detailUser.value = user
                self?.getCountryName()
            }
        }
    }
    
    func getCountryName() {
        let path = Path.countryLookup.path
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        
        service.doExecute({[weak self] (result, error) in
            if let countryResult = result as? [String: Any] {
                if let cntyDict = countryResult["countries"] as? [[String: Any]] {
                    for dict in cntyDict {
                        let info = CountryInfo(dict: dict)
                        if info.id == self?.detailUser.value.country {
                            self?.country.value = info.name
                        }
                    }
                }
            }
        })
    }
    
    func getIndustryName() {
        let path = Path.industryLookup.path
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        
        service.doExecute({[weak self] (result, error) in
            if let countryResult = result as? [String: Any] {
                if let cntyDict = countryResult["industries"] as? [[String: Any]] {
                    for dict in cntyDict {
                        let info = IndustryInfo(dict: dict)
                        if info.id == self?.detailUser.value.industry {
                            self?.industry.value = info.name
                        }
                    }
                }
            }
        })
    }
    
}
