//
//  DropDownViewModel.swift
//  PNA
//
//  Created by QuangLH on 7/19/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class DropDownViewModel {
    
    func industrySetup(completion: @escaping ([IndustryInfo]?)->()) {
        let path = Path.industryLookup.path
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute({(result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let industryResult = result as? [String: Any] {
                    if let indDicts = industryResult["industries"] as? [[String: Any]] {
                        var temp = [IndustryInfo]()
                        for dict in indDicts {
                            let info = IndustryInfo(dict: dict)
                            temp.append(info)
                        }
                        completion(temp)
                    }
                }
            }
            
        })
    }
    
    func countrySetup(completion: @escaping ([CountryInfo]?)->()) {
        let path = Path.countryLookup.path
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        
        service.doExecute({(result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let countryResult = result as? [String: Any] {
                    if let cntyDict = countryResult["countries"] as? [[String: Any]] {
                        var temp = [CountryInfo]()
                        for dict in cntyDict {
                            let info = CountryInfo(dict: dict)
                            temp.append(info)
                        }
                        completion(temp)
                    }
                }
            }
            
        })
    }
    
    func stateSetup(countryId: Int, completion: @escaping ([StateInfo]?)->()) {
        let path = Path.stateLookup.path + "/\(countryId)"
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        
        service.doExecute({(result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
                completion(nil)
            } else {
                if let stateResult = result as? [String: Any] {
                    if let stateDict = stateResult["states"] as? [[String: Any]] {
                        var temp = [StateInfo]()
                        for dict in stateDict {
                            let info = StateInfo(dict: dict)
                            temp.append(info)
                            completion(temp)
                        }
                    }
                    
                }
            }
            
        })
    }
}
