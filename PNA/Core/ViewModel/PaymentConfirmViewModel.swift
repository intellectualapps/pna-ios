//
//  PaymentConfirmViewModel.swift
//  PNA
//
//  Created by MTQ on 3/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//
import Foundation

class PaymentConfirmViewModel: BaseViewModel {
    func requestAPIVerifyTransaction(referenceCode: String, email: String, completion: @escaping (() -> ())) {
        let path = Path.verityTransaction(referenceCode: referenceCode, email: email).path
        let service = VerifyTransactionService(apiPath: path,
                                               method: .get,
                                               paramEncoding: Encoding.forMethod(method: .get),
                                               retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let _ = response as? ApiResponseBasicModel {
                completion()
            }
        }
    }
    
    func requestAPIGetPublicKey(completion: @escaping ((String) -> ())) {
        let path = Path.getPublicKey.path
        let service = PublicKeyService(apiPath: path,
                                               method: .get,
                                               paramEncoding: Encoding.forMethod(method: .get),
                                               retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let response = response as? String {
                completion(response)
            }
        }
    }
}
