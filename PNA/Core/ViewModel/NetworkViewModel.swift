//
//  NetworkViewModel.swift
//  PNA
//
//  Created by center12 on 2/22/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

typealias NetworkCompleted = () -> ()

class NetworkViewModel: BaseViewModel {
    var networkUsers = Variable([NetworkUser]())
    var selectedUsers = Variable([NetworkUser]())
    weak var view: NetworkViewController?
    
    func getNetworkList(excution: @escaping NetworkCompleted) {
        let memberId = AppDelegate.shared().loggedUser?.id ?? 0
        let path = Path.Network(memberId).path
        let service = NetworkService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let users = response as? [NetworkUser] {
                self?.networkUsers.value = users
                excution()
            }
        }
    }
    
    func convertToSuggestionUser(user: NetworkUser) -> SuggestionUser {
        let suggestUser = SuggestionUser()
        suggestUser.pic = user.pic ?? ""
        suggestUser.jobTitle = user.jobTitle ?? ""
        suggestUser.fname = user.firstName ?? ""
        suggestUser.lname = user.lastName ?? ""
        suggestUser.isAdded = true
        
        return suggestUser
    }
    
    
}

// MARK: - extension networkView
extension NetworkViewModel: NetworkViewDelegate {
    func onDidSelectedMember(index: Int) -> (identifier: String, user: NetworkUser) {
        if index > networkUsers.value.count {
            return ("", NetworkUser())
        }
        let user = networkUsers.value[index]
        let identifier = user.hasAccepted ? "profileDetail" : "slimProfile"
        
        return (identifier, user)
    }
}
