//
//  IndustryViewModel.swift
//  PNA
//
//  Created by nguyen.van.dungb on 2/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class IndustryViewModel: BaseViewModel {
    //MARK: Properties
    var email = ""
    var password = ""
    var auth_type = ""
    
    func login(completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(self.email, forKey: "email")
        params.setValue(self.password, forKey: "password")
        params.setValue(self.auth_type, forKey: "auth-type")
        
        let loginPath = Path.authen.path
        let encoding = Encoding.forMethod(method: .post)
        let service = LoginService(apiPath: loginPath, method: .post, requestParam: params, paramEncoding: encoding, retryCount: 1)
        service.doExecute(completion)
    }
}
