//
//  SearchMemberViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/15/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SearchMemberViewModel: BaseViewModel {
    var members = Variable([SearchProfile]())
    var industies = Variable([IndustryInfo]())
    var indestriesNames: [String] {
        return industies.value.map({$0.name})
    }
    var currentIndustryId = -1
    var industry = -1 {
        didSet {
            self.selectedIndustry = industies.value[industry]
        }
    }
    var selectedIndustry: IndustryInfo?
    var memberId = AppDelegate.shared().loggedUser?.id ?? 0
    var pageSize = 10
    var start = 0
    
    func getMembers(text: String) {
        var industryid: String?
        if let industry = selectedIndustry {
            industryid = String(industry.id)
        }
        let path = Path.searchMember(memId: memberId, text: text, industry: industryid).path
        let params = RequestParams()
        params.setValue(start, forKey: "start")
        params.setValue(pageSize, forKey: "page-size")

        let service = SearchMemberService(apiPath: path,
                                          method: .get,
                                          requestParam: params,
                                          paramEncoding: Encoding.forMethod(method: .get),
                                          retryCount: 1)
        service.doExecute { [weak self](response, error) in
            guard let `self` = self else { return }
            if let error = error {
                print(error.message)
            } else {
                let members = response as? [SearchProfile] ?? []
                self.members.value = members
                if members.isEmpty {
                    Alert.showAlertWithErrorMessage("No results found")
                }
            }
        }
    }
    
    func addUserSlectedToMyNetwrok(membersId: [Int], completion: @escaping ()->()) {
        let email = AppDelegate.shared().loggedUser?.email ?? ""
        let membersId = membersId.flatMap{String($0)}.joined(separator: ",")
        let path = Path.networkInvitation.path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(membersId, forKey: "member-ids")
        let service = NetworkInvitationService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let users = response as? [NetworkUser] {
                //do something with users
                Alert.showAlertWithErrorMessage("Invitation sent")
                completion()
            }
        }
    }
}
