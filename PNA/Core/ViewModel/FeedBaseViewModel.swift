//
//  FeedBaseViewModel.swift
//  PNA
//
//  Created by Admin on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

typealias FeedLikeServiceComplete = (_ feed: FeedInfo) -> ()

class FeedBaseViewModel: BaseViewModel {
    
    func likeFeed(feed: FeedInfo, excution: @escaping FeedLikeServiceComplete) {
        let email = AppDelegate.shared().loggedUser?.email ?? ""
        let path = Path.feedLike(feedId: feed.feedid).path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        let service = FeedLikeService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let feed = response as? FeedInfo {
                excution(feed)
            }
        }
    }
}
