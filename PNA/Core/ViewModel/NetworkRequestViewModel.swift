//
//  NetworkRequestViewModel.swift
//  PNA
//
//  Created by center12 on 2/22/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

typealias UpdateNetworkInvitationCompleted = (_ user: NetworkUser) -> ()
typealias NetworkRequestCompleted = () -> ()

class NetworkRequestViewModel: BaseViewModel {
    var networkRequestUsers = Variable([NetworkUser]())
    
    func getNetworkRequestList(_ completion: @escaping (Bool) -> ()) {
        let memberId = AppDelegate.shared().loggedUser?.id ?? 0
        let path = Path.networkRequest(memberId).path
        let service = NetworkRequestService(apiPath: path, method: .get, requestParam: nil, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let users = response as? [NetworkUser] {
                self?.networkRequestUsers.value = users
                completion(true)
            }
        }
    }
    
    func removeUser(id: Int) {
        var listUsers = networkRequestUsers.value
        if listUsers.count == 0 {
            return
        }
        var index = listUsers.count - 1
        for info in listUsers.reversed() {
            if info.memberId == id {
                listUsers.remove(at: index)
                break
            }
            index -= 1
        }
        networkRequestUsers.value = listUsers
    }
    
    func updateNetworkInvitation(user: NetworkUser, accepted: Bool = false, _ success: @escaping UpdateNetworkInvitationCompleted) {
        let memberId = AppDelegate.shared().loggedUser?.id ?? 0
        let networkId = user.networkId
        let option = accepted ? "true" : "false"
        let path = Path.updateNetworkInvitation(networkId: networkId, option: option, memberId: memberId).path
        let service = UpdateNetworkInvitationService(apiPath: path, method: .put, requestParam: nil, paramEncoding: Encoding.forMethod(method: .put), retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else if let user = response as? NetworkUser {
                self?.removeUser(id: user.memberId)
                success(user)
            }
        }
    }
}

extension NetworkRequestViewModel: NetworkInvitationViewDelegate {
    func onAcceptedTapped(user: NetworkUser?, _ excution: @escaping UpdateNetworkInvitationCompleted) {
        if let requestUser = user {
            updateNetworkInvitation(user: requestUser, accepted: true) { user in
                excution(user)
            }
        }
        
    }
    
    func onDeclineTapped(user: NetworkUser?, _ excution: @escaping UpdateNetworkInvitationCompleted) {
        if let requestUser = user {
            updateNetworkInvitation(user: requestUser, accepted: false) { user in
                excution(user)
            }
        }
    }
}

