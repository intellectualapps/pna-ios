//
//  ProfileViewModel.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

class IndustryInfo: Model {
    var id = 0
    var name = ""
    
    override func parseContentFromDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        name = dict.stringOrEmptyForKey(key: "industry")
    }
}

class CountryInfo: Model {
    var id = 0
    var sortName = ""
    var name = ""
    var phoneCode = ""
    
    override func parseContentFromDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        sortName = dict.stringOrEmptyForKey(key: "sortName")
        name = dict.stringOrEmptyForKey(key: "name")
        phoneCode = dict.stringOrEmptyForKey(key: "phoneCode")
    }
}

class StateInfo: Model {
    var id = 0
    var name = ""
    
    override func parseContentFromDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        name = dict.stringOrEmptyForKey(key: "name")
    }
}

class ProfileViewModel: BaseViewModel {
    var email = ""
    var jobTitle = ""
    var company = ""
    var currentIndustryId = -1
    var currentCountryId  = -1
    var currentStateId    = -1
    var industry = -1 {
        didSet {
            self.selectedIndustry = industies.value[industry]
        }
    }
    
    var state = -1 {
        didSet {
            selectedState = states.value[state]
        }
    }
    //country
    var country = -1 {
        didSet {
            selectedCountry = countries.value[country]
        }
    }
    
    var selectedCountry: CountryInfo?
    var countries = Variable([CountryInfo]())
    var countrynames: [String] {
        return countries.value.map({$0.name})
    }

    //industry
    var selectedIndustry: IndustryInfo?
    var industies = Variable([IndustryInfo]())
    var indestriesNames: [String] {
        return industies.value.map({$0.name})
    }

    var selectedState: StateInfo?
    var states = Variable([StateInfo]())
    var stateNames: [String] {
        return states.value.map({$0.name})
    }
    
    func fillFromUser(user: User) {
        email = user.email ?? ""
        jobTitle = user.jobTitle ?? ""
        company = user.company ?? ""
        currentIndustryId = user.industry ?? -1
        currentCountryId = user.country ?? -1
        currentStateId = user.state ?? -1
    }

    func errorMessage() -> String {
        if jobTitle.trim().isEmpty {
            return "Please enter job title"
        } else if company.trim().isEmpty {
            return "Please enter company"
        } else if self.selectedIndustry?.id == nil {
            return "Please select Industry"
        } else if self.selectedCountry?.id == nil {
            return "Please select country"
        } else if self.selectedState?.id == nil {
            return "Please select state"
        }
        return ""
    }

    func updateProfie(completion: @escaping NetworkServiceCompletion) {
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(jobTitle, forKey: "job-title")
        params.setValue(company, forKey: "company")
        params.setValue(self.selectedIndustry?.id ?? "", forKey: "industry")
        params.setValue(self.selectedCountry?.id ?? "", forKey: "country")
        params.setValue(self.selectedState?.id ?? "", forKey: "state")
        let path = Path.updateProfile.path
        let service = UpdateProfileService(apiPath: path, method: .put, requestParam: params, paramEncoding: Encoding.forMethod(method: .put), retryCount: 1)
        service.doExecute(completion)
    }
}
