//
//  Ultilities.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/27/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import EventKit

class Ultilites {
    static func addEventToCalendar(title: String, notes: String?, address: String?, startDate: Date, endDate: Date, completion: @escaping ()->()) {
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = notes
                event.calendar = eventStore.defaultCalendarForNewEvents
                event.location = address
                do {
                    try eventStore.save(event, span: .thisEvent)
                    DispatchQueue.main.async {
                        completion()                        
                    }
                } catch let e as NSError {
                    print(e.localizedDescription)
                    return
                }
            }
        })
    }
    
    static func removeEventFromCalendar(title: String) {
        let eventStore = EKEventStore()
        let startDate=Date().addingTimeInterval(-60*60*24*365)  //back for a year
        let endDate=Date().addingTimeInterval(60*60*24*365)     //go forward for a year
        let predicate = eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: nil)
        let events = eventStore.events(matching: predicate)
        for event in events {
            if event.title == title {
                do {
                    try eventStore.remove(event, span: EKSpan.thisEvent, commit: true)
                } catch let e as NSError {
                    print(e.localizedDescription)
                    return
                }
            }
        }
    }
    
    static func addReminderToReminders(title: String, date: Date?, completion: @escaping ()->()) {
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: .reminder, completion: {(granted, error) in
            if granted && error == nil {
                let reminder = EKReminder(eventStore: eventStore)
                reminder.title = title
                let dateComponents = Calendar.current.dateComponents([.day, .month, .year], from: date ?? Date())
                reminder.dueDateComponents = dateComponents
                reminder.calendar = eventStore.defaultCalendarForNewReminders()
                do {
                    try eventStore.save(reminder, commit: true)
                    DispatchQueue.main.async {
                        completion()
                    }
                } catch let e as NSError {
                    print(e.localizedDescription)
                    return
                }
            }
        })
    }
    
    static func removeReminderFromReminders(title: String) {
        let eventStore = EKEventStore()
        let predicate = eventStore.predicateForReminders(in: nil)
        eventStore.fetchReminders(matching: predicate) { (reminders) in
            guard let reminders = reminders else { return }
            for reminder in reminders {
                if reminder.title == title {
                    do {
                        try eventStore.remove(reminder, commit: true)
                    } catch let e as NSError {
                        print(e.localizedDescription)
                        return
                    }
                }
            }
        }
    }
    
}
