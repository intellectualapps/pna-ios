//
//  PaddingLabel.swift
//  PNA
//
//  Created by hien.bui on 2/27/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class PaddingLabel: UILabel {
    @IBInspectable var topInset: CGFloat    = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat   = 0.0
    @IBInspectable var rightInset: CGFloat  = 0.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset,
                                       left: leftInset,
                                       bottom: bottomInset,
                                       right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
}
