//
//  CardPaymentView.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/12/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import Paystack

class CardPaymentView: BaseViewController {

    @IBOutlet weak var cardNumberTextField: WTReTextField!
    @IBOutlet weak var expiryTextField: WTReTextField!
    @IBOutlet weak var cvvTextField: WTReTextField!
    
    var paymentViewModel = PaymentConfirmViewModel()
    var transaction: Transaction?
    var viewModel = TicketAndTableViewModel()
    var purchaseType: EventPuchaseType!
    var successClousure: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.expiryTextField?.pattern = "^(1[0-2]|(?:0)[1-9])(?:/)\\d{2}$"
        self.cvvTextField?.pattern = "^\\d{3}$"
        self.cardNumberTextField?.pattern = "^(\\d{4}(?: )){3}\\d{4}$"
    }
    
    init() {
        super.init(nibName: "CardPaymentView", bundle: Bundle.main)
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBAction func onPay(_ sender: UIButton) {
        paymentViewModel.requestAPIGetPublicKey { publicKey in
            if (publicKey == "" || !publicKey.hasPrefix("pk_")) {
                print("You need to set your Paystack public key. You can find your public key at https://dashboard.paystack.co/#/settings/developer .")
                // You need to set your Paystack public key.
                return
            }
            Paystack.setDefaultPublicKey(publicKey)
            self.fetchAccessCodeAndChargeCard()
        }
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    private func fetchAccessCodeAndChargeCard() {
        guard let accessCode = transaction?.accessCode else { return }
        self.chargeWithSDK(newCode: accessCode)
    }
    
    private func chargeWithSDK(newCode: String) {
        let transactionParams = PSTCKTransactionParams.init()
        transactionParams.access_code = newCode as String;
        
        // use library to create charge and get its reference
        let cardParams = PSTCKCardParams()
        cardParams.number = cardNumberTextField.text ?? ""
        let result = expiryTextField.text?.components(separatedBy: "/")
        cardParams.expMonth = UInt(result?.first ?? "0") ?? 0
        cardParams.expYear = UInt(result?.last ?? "0") ?? 0
        cardParams.cvc = cvvTextField.text ?? ""
        
        PSTCKAPIClient.shared().chargeCard(cardParams, forTransaction: transactionParams, on: self, didEndWithError: { (error, reference) in
            print("Charge errored")
            // what should I do if an error occured?
            print(error)
            if error._code == PSTCKErrorCode.PSTCKExpiredAccessCodeError.rawValue{
                // access code could not be used
                // we may as well try afresh
            }
            if error._code == PSTCKErrorCode.PSTCKConflictError.rawValue{
                // another transaction is currently being
                // processed by the SDK... please wait
            }
            if let errorDict = (error._userInfo as! NSDictionary?){
                if let errorString = errorDict.value(forKeyPath: "com.paystack.lib:ErrorMessageKey") as! String? {
                    if let reference=reference {
                        print("\(reference) : \(errorString)")
                        AlertUtils.shared.showMessage(viewController: self, title: nil, message: errorString)
                    } else {
                        print(errorString)
                        AlertUtils.shared.showMessage(viewController: self, title: nil, message: errorString)
                    }
                }
            }
            //            self.chargeCardButton.isEnabled = true;
        }, didRequestValidation: { (reference) in
            print("requested validation: \(reference)")
        }, willPresentDialog: {
            // make sure dialog can show
            // if using a "processing" dialog, please hide it
            print("will show a dialog")
        }, dismissedDialog: {
            // if using a processing dialog, please make it visible again
            print("dismissed dialog")
        }) { (reference) in
            print("succeeded: \(reference)")
            //            self.chargeCardButton.isEnabled = true;
            self.verifyTransaction(referenceCode: reference)
        }
        return
    }
    
    private func verifyTransaction(referenceCode: String) {
        guard let purchaseType = purchaseType else { return }
        switch purchaseType {
        case .ticket:
            viewModel.requestVerifyEventTicketTransaction(referenceCode: referenceCode) {[weak self] in
                self?.dismiss(animated: true, completion: nil)
                self?.successClousure?()
            }
        case .table:
            viewModel.requestVerifyEventTableTransaction(referenceCode: referenceCode) {[weak self] in
                self?.dismiss(animated: true, completion: nil)
                self?.successClousure?()
            }
        default:
            break
        }
    }
}
