//
//  EventOptionView.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/19/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class EventOptionView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var purchaseTypeLabel: UILabel!
    @IBOutlet weak var purchaseTypeImage: UIImageView!
    @IBOutlet weak var regularPriceLabel: UILabel!
    @IBOutlet weak var regularImage: UIImageView!
    @IBOutlet weak var vipImage: UIImageView!
    @IBOutlet weak var vipPriceLabel: UILabel!
    @IBOutlet weak var coverView: UIView!
    
    var viewModel: TicketAndTableViewModel!
    var type: EventPuchaseType!
    var purchaseTypeTapped: (()->())?
    var priceTypeTapped: (()->())?
    var disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("EventOptionView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        coverView?.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        viewModel
            .purchaseType
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (currentType) in
                guard let `self` = self else { return }
                switch currentType {
                case self.type:
                    self.purchaseTypeImage.image = #imageLiteral(resourceName: "selectedOptionIcon")
                    self.coverView.isHidden = true
                case .none:
                    self.purchaseTypeImage.image = #imageLiteral(resourceName: "unselectOptionIcon")
                    self.viewModel.priceType.value = .none
                    self.coverView.isHidden = true
                default:
                    self.purchaseTypeImage.image = #imageLiteral(resourceName: "unselectOptionIcon")
                    self.coverView.isHidden = false
                }
            }).disposed(by: disposeBag)
        viewModel
            .priceType
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (currentPriceType) in
                guard let `self` = self else { return }
                switch currentPriceType {
                case .regular:
                    if self.viewModel.purchaseType.value != self.type { break }
                    self.regularImage.image = #imageLiteral(resourceName: "selectedOptionIcon")
                    self.vipImage.image = #imageLiteral(resourceName: "unselectOptionIcon")
                    self.viewModel.price.value = self.regularPriceLabel.text ?? ""
                case .vip:
                    if self.viewModel.purchaseType.value != self.type { break }
                    self.regularImage.image = #imageLiteral(resourceName: "unselectOptionIcon")
                    self.vipImage.image = #imageLiteral(resourceName: "selectedOptionIcon")
                    self.viewModel.price.value = self.vipPriceLabel.text ?? ""
                case .none:
                    self.regularImage.image = #imageLiteral(resourceName: "unselectOptionIcon")
                    self.vipImage.image = #imageLiteral(resourceName: "unselectOptionIcon")
                    self.viewModel.price.value = ""
                }
            }).disposed(by: disposeBag)
    }
    
    @IBAction func onPurchaseTypeButton(_ sender: UIButton) {
        if viewModel.purchaseType.value == type {
            viewModel.purchaseType.value = .none
        } else {
            viewModel.purchaseType.value = type
        }
    }
    @IBAction func onRegularButton(_ sender: UIButton) {
        if viewModel.purchaseType.value != type {
            viewModel.purchaseType.value = type
        }
        viewModel.priceType.value = .regular
    }
    @IBAction func onVipButton(_ sender: UIButton) {
        if viewModel.purchaseType.value != type {
            viewModel.purchaseType.value = type
        }
        viewModel.priceType.value = .vip
    }
    
}
