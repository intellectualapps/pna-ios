//
//  ProfileViewController.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import DropDown
import Foundation

class ProfileViewController: BaseViewController {
    var viewModel = ProfileViewModel()
    //MARK: UI elements
    @IBOutlet weak var jobTitleTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var industryButton: UIButton!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var stateButton: UIButton!
    @IBOutlet weak var updateProfileButton: UIButton!
    //MARK: Local variables
    let industryDropDown = DropDown()
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    var is_first_regist = !Defaults.isNotFirstTime.getBool()
    var disposed         = DisposeBag()

    //MARK: UI view
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let user = AppDelegate.shared().loggedUser {
            viewModel.fillFromUser(user: user)
        }
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(!canAddLeftButton(), animated: false)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func canAddLeftButton() -> Bool {
        if let navi = self.navigationController {
            if navi.isKind(of: CustomNavigationController.self) == false {
                return true
            }
        }
        return false
    }

    override func addLeftButton() {
        if canAddLeftButton() {
            super.addLeftButton()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setup view
    func industrySetup() {
        let path = Path.industryLookup.path
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        self.showProgress()
        service.doExecute({[weak self] (result, error) in
            self?.hideProgress()

            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let industryResult = result as? [String: Any] {
                    if let indDicts = industryResult["industries"] as? [[String: Any]] {
                        var temp = [IndustryInfo]()
                        for dict in indDicts {
                            let info = IndustryInfo(dict: dict)
                            temp.append(info)
                        }
                        self?.viewModel.industies.value = temp
                        self?.didReceiveIndustries()
                    }
                }
            }
            
        })
    }
    
    func didReceiveIndustries() {
        self.makeIndustrySelection()
    }
    
    func countrySetup() {
        let path = Path.countryLookup.path
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        
        service.doExecute({[weak self] (result, error) in
            self?.hideProgress()
            
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let countryResult = result as? [String: Any] {
                    if let cntyDict = countryResult["countries"] as? [[String: Any]] {
                        var temp = [CountryInfo]()
                        for dict in cntyDict {
                            let info = CountryInfo(dict: dict)
                            temp.append(info)
                        }
                        self?.viewModel.countries.value = temp

                        self?.didReceiveCountries()
                    }
                }
            }
            
        })
    }
    
    func didReceiveCountries() {
        self.makeCountrySelection()
    }
    
    func stateSetup(countryId: Int) {
        let path = Path.stateLookup.path + "/\(countryId)"
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        
        service.doExecute({[weak self] (result, error) in
            self?.hideProgress()
            
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let stateResult = result as? [String: Any] {
                    if let stateDict = stateResult["states"] as? [[String: Any]] {
                        var temp = [StateInfo]()
                        for dict in stateDict {
                            let info = StateInfo(dict: dict)
                            temp.append(info)
                        }
                        self?.viewModel.states.value = temp
                        self?.didReceiveStates(country: countryId)
                    }
                    
                }
            }
            
        })
    }
    func didReceiveStates(country: Int) {
        self.makeStateSelection(country: country)
        self.stateButton.isUserInteractionEnabled = true
    }
    
    func makeIndustrySelection() {
        self.industryDropDown.anchorView = self.industryButton
        self.industryDropDown.dataSource = viewModel.indestriesNames
        self.industryDropDown.bottomOffset = CGPoint(x: 0, y: self.industryButton?.bounds.height ?? 0)

        if self.viewModel.currentIndustryId > 0 {
            let selectedIndustryObj: [IndustryInfo] = viewModel.industies.value.filter {$0.id == self.viewModel.currentIndustryId }
            let currentIdx = viewModel.industies.value.index(of: selectedIndustryObj[0]) ?? -1
            if (currentIdx >= 0) {
                let title  = viewModel.indestriesNames[currentIdx]
                self.industryButton?.setTitle(title, for: .normal)
                self.industryButton?.setTitleColor(.black, for: .normal)
                self.viewModel.industry = currentIdx
            }
        }
        self.industryDropDown.selectionAction = {[weak self] (index, item) in
            self?.industryButton?.setTitle(item, for: .normal)
            self?.industryButton?.setTitleColor(.black, for: .normal)
            self?.viewModel.industry = index
        }
    }
    func makeCountrySelection() {
        self.countryDropDown.anchorView = self.countryButton
        self.countryDropDown.dataSource = viewModel.countrynames
        self.countryDropDown.bottomOffset = CGPoint(x: 0, y: self.countryButton?.bounds.height ?? 0)
        if self.viewModel.currentCountryId > 0 {
            let selectedCountryObj: [CountryInfo] = viewModel.countries.value.filter {$0.id == self.viewModel.currentCountryId }
            let currentIdx = viewModel.countries.value.index(of: selectedCountryObj[0]) ?? -1
            if (currentIdx >= 0) {
                let title  = viewModel.countrynames[currentIdx]
                self.countryButton?.setTitle(title, for: .normal)
                self.countryButton?.setTitleColor(.black, for: .normal)
                self.viewModel.country = currentIdx
                self.stateSetup(countryId: self.viewModel.currentCountryId)
            }
        }
        
        self.countryDropDown.selectionAction = { [weak self](index, item) in
            self?.countryButton?.setTitle(item, for: .normal)
            self?.viewModel.country = index
            self?.countryButton?.setTitleColor(.black, for: .normal)
            self?.stateButton?.setTitle("State", for: .normal)
            self?.stateButton?.titleLabel?.textColor = .black
            if let selectedCountryId = self?.viewModel.countries.value[index].id {                self?.stateSetup(countryId: selectedCountryId)
            }
        }
    }
    
    func makeStateSelection(country: Int) {
        self.stateDropDown.anchorView = self.stateButton
        self.stateDropDown.dataSource = viewModel.stateNames
        self.stateDropDown.bottomOffset = CGPoint(x: 0, y: self.stateButton?.bounds.height ?? 0)
        if self.viewModel.currentCountryId == country {
            let selectedStateObj: [StateInfo] = viewModel.states.value.filter {$0.id == self.viewModel.currentStateId }
            if let first = selectedStateObj.first {
                let currentIdx = viewModel.states.value.index(of: first) ?? -1
                if (currentIdx >= 0) {
                    let title  = viewModel.stateNames[currentIdx]
                    self.stateButton?.setTitle(title, for: .normal)
                    self.stateButton?.setTitleColor(.black, for: .normal)
                    self.viewModel.state = currentIdx
                }
            }
        }
        
        self.stateDropDown.selectionAction = { [weak self] (index, item) in
            self?.stateButton?.setTitle(item, for: .normal)
            self?.stateButton?.setTitleColor(.black, for: .normal)
            self?.viewModel.state = index
        }
    }

    func setupView() {
        jobTitleTextField.text = self.viewModel.jobTitle
        companyTextField.text = self.viewModel.company
        self.stateButton.isUserInteractionEnabled = false;
        
        jobTitleTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.jobTitle = value
                }
            })
            .disposed(by: disposed)
        
        companyTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.company = value
                }
            })
            .disposed(by: disposed)
        
        industrySetup()
        countrySetup()
        
        if !is_first_regist {
            self.updateProfileButton.setTitle("Update Profile", for: .normal)
        }
    }
    
    //MARK: Action
    @IBAction func continueAction(_ sender: Any) {
        UIViewController.topMostViewController()?.showProgress()
        let errMsg = self.viewModel.errorMessage()
        if errMsg.count > 0 {
            Alert.showAlertWithErrorMessage(errMsg)
            return
        }
        self.viewModel.updateProfie (completion: {[weak self] (result, error) in

            UIViewController.topMostViewController()?.hideProgress()
            guard let `self`  = self else {
                return
            }
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            }else{
                if self.is_first_regist {
                    self.showNnetworkSuggestionScreen()
                }
                else {
                    self.showHomeScreen()
                }
            }
        })
    }
    
    @IBAction func industryAction(_ sender: Any) {
        industryDropDown.show()
    }
    
    @IBAction func countryAction(_ sender: Any) {
        countryDropDown.show()
    }
    
    @IBAction func stateAction(_ sender: Any) {
        stateDropDown.show()
    }
    
    func showNnetworkSuggestionScreen() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard.init(name: "Interest", bundle: nil)
            if let controller = storyboard.instantiateInitialViewController() {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    override func backAction() {
        showHomeScreen()
    }
}
