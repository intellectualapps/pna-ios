//
//  SlimProfileViewController.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import SDWebImage

class SlimProfileViewController: BaseViewController {
    var suggestViewModel: NetworkSuggestionViewModel?
    // MAKR: UI Elements
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var addNetworkButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    //MARK: Local variables
    var suggestionUser: SuggestionUser?
    //MARK: UI View
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadUserToView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: Action
    @IBAction func addNetworkAction(_ sender: Any) {
        if let user = suggestionUser {
            user.isAdded = true
            self.suggestViewModel?.addUser(user: user)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func loadUserToView() {
        guard let user = suggestionUser else {
            return
        }
        if let url = URL(string: user.avatarPath) {
            avatarImageView?.sd_setImage(with: url, completed: nil)
        }
        usernameLabel?.text = user.fullname
        jobTitleLabel?.text = user.jobTitle
        companyLabel?.text = user.company
        if user.isAdded {
            setPendingStatus()
        }
    }
    
    func setPendingStatus() {
        addNetworkButton?.setTitle("Invitation pending", for: .normal)
        addNetworkButton?.setBackgroundImage(nil, for: .normal)
        addNetworkButton?.setTitleColor(UIColor.init(hex: "B3B3B3"), for: .normal)
        addNetworkButton?.isEnabled = false
    }
}

// MARK: - extension tableviewDatasource
extension SlimProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

// MARK: - extension tableviewDelegate
extension SlimProfileViewController: UITableViewDelegate {
    
}
