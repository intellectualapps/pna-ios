//
//  WelcomeScreenTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/27/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class WelcomeScreenTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var youtubeIcon: UIButton!
    var imageLoadCompletion: ((CGFloat, UITableViewCell)->())?
    
    //MARK: - Local Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        coverImageView?.image = UIImage.init(named: "image02")
        setup()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        setup()
    }

    func setup() {
        coverImageView?.isHidden = false
        youtubeIcon?.isHidden = true
    }

    func config(title: String?,
                imageUrl: String?,
                isVideo: Bool = false) {
        titleLabel?.text = title
        coverImageView?.isHidden = isVideo
        youtubeIcon?.isHidden = !isVideo
        if let url = URL(string: imageUrl ?? "") {
            coverImageView.sd_setImage(with: url, placeholderImage: UIImage(), options: .continueInBackground, completed: { [weak self] (image, error, cacheType, imageURL) in
                guard let `self` = self else { return }
                if let image = image {
                    let ratio = image.size.width / image.size.height
                    let imageHeight = self.frame.width / ratio
                    if let photoImageLoadComplete = self.imageLoadCompletion {
                        photoImageLoadComplete(imageHeight, self)
                    }
                }
            })
        }
    }

}
