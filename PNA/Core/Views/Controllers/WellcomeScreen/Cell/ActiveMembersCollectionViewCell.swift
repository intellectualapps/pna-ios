//
//  ActiveMembersCollectionViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 7/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class ActiveMembersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.cornerRadius = 3
        layer.cornerRadius = 3
    }
    
}
