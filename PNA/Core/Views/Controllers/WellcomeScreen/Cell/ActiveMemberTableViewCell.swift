//
//  ActiveMemberTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 7/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class ActiveMemberTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var users: [WelcomeUser] = [] {
        didSet {
            collectionView?.reloadData()
        }
    }
    var delegate: ActiveMemberTableViewCellDelegate?
    var inset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.registerCell(ActiveMembersCollectionViewCell.self)
        collectionView?.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = inset
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 3
        collectionView?.collectionViewLayout = layout
    }
    
    deinit {
        collectionView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            let cellHeight = collectionView.contentSize.height + 35
            delegate?.collectionViewDidChangeContentSize(cellHeight: cellHeight)
            print(collectionView.contentSize)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(users: [WelcomeUser]) {
        self.users = users
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActiveMembersCollectionViewCell", for: indexPath)
            as! ActiveMembersCollectionViewCell
        let user = users[indexPath.row]
        if let url = URL(string: user.pic ?? "") {
            cell.imageView.sd_setImage(with: url, placeholderImage: nil, options: .cacheMemoryOnly, completed: nil)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let edge = (self.collectionView.bounds.width - inset.left * 5) / 4
        return CGSize(width: edge, height: edge)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let user = users[indexPath.row]
        delegate?.didTapOnCollectionViewCell(user: user)
    }

}

protocol ActiveMemberTableViewCellDelegate: class {
    func collectionViewDidChangeContentSize(cellHeight: CGFloat)
    func didTapOnCollectionViewCell(user: WelcomeUser)
}
