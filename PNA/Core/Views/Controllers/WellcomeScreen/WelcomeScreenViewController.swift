//
//  WelcomeScreenViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/27/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class WelcomeScreenViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logoView: UIView!
    
    //MARK: - Local Variables:
    fileprivate let NORMAL_CELL_ID = "WelcomeScreenTableViewCell"
    fileprivate let OURMEMBER_CELL_ID = "ActiveMemberTableViewCell"
    var viewModel = WelcomeViewModel()
    var eventImageHeight: CGFloat = 200
    var offerImageHeight: CGFloat = 200
    var ourMembersCellHeight: CGFloat = 200 {
        didSet {
            DispatchQueue.main.async {
                let rowIndex = SectionID.ourMembers.rawValue - 1
                if rowIndex >= 0 && rowIndex < 5 {
                    self.tableView.reloadRows(at: [IndexPath(row: rowIndex, section: 0)], with: .none)
                }
            }
        }
    }
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getWelcomeSections()
    }

    func reload() {
        DispatchQueue.main.async {
            self.tableView?.reloadData()
        }
    }
    override func setupObservable() {
        viewModel.sections.asObservable()
            .subscribe(onNext: {[weak self] (infos) in
                self?.reload()
            }).disposed(by: rx.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        Defaults.isNotFirstTime.set(value: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func setupUI() {
        tableView.registerCell(WelcomeScreenTableViewCell.self)
        tableView.registerCell(ActiveMemberTableViewCell.self)
        self.view.addSubview(logoView)
        logoView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 170)
        tableView.tableHeaderView = logoView
        tableView.tableFooterView = nil
        tableView.estimatedRowHeight = 0
        removeNavigationBarItem()
        setNavigationBarItem()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: - Actions
    @IBAction func onMenuButton(_ sender: UIButton) {
        menuAction()
    }
    
    @IBAction func unwindFromEventPaymentStatusVCToWelcome(_ segue: UIStoryboardSegue) {
        print("payment success")
    }
}

extension WelcomeScreenViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.value.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sInfo = viewModel.sections.value[section]
        return sInfo.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sInfo = viewModel.sections.value[indexPath.section]
        let rowInfo = sInfo.cells[indexPath.row]
        let identifier = rowInfo.identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        if let memberCell = cell as? ActiveMemberTableViewCell {
            memberCell.selectionStyle = .none
            memberCell.config(users: viewModel.ourMembers.value)
            memberCell.delegate = self
        }
        if let normalCell = cell as? WelcomeScreenTableViewCell {
            normalCell.selectionStyle = .none
            if let content = rowInfo.content {
                if let pnaDaily = content as? PNADaily {
                    normalCell.config(title: pnaDaily.title,
                                      imageUrl: pnaDaily.imageUrl)
                } else if let event = content as? Event {
                    normalCell.config(title: event.title,
                                      imageUrl: event.imageWelcomeUrl)
                    normalCell.imageLoadCompletion = {[weak self] (height, cell) in
                        self?.eventImageHeight = height
                    }
                } else if let offer = content as? Offer {
                    normalCell.config(title: offer.title,
                                      imageUrl: offer.imageWelcomeUrl)
                    normalCell.imageLoadCompletion = {[weak self] (height, cell) in
                        self?.offerImageHeight = height
                    }
                } else if let video = content as? Video {
                    normalCell.config(title: video.title,
                                      imageUrl: video.imageUrl,
                                      isVideo: true)
                }
            }
        }
        return cell
    }
}

extension WelcomeScreenViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sInfo = viewModel.sections.value[indexPath.section]
        let rowInfo = sInfo.cells[indexPath.row]
        if let content = rowInfo.content {
            if let _ = content as? PNADaily {
                pushToPNADaily()
            } else if let _ = content as? Event {
                pushToEventDetail()
            } else if let _ = content as? Offer {
                pushToOfferDetail()
            } else if let _ = content as? Video {
                playVideo()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sInfo = viewModel.sections.value[indexPath.section]
        let rowInfo = sInfo.cells[indexPath.row]
        if rowInfo.cellId == SectionID.ourMembers.rawValue {
            return ourMembersCellHeight
        }
        return 250
    }
    
    private func pushToEventDetail() {
        let eventDetailVC = UIStoryboard.instantiate(EventDetailViewController.self, storyboardType: .events)
        viewModel.getEventDetail {[weak self] (event) in
            guard let event = event else { return }
            eventDetailVC.event = event
            eventDetailVC.event?.imageHeight = self?.eventImageHeight ?? 200
            self?.navigationController?.pushViewController(eventDetailVC, animated: true)
        }
    }
    
    private func pushToOfferDetail() {
        let offerDetailVC = UIStoryboard.instantiate(DiscountsDetailTableViewController.self, storyboardType: .discounts)
        viewModel.getOfferDetail { [weak self] (offer) in
            guard let offer = offer else { return }
            offerDetailVC.offer = offer
            offerDetailVC.offer?.imageHeight = self?.offerImageHeight ?? 200
            self?.navigationController?.pushViewController(offerDetailVC, animated: true)
        }
    }
    
    private func pushToPNADaily() {
        let pnaDailyVC = UIStoryboard.instantiate(PNADailyViewController.self, storyboardType: .pnaDaily)
        pnaDailyVC.isMenuAction = false
        navigationController?.pushViewController(pnaDailyVC, animated: true)
    }
    
    private func playVideo() {
        if let url = URL(string: viewModel.video.value.videoUrl ?? "") {
            let playerVC = MobilePlayerViewController(contentURL: url)
            playerVC.title = viewModel.video.value.title ?? ""
            playerVC.activityItems = [url]
            present(playerVC, animated: true) {
                playerVC.play()
            }
        }
    }
}

extension WelcomeScreenViewController: ActiveMemberTableViewCellDelegate {
    func collectionViewDidChangeContentSize(cellHeight: CGFloat) {

        if ourMembersCellHeight != cellHeight {
            ourMembersCellHeight = cellHeight
        }
    }
    
    func didTapOnCollectionViewCell(user: WelcomeUser) {
        let vc = UIStoryboard.instantiate(SearchAddMemberViewController.self, storyboardType: .searchMember)
        viewModel.getProfile(id: user.memberId) {[weak self] (user) in
            vc.user = SearchProfile()
            vc.user?.pic = user?.pic
            vc.user?.networkStatus = user?.networkStatus
            vc.user?.id = user?.id
            vc.user?.jobTitle = user?.jobTitle
            vc.user?.company = user?.company
            vc.user?.firstName = user?.firstName
            vc.user?.lastName = user?.lastName
            vc.searchViewModel = SearchMemberViewModel()
            self?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
