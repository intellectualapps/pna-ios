//
//  Config.swift
//  PNA
//
//  Created by center12 on 1/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

struct MenuItem {
    var icon = ""
    var name = ""
    
    init(icon: String, name: String) {
        self.icon = icon
        self.name = name
    }
}

enum Menu {
    case Home
    case Feed
    case Profile
    case Network
    case Events
    case PNADaily
    case PNAYellowPages
    case PNADiscounts
    case PNABussines
    case PNATreavelClub
    case About
    case Logout
    
    var value: MenuItem {
        switch self {
        case .Profile:
            return MenuItem(icon: "ic_profile", name: "Profile")
        case .Network:
            return MenuItem(icon: "ic_network", name: "Network")
        case .Events:
            return MenuItem(icon: "ic_events", name: "Events")
        case .PNADaily:
            return MenuItem(icon: "ic_pna_daily", name: "PNA Daily")
        case .PNAYellowPages:
            return MenuItem(icon: "ic_yellow_pages", name: "Yellow Pages")
        case .PNADiscounts:
            return MenuItem(icon: "ic_pna_discounts", name: "PNA Discounts")
        case .PNABussines:
            return MenuItem(icon: "ic_pna_business", name: "PNA Business")
        case .PNATreavelClub:
            return MenuItem(icon: "ic_pna_travel_club.png", name: "PNA Travel Club")
        case .About:
            return MenuItem(icon: "ic_about", name: "About")
        case .Logout:
            return MenuItem(icon: "Logout", name: "Logout")
        case .Home:
            return MenuItem(icon: "house-outline", name: "Home")
        case .Feed:
            return MenuItem(icon: "", name: "Feed")
        }
    }
}
