//
//  LeftMenuViewController.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import Material
import SlideMenuControllerSwift

class LeftMenuViewController: BaseViewController {
    //MAKR: UI Elements
    @IBOutlet weak var profileHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var banerImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var menuTableView: UITableView! {
        didSet {
            menuTableView?.delegate = self
            menuTableView?.dataSource = self
        }
    }
    
    //MARK: Local variables
    var menus:[Menu] = [Menu]()
    //mennu
    var mainViewController: UIViewController!
    var feedViewController: UIViewController!
    var profileViewController: UIViewController!
    var networkViewController: UIViewController!
    var navDiscountsController: UIViewController!
    var eventsViewController: UIViewController!
    var yellowPagesViewController: UIViewController!
    var welcomeScreenViewController: UIViewController!
    var pnaDailyViewController: UIViewController!
    var profileInUpdateViewController: UIViewController!
    
    //MARK: UI view
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupData()
        setupView()
        setupMenuItem()
        menuTableView?.contentInset = UIEdgeInsets(top: 32,
                                                   left: 0,
                                                   bottom: 0,
                                                   right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let urlString = AppDelegate.shared().loggedUser?.pic,
            let url = URL(string: urlString) {
            avatarImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "avatar_default"), options: .cacheMemoryOnly, completed: nil)
        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
    }
    
    // MARK: setup view
    func setupView() {
        avatarImage?.roundCorner(radius: 73, borderColor: UIColor.clear, borderWidth: 0)
        view?.backgroundColor = UIColor.init(rgb: 0x272e31)
        avatarImage?.layer.masksToBounds = true
        menuTableView?.backgroundColor = UIColor.init(rgb: 0x272e31)
    }
    
    func setupMenuItem() {
        self.profileViewController = getNavController(storyboardName: "UpdateProfile", identifier: "UpdateProfileViewController")
        self.mainViewController = getNavController(storyboardName: "Feed", identifier: "home")
        self.networkViewController = getNavController(storyboardName: "Network", identifier: "NetworkViewController")
        self.navDiscountsController = getNavController(storyboardName: "Discounts", identifier: "DiscountsHomeViewController")
        self.eventsViewController = getNavController(storyboardName: "Events", identifier: "EventsViewController")
        self.yellowPagesViewController = getNavController(storyboardName: "YellowPages", identifier: "YellowPagesViewController")
        self.welcomeScreenViewController = getNavController(storyboardName: "WelcomeScreen", identifier: "WelcomeScreenViewController")
        self.pnaDailyViewController = getNavController(storyboardName: "PNADaily", identifier: "PNADailyViewController")
        let profileVC = UIStoryboard.instantiate(StickyHeaderViewController.self, storyboardType: .profileDetail)
        object_setClass(profileVC, ProfileInUpdateViewController.self)
        if let profileInUpdateVC = profileVC as? ProfileInUpdateViewController {
            self.profileInUpdateViewController = UINavigationController(rootViewController: profileInUpdateVC)
        }
    }
    
    func getNavController(storyboardName: String, identifier: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier)
        return UINavigationController(rootViewController: viewController)
    }
    
    @IBAction func avatarButtonAction(_ sender: UIButton) {
        if let index = menus.index(of: Menu.Profile) {
            menuTableView?.selectRow(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .none)
            gotoUpdateProfile()
        }
    }
    
    @IBAction func showAvatarImage(_ sender: UIButton) {
        let avatarLargeImageView = AvatarImageView()
        avatarLargeImageView.imageUrl = AppDelegate.shared().loggedUser?.pic ?? ""
        present(avatarLargeImageView, animated: true, completion: nil)
    }
    
    func gotoUpdateProfile() {
        self.slideMenuController()?.changeMainViewController(self.profileInUpdateViewController, close: true)
    }
    
    func changeViewController(_ menu: Menu) {
        switch menu {
        case .Feed:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .Profile:
            self.slideMenuController()?.changeMainViewController(self.profileInUpdateViewController, close: true)
        case .Network:
            self.slideMenuController()?.changeMainViewController(self.networkViewController, close: true)
        case .PNADiscounts:
            self.slideMenuController()?.changeMainViewController(self.navDiscountsController, close: true)
        case .PNAYellowPages:
            self.slideMenuController()?.changeMainViewController(self.yellowPagesViewController, close: true)
        case .PNABussines:
            let webController = WebViewVC()
            webController.titleStr = Menu.PNABussines.value.name
            let navi = UINavigationController(rootViewController: webController)
            self.slideMenuController()?.changeMainViewController(navi, close: true)
            break
        case .PNATreavelClub:
            let webController = WebViewVC()
            webController.titleStr = Menu.PNATreavelClub.value.name
            let navi = UINavigationController(rootViewController: webController)
            self.slideMenuController()?.changeMainViewController(navi, close: true)
            break
        case .Events:
            self.slideMenuController()?.changeMainViewController(self.eventsViewController, close: true)
        case .Logout:
            AppDelegate.shared().showCustomScreen("Login")
            AppDelegate.shared().loggedUser = nil
            UserDefaults.standard.set(nil, forKey: "user")
            UserDefaults.standard.synchronize()
        case .Home:
            self.slideMenuController()?.changeMainViewController(self.welcomeScreenViewController, close: true)
        case .PNADaily:
            self.slideMenuController()?.changeMainViewController(self.pnaDailyViewController, close: true)
        default:
//            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
            break
        }
    }
    
    func setupData() {
        let data: [Menu] = [
            .Home,
//            .Feed,
            .Profile,
            .Network,
            .Events,
            .PNADaily,
            .PNAYellowPages,
            .PNADiscounts,
            .PNATreavelClub,
            .PNABussines,
            .About,
            .Logout
        ]
        
        for item in data {
            menus.append(item)
        }
        
        if let user = AppDelegate.shared().loggedUser {
            infoLabel.text = user.jobTitle
            usernameLabel?.text = user.fullName?.capitalized
            usernameLabel?.font = UIFont.latoFont(size: 23, type: .regular)
            // XXX: Need set avatar image later
        }
        
    }
    
    func showCreateProfileScreen(is_first_regist:Bool = false ) {
        DispatchQueue.main.async {
            let controller = UIStoryboard.instantiate(ProfileViewController.self, storyboardType: .createProfile)
            let navi = UINavigationController(rootViewController: controller)
            controller.is_first_regist = is_first_regist
            UIViewController.topMostViewController()?.present(navi, animated: true, completion: nil)
        }
    }
    
    func showNetworkScreen() {
        DispatchQueue.main.async {
            let controller = UIStoryboard.instantiate(NetworkViewController.self, storyboardType: .network)
            let navi = UINavigationController(rootViewController: controller)
            UIViewController.topMostViewController()?.present(navi, animated: true, completion: nil)
        }
    }

    //MARK: Action    
    
}

// MARK: extension uitableviewDelegate
extension LeftMenuViewController: UITableViewDelegate {
    
}

//MARK: extension uitableviewDatasource
extension LeftMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuViewCell.identifier, for: indexPath)
        if let menuCell = cell as? MenuViewCell {
            menuCell.rowData = menus[indexPath.row].value
            return menuCell
        }
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        if let acell = cell as? MenuViewCell {
            acell.menuNameLabel?.textColor = UIColor.init(hex: "c4c6c6", alpha: 1)
        }
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.backgroundView?.backgroundColor = UIColor.init(rgb: 0x272e31)
        (view as? UITableViewHeaderFooterView)?.contentView.backgroundColor = UIColor.init(rgb: 0x272e31)
    }

    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.backgroundView?.backgroundColor = UIColor.init(rgb: 0x272e31)
        (view as? UITableViewHeaderFooterView)?.contentView.backgroundColor = UIColor.init(rgb: 0x272e31)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let heightDefault:CGFloat = 53
        let item = CGFloat(menus.count)
        let tableHeight = Int(tableView.frame.height / item)
        let height = (tableHeight <= 53) ? CGFloat(tableHeight) : heightDefault
        return heightDefault
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < menus.count {
            let menu = menus[indexPath.row]
            self.changeViewController(menu)
        }
    }
}

