//
//  MenuViewCell.swift
//  PNA
//
//  Created by center12 on 1/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class MenuViewCell: UITableViewCell {
    static var identifier = "MenuCell"
    
    //MARK: UI Elements
    @IBOutlet weak var menuNameLabel: UILabel!
    
    @IBOutlet weak var btnIcon: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
    
    //MARK: Local variables
    var rowData: MenuItem? {
        didSet {
            guard let item = self.rowData else { return }
            btnIcon?.setImage(UIImage(named: item.icon), for: .normal)
            menuNameLabel?.text = item.name
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        btnIcon?.setImage(nil, for: .normal)
    }
    
}
