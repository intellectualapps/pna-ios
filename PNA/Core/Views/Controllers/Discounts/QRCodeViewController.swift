//
//  QRCodeViewController.swift
//  PNA
//
//  Created by hien.bui on 2/22/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader

private struct Constant {
    struct SegueIdentifier {
        static let unwindToDiscountsHomeVC = "unwindFromQrCodeVCToDiscountsHomeVC"
        static let successScanToReveal = "successScanToReveal"
    }
}

class QRCodeViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var previewView: QRCodeReaderView! {
        didSet {
            previewView.setupComponents(showCancelButton: false,
                                    showSwitchCameraButton: false,
                                    showTorchButton: false,
                                    showOverlayView: true,
                                    reader: reader)
        }
    }
    
    // MARK: - Local variables
    lazy var reader: QRCodeReader = QRCodeReader()
    var offer: Offer?
    var viewModel = QRCodeViewModel()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard checkScanPermissions(), !reader.isRunning else {
            return
        }
        reader.didFindCode = { [weak self] result in
            let scannedToken = result.value.trimmingCharacters(in: .whitespaces)
            self?.viewModel.requestAPI(scannedToken: scannedToken, offer: self!.offer!, completion: { success in
                if success {
//                    self?.performSegue(withIdentifier: Constant.SegueIdentifier.unwindToDiscountsHomeVC, sender: self)
                    self?.performSegue(withIdentifier: Constant.SegueIdentifier.successScanToReveal, sender: self)
                } else {
//                    self?.performSegue(withIdentifier: Constant.SegueIdentifier.unwindToDiscountsHomeVC, sender: self)
                    self?.reader.startScanning()
                }
            })
        }
        reader.startScanning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.SegueIdentifier.successScanToReveal {
            if let revealVC = segue.destination as? RevealMemberCardViewController {
                revealVC.offer = offer
            }
        }
    }
    
    override func backAction() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Actions
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController?
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert?.addAction(UIAlertAction(title: "Setting", style: .default, handler: {  (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert?.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            case -11814:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert?.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            default:
                alert = nil
            }
            
            guard let vc = alert else { return false }
            
            present(vc, animated: true, completion: nil)
            
            return false
        }
    }
}
