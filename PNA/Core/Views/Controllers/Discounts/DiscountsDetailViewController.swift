//
//  DiscountsDetailViewController.swift
//  PNA
//
//  Created by hien.bui on 2/21/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class DiscountsDetailViewController: BaseViewController {
    // MARK: - Outlets
    @IBOutlet private weak var logoImageView: UIImageView?
    @IBOutlet private weak var businessNameLabel: UILabel?
    @IBOutlet private weak var favoriteButton: UIButton?
    @IBOutlet private weak var photoImageView: UIImageView?
    @IBOutlet private weak var discountTypeLabel: UILabel?
    @IBOutlet private weak var levelLabel: UILabel?
    @IBOutlet private weak var endsLabel: UILabel?
    @IBOutlet private weak var redeemendLabel: UILabel?
    @IBOutlet private weak var redeemAtLabel: UILabel?
    @IBOutlet private weak var heightConstraintPremiumMemberButton: NSLayoutConstraint!
    @IBOutlet private weak var redeemOfferButton: UIButton?
    
    @IBOutlet private weak var fakeDataLabel: UILabel!
    
    var offer: Offer?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hard-code feedback customer
        heightConstraintPremiumMemberButton.constant = 0
    }
    
    override func setupUI() {
        self.title = "PNA Discounts"
        guard let offer = offer else {
            return
        }
        businessNameLabel?.text = offer.businessName
        fakeDataLabel.text = offer.businessName?[0]
        favoriteButton?.isHidden = !(offer.offerLevel == "Premium")
        if let photo = offer.photo {
            showPhotoImageView()
            let url = URL(string: photo)
            photoImageView?.sd_setImage(with: url, placeholderImage: nil)
        } else {
            hidePhotoImageView()
        }
        discountTypeLabel?.text = offer.type
        levelLabel?.text = offer.offerLevel
        endsLabel?.text = offer.stopsAt
        redeemendLabel?.text = "\(offer.numberOfTimesRedeemed ?? 0) times"
        redeemAtLabel?.text = offer.address
    }
    
    override func backAction() {
        navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let qrCodeViewController = segue.destination as? QRCodeViewController else {
            return
        }
        qrCodeViewController.offer = offer
    }

    private func showPhotoImageView() {
    }
    
    private func hidePhotoImageView() {
    }
}
