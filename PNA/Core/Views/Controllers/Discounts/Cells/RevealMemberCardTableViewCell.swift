//
//  RevealMemberCardTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 6/26/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class RevealMemberCardTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var memberTypeLabel: UILabel!
    @IBOutlet weak var discountTypeLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var businessNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var delegate: RevealMemberCardTableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func config(with offer: Offer) {
        guard let user = AppDelegate.shared().loggedUser else { return }
        imageHeightConstraint?.constant = 200
        if let url = URL(string: user.pic ?? "") {
            avatarImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "pna_africa_map"), options: .cacheMemoryOnly) {[weak self] (image, _, _, _) in
                guard let image = image else { return }
                let ratio = image.height / image.width
                let height = UIScreen.main.bounds.width * ratio
                self?.imageHeightConstraint?.constant = height
                self?.delegate?.imageDidReceaved()
            }
        }
        idLabel?.text = "\(user.id ?? 0)"
        nameLabel?.text = user.fullName
        memberTypeLabel?.text = (user.membershipType?.capitalizingFirstLetter() ?? "") + " Member"
        discountTypeLabel?.text = offer.type
        levelLabel?.text = offer.offerLevel
        businessNameLabel?.text = offer.businessName
        addressLabel?.text = offer.address
    }
    
    @IBAction func onCloseButton(_ sender: UIButton) {
        delegate?.onCloseButton()
    }
    
}

protocol RevealMemberCardTableViewCellProtocol {
    func onCloseButton()
    func imageDidReceaved()
}
