//
//  DiscountTableViewCell.swift
//  PNA
//
//  Created by hien.bui on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import SDWebImage

protocol DiscountTableViewCellDelegate: class {
    func discountTableViewCell(_ discountTableViewCell: DiscountTableViewCell, photoImageLoadComplete indexPath: IndexPath?)
}

class DiscountTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet private weak var logoImageView: UIImageView?
    @IBOutlet private weak var businessNameLabel: UILabel?
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel?
    @IBOutlet private weak var heightConstantPhotoImageView: NSLayoutConstraint!
    @IBOutlet private weak var fakeDataLabel: UILabel?
    @IBOutlet weak var logImageView: UIImageView!
    
    var photoImageLoadComplete: ((CGFloat, IndexPath?) -> ())?
    
    weak var delegate: DiscountTableViewCellDelegate?
    var viewModel: DiscountsViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func clear() {
        logoImageView?.image = nil
        businessNameLabel?.text = nil
        photoImageView?.image = nil
        descriptionLabel?.text = nil
        logImageView?.isHidden = true
    }
    
    func config(offer: Offer) {
        fakeDataLabel?.text = offer.businessName?[0]
        logoImageView?.image = nil
        if offer.offerLevel?.lowercased() == MembershipType.premium.rawValue {
            logImageView?.isHidden = false
        }
        businessNameLabel?.text = offer.businessName
            if let url = URL(string: offer.photo ?? "") {
                photoImageView.sd_setImage(with: url) { [weak self] (image, _, _, _) in
                    guard let `self` = self else { return }
                    if let image = image {
                        let ratio = image.size.width / image.size.height
                        self.heightConstantPhotoImageView.constant = UIScreen.main.bounds.width / ratio
                        if let indexPath = offer.indexPath {
                            self.viewModel?.offers[indexPath.row].imageHeight = self.heightConstantPhotoImageView.constant
                        }
                        self.delegate?.discountTableViewCell(self, photoImageLoadComplete: offer.indexPath)
                    }
                }
        }
    }

}
