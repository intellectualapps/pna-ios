//
//  DiscountsViewController.swift
//  PNA
//
//  Created by hien.bui on 2/21/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let showDiscountsDetail = "showDetail"
    }
}

class DiscountsHomeViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView?
    
    // MARK: - Local variables
    var viewModel = DiscountsViewModel()
    private var disposeBag = DisposeBag()
    fileprivate var offers = [Offer]()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        removeNavigationBarItem()
        setNavigationBarItem()
        requestFetchOffers()
        self.title = "PNA Discounts"
    }
   
    override func backAction() {
        self.menuAction()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hard-code feedback customer
    }
    
    override func setupUI() {
        tableView?.tableFooterView = UIView()
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 140
        tableView?.addPullToRefresh(actionHandler: {[weak self] in
            self?.requestFetchOffers()
        })
        tableView?.pullToRefreshView?.activityIndicatorViewStyle = .gray
        tableView?.pullToRefreshView?.arrowColor = .gray
        tableView?.pullToRefreshView?.titleLabel?.text = nil
        tableView?.pullToRefreshView?.subtitleLabel?.text = nil
    }
    
    func requestFetchOffers() {
        viewModel.getOffers { [weak self] (offers) in
            self?.offers = offers
            self?.tableView?.reloadData()
            self?.tableView?.pullToRefreshView.stopAnimating()
        }
    }
    
    // MARK: - Prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier ?? "" {
        case "showDetail":
            guard let discountsDetailViewController = segue.destination as? DiscountsDetailTableViewController else {
                return
            }
            guard let indexPathSelected = tableView?.indexPathForSelectedRow else {
                return
            }
            discountsDetailViewController.offer = viewModel.offers[indexPathSelected.row]
        default:
            break
        }
    }
    
    // MARK: - Unwind Segue
    @IBAction func unwindFromQrCodeVCToDiscountsHomeVC(_ segue: UIStoryboardSegue) {
        guard let _ = segue.source as? QRCodeViewController else {
            return
        }
        requestFetchOffers()
    }
    
    @IBAction func unwindFromPaymentStatusVCToDiscountsHomeVC(_ segue: UIStoryboardSegue) {
        DispatchQueue.main.async {[weak self] in
            self?.performSegue(withIdentifier: Constant.SegueIdentifier.showDiscountsDetail, sender: self)
        }
    }
}

// MARK: - UITableViewDataSource

extension DiscountsHomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(DiscountTableViewCell.self, indexPath: indexPath)
        offers[indexPath.row].indexPath = indexPath
        cell.viewModel = viewModel
        cell.config(offer: offers[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension DiscountsHomeViewController: DiscountTableViewCellDelegate {
    func discountTableViewCell(_ discountTableViewCell: DiscountTableViewCell, photoImageLoadComplete indexPath: IndexPath?) {
        guard let indexPath = indexPath else { return }
        tableView?.reloadRows(at: [indexPath], with: .fade)
    }
}

// MARK: - UITableViewDelegate

extension DiscountsHomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if offers[indexPath.row].offerLevel?.lowercased() == MembershipType.premium.rawValue {
            let premiumVC = PremiumEventViewController()
            premiumVC.name = offers[indexPath.row].businessName
            premiumVC.onProceedClousure = {[weak self] in
                let paymentVC = UIStoryboard.instantiate(PaymentSubscriptionViewController.self,
                                                         storyboardType: .payment)
                paymentVC.paymentType = .discount
                self?.navigationController?.pushViewController(paymentVC, animated: true)
            }
            present(premiumVC, animated: true, completion: nil)
        } else {
            performSegue(withIdentifier: Constant.SegueIdentifier.showDiscountsDetail, sender: self)
        }
    }
}
