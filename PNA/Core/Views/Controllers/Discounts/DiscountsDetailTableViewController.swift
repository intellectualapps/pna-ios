//
//  DiscountsDetailTableViewController.swift
//  PNA
//
//  Created by MTQ on 4/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

private struct Constant {
    struct SegueID {
        static let goToReveal = "goToReveal"
        static let goToScan = "goToScan"
    }
}

class DiscountsDetailTableViewController: UITableViewController {
    // MARK: - Outlets
    @IBOutlet private weak var logoImageView: UIImageView?
    @IBOutlet private weak var businessNameLabel: UILabel?
    @IBOutlet private weak var photoImageView: UIImageView?
    @IBOutlet private weak var discountTypeLabel: UILabel?
    @IBOutlet private weak var levelLabel: UILabel?
    @IBOutlet private weak var endsLabel: UILabel?
    @IBOutlet private weak var redeemendLabel: UILabel?
    @IBOutlet private weak var redeemAtLabel: UILabel?
    @IBOutlet private weak var heightConstraintPhotoImageView: NSLayoutConstraint!
    @IBOutlet private weak var redeemOfferButton: UIButton?
    @IBOutlet private weak var fakeDataLabel: UILabel!
    @IBOutlet weak var logImagView: UIImageView!
    
    var offer: Offer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        addLeftBarButtonWithImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hard-code feedback customer
        setColorForNavigationBar(color: .black)
    }
    
    func setupUI() {
        self.title = "PNA Discounts"
        guard var offer = offer else {
            return
        }
        if offer.offerLevel?.lowercased() == MembershipType.premium.rawValue {
            logImagView?.isHidden = false
        } else {
            logImagView?.isHidden = true
        }
        businessNameLabel?.text = offer.businessName
        fakeDataLabel.text = offer.businessName?[0]
        if let photo = offer.photo {
            showPhotoImageView()
            let url = URL(string: photo)
            photoImageView?.sd_setImage(with: url, placeholderImage: nil, options: .cacheMemoryOnly, completed: {[weak self] (image, _, _, _) in
                if offer.imageHeight != 200 { return }
                guard let image = image else { return }
                let ratio = image.height / image.width
                offer.imageHeight = UIScreen.main.bounds.width * ratio
                self?.offer?.imageHeight = UIScreen.main.bounds.width * ratio
                self?.heightConstraintPhotoImageView.constant = offer.imageHeight
                self?.tableView.reloadData()
            })
        } else {
            hidePhotoImageView()
        }
        discountTypeLabel?.text = offer.type
        levelLabel?.text = offer.offerLevel
        endsLabel?.text = offer.stopsAt
        redeemendLabel?.text = "\(offer.numberOfTimesRedeemed ?? 0) times"
        redeemAtLabel?.text = offer.address
        
        self.heightConstraintPhotoImageView.constant = offer.imageHeight
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case Constant.SegueID.goToReveal:
            if let revealVC = segue.destination as? RevealMemberCardViewController {
                revealVC.offer = self.offer
            }
        case Constant.SegueID.goToScan:
            guard let qrCodeViewController = segue.destination as? QRCodeViewController else {
                return
            }
            qrCodeViewController.offer = offer
        default:
            break
        }
    }
    
    @IBAction func unwindFromRevealVC(_ segue: UIStoryboardSegue) {
        print("unwind from reveal vc")
    }
    
    @objc func backAction() {
        navigationController?.popViewController(animated: true)
    }
    
    func addLeftBarButtonWithImage(tintColor: UIColor? = nil, btnSize: CGSize = .zero) {
        var size = btnSize
        if size.width == 0 {
            size.width = 20
            size.height = 20
        }
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        button.imageView?.contentMode = .scaleAspectFit
        if let img = UIImage(named: "back_btn") {
            button.setImage(img, for: .normal)
        }
        let iOSVersion = Int(UIDevice.current.systemVersion) ?? 0
        if iOSVersion < 11 {
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        button.addTarget(self, action: #selector(self.backAction), for: UIControlEvents.touchUpInside)
        button.tintColor = tintColor
        let leftBtn = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = leftBtn
    }
    
    private func showPhotoImageView() {
    }
    
    private func hidePhotoImageView() {
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400 + (offer?.imageHeight ?? 0)
    }
}
