//
//  RevealMemberCardViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 6/26/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct CellID {
        static let revealCell = "RevealMemberCardTableViewCell"
    }
    struct SegueID {
        static let unwind = "unwindToDetail"
    }
}

class RevealMemberCardViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var offer: Offer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Member Card"
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 200
    }
}

extension RevealMemberCardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellID.revealCell, for: indexPath) as! RevealMemberCardTableViewCell
        if let offer = self.offer {
            cell.config(with: offer)
        }
        cell.delegate = self
        return cell
    }
}

extension RevealMemberCardViewController: RevealMemberCardTableViewCellProtocol {
    func imageDidReceaved() {
        self.tableView?.reloadData()
    }
    
    func onCloseButton() {
        performSegue(withIdentifier: Constant.SegueID.unwind, sender: nil)
    }
}
