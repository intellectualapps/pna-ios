//
//  YellowPageFilterDetailViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/18/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class YellowPageFilterDetailViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var viewModel: YellowPagesViewModel?
    var filterType: YellowPageFilterType?
    var dataArray: [Model] = []
    var selectedIndex: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupUI() {
        guard let filterType = self.filterType else { return }
        switch filterType {
        case .state:
            viewModel?.getState()
            self.title = "States"
        case .category:
            viewModel?.getCategories()
            self.title = "Industries"
        }
    }
    
    override func setupObservable() {
        guard let filterType = self.filterType else { return }
        switch filterType {
        case .state:
            viewModel?
                .states
                .asObservable()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: {[weak self] (states) in
                    self?.dataArray = states
                    for state in (self?.viewModel?.selectedStates.value ?? []) {
                        if let index = states.index(of: state) {
                            self?.selectedIndex.append(index)
                        }
                    }
                    self?.tableView?.reloadData()
                }).disposed(by: rx.disposeBag)
        case .category:
            viewModel?
                .categories
                .asObservable()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: {[weak self] (categories) in
                    self?.dataArray = categories
                    for state in (self?.viewModel?.selectedCategories.value ?? []) {
                        if let index = categories.index(of: state) {
                            self?.selectedIndex.append(index)
                        }
                    }
                    self?.tableView?.reloadData()
                }).disposed(by: rx.disposeBag)   
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        var selected = [Model]()
        for index in selectedIndex {
            selected.append(dataArray[index])
        }
        guard let filterType = self.filterType else { return }
        switch filterType {
        case .state:
            if let states = selected as? [StateInfo] {
                viewModel?.selectedStates.value = states
            }
        case .category:
            if let categories = selected as? [YellowPageCategory] {
                viewModel?.selectedCategories.value = categories
            }
        }
    }
    
}

extension YellowPageFilterDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
        if let states = dataArray as? [StateInfo] {
            cell.nameLabel.text = states[indexPath.row].name
        } else if let categories = dataArray as? [YellowPageCategory] {
            cell.nameLabel.text = categories[indexPath.row].name
        }
        cell.selectedView.isHidden = !selectedIndex.contains(indexPath.row)
        return cell
    }
}

extension YellowPageFilterDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex.contains(indexPath.row) {
            guard let index = selectedIndex.index(of: indexPath.row) else { return }
            selectedIndex.remove(at: index)
        } else {
            selectedIndex.append(indexPath.row)
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
