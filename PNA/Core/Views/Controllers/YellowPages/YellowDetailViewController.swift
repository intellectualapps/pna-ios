//
//  YellowDetailViewController.swift
//  PNA
//
//  Created by bui.duy.hien on 3/19/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import GoogleMaps

class YellowDetailViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var tableView: UITableView?
    // MARK: - Local variables
    var yellowPageObject: YellowPageObject?
    fileprivate var location: Location?
    fileprivate var viewModel = YellowDetailViewModel()
    private var locationManager = CLLocationManager()
    fileprivate var rowHeight: CGFloat = 650.0
    fileprivate var heightTextView: CGFloat = 128.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = yellowPageObject?.businessName
        requestAPIGetYellowPageDetail()
    }
    
    override func setupUI() {
        self.tableView?.registerCell(YellowDetailTableViewCell.self)
    }
    
    // MARK: - Methods
    func getHeightLabel(text: String) -> CGFloat {
        let screenwidth = UIScreen.main.bounds.width
        let businessNameBoundSize = CGSize(width: screenwidth - 50, height: CGFloat.greatestFiniteMagnitude)
        let size = text.sizeWithBoundSize(boundSize: businessNameBoundSize, option: .usesLineFragmentOrigin, lineBreakMode: .byWordWrapping, font: UIFont.systemFont(ofSize: 17))
        return size.height
    }
    
    func requestAPIGetYellowPageDetail() {
        if let id = yellowPageObject?.id {
            viewModel.getYellowPage(withID: id, completion: { [weak self] (yellowPageObject) in
                guard let `self` = self else { return }
                self.yellowPageObject = yellowPageObject
                
                let textView = UITextView()
                textView.text = ""
                if yellowPageObject.businessAddress != "" {
                    textView.text.append(yellowPageObject.businessAddress)
                }
                if yellowPageObject.businessCategory != "" {
                    textView.text.append("\n\n\(yellowPageObject.businessCategory)")
                }
                if yellowPageObject.businessEmail != "" {
                    textView.text.append("\n\n\(yellowPageObject.businessEmail)")
                }
                if yellowPageObject.businessWebsite != "" {
                    textView.text.append("\n\n\(yellowPageObject.businessWebsite)")
                }
                if let businessOffers = yellowPageObject.businessOffers {
                    textView.text.append("\n")
                    for offer in businessOffers {
                        textView.text.append("\n\(offer)")
                    }
                }
                if let businessName = textView.text {
                    self.heightTextView = self.getHeightLabel(text: businessName)
                }
                
                self.rowHeight = (620-128) + self.heightTextView
                
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                }
                
                let address = yellowPageObject.businessAddress + "+" + yellowPageObject.businessState
                // Geocode With LocationManager
                let searchAddress = address.replacingOccurrences(of: " ", with: "+")
                LocationManager.sharedInstance.geocodeUsingGoogleAddressString(address: searchAddress as NSString, onGeocodingCompletionHandler: { (gecodeInfo, placemark, error) in
                    if let geoCodeInfo = gecodeInfo {
                        let lat = (geoCodeInfo["latitude"] as? Double) ?? 0
                        let long = (geoCodeInfo["longitude"] as? Double) ?? 0
                        self.location = Location(lat: lat, long: long)
                        DispatchQueue.main.async {
                            self.tableView?.reloadData()
                        }
                    } else if error != nil {
                        let lat: Double = 9.072264      //center of Nigeria : Abuja
                        let long: Double = 7.491302
                        self.location = Location(lat: lat, long: long)
                        DispatchQueue.main.async {
                            Alert.showAlertWithErrorMessage("\(error?.localized() ?? "")\nAuto focus to Abuja, Nigeria")
                            self.tableView?.reloadData()
                        }
                    }
                })
            })
        }
    }
    
    func callAction(_ button: UIButton) {
        var busPhone = button.currentTitle?.replacingOccurrences(of: "Call  ", with: "")
        busPhone = busPhone?.replacingOccurrences(of: "-", with: "")
        if let url = URL(string: "tel://\(busPhone ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            AlertUtils.shared.showMessage(viewController: self, title: "Announce", message: "Invalid Phone Number")
        }
    }
}

// MARK: - UITableViewDataSource
extension YellowDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(YellowDetailTableViewCell.self, indexPath: indexPath)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension YellowDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let yellowPageTableViewCell = cell as? YellowDetailTableViewCell {
            yellowPageTableViewCell.heightConstantTextView.constant = self.heightTextView + 10
            yellowPageTableViewCell.config(self.yellowPageObject, location: self.location)
            // call phone number
            yellowPageTableViewCell.callPhoneNumber = { [weak self] button in
                self?.callAction(button)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
}
