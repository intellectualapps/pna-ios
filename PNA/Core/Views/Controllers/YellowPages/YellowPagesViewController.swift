//
//  YellowPagesViewController.swift
//  PNA
//
//  Created by hien.bui on 3/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let pushToDetailYellow = "pushToDetailYellow"
        static let pushToFilter = "goToFilter"
    }
}

class YellowPagesViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var tableView: UITableView?
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet private weak var tableHeaderView: UIView?
    // MARK: - Local Variables
    let searchController = UISearchController(searchResultsController: nil)
    fileprivate var viewModel = YellowPagesViewModel()
    fileprivate var isSearching = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Yellow Pages"
        removeNavigationBarItem()
        setNavigationBarItem()
        setupObservable()
        viewModel.getYellowPages(isLoadMore: false, start: 0)
    }
    
    override func backAction() {
        self.menuAction()
    }

    override func setupUI() {
        self.tableView?.tableHeaderView = self.tableHeaderView
        self.tableView?.registerCell(YellowPageTableViewCell.self)
        self.tableView?.addPullToRefresh(actionHandler: { [weak self] in
            self?.viewModel.getYellowPages(isLoadMore: false, start: 0)
            self?.tableView?.pullToRefreshView.stopAnimating()
        })
        
        self.tableView?.addInfiniteScrolling(actionHandler: { [weak self] in
            guard let `self` = self else { return }
            let page = self.viewModel.startPage()
            self.viewModel.getYellowPages(isLoadMore: true, start: page)
        })
        
        tableView?.tableFooterView = UIView()
    }

    override func setupObservable() {
        viewModel
            .yellowPages
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] yellowPages in
                self?.reloadTableView()
            })
            .disposed(by: rx.disposeBag)
        
        viewModel
            .yellowPagesSearch
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] yellowPagesSearch in
                self?.reloadTableView()
            })
            .disposed(by: rx.disposeBag)
        searchTextField?
            .rx
            .text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: {[weak self] (text) in
                if text == "" {
                    self?.isSearching = false
                    self?.reloadTableView()
                    return
                }
                self?.isSearching = true
                self?.viewModel.getYellowPagesSearch(searchText: text, isLoadMore: false, start: 0, pageSize: 30)
            }).disposed(by: rx.disposeBag)
    }
    
    func reloadTableView() {
        let lastOffset = tableView?.contentOffset ?? .zero
        tableView?.reloadData()
        tableView?.contentOffset = lastOffset
    }
    
    //MARK: - Actions
    @IBAction func onSearchButton(_ sender: UIButton) {
        searchTextField.becomeFirstResponder()
    }
    
    // MARK: - Prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination
        switch segue.identifier ?? "" {
        case Constant.SegueIdentifier.pushToDetailYellow:
            guard let yellowDetailViewController = viewController as? YellowDetailViewController else { return }
            guard let indexPathSelected = tableView?.indexPathForSelectedRow else { return }
            yellowDetailViewController.yellowPageObject = viewModel.yellowPages.value[indexPathSelected.row]
        case Constant.SegueIdentifier.pushToFilter :
            guard let filterViewController = viewController as? YellowPagesFilterViewController else { return }
            filterViewController.viewModel = self.viewModel
        default:
            break
        }
    }
}

extension YellowPagesViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let searchText = textField.text, searchText != "" else {
            self.isSearching = false
            self.reloadTableView()
            return
        }
        self.isSearching = true
        viewModel.getYellowPagesSearch(searchText: searchText, isLoadMore: false, start: 0, pageSize: 30)
    }
}

// MARK: - UITableViewDataSource
extension YellowPagesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching {
            return viewModel.yellowPagesSearch.value.count
        }
        return viewModel.yellowPages.value.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.isSearching {
            let yellowPageObject = viewModel.yellowPagesSearch.value[indexPath.row]
            return yellowPageObject.height
        } else {
            let yellowPageObject = viewModel.yellowPages.value[indexPath.row]
            return yellowPageObject.height
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "YellowPageTableViewCell", for: indexPath)
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension YellowPagesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let yellowPageTableViewCell = cell as? YellowPageTableViewCell {
            if self.isSearching {
                let yellowPageObject = viewModel.yellowPagesSearch.value[indexPath.row]
                yellowPageTableViewCell.config(yellowPageObject)
            } else {
                let yellowPageObject = viewModel.yellowPages.value[indexPath.row]
                yellowPageTableViewCell.config(yellowPageObject)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constant.SegueIdentifier.pushToDetailYellow, sender: self)
    }
}
