//
//  YellowPagesFilterViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import DropDown

class YellowPagesFilterViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var stateButton: UIButton!
    @IBOutlet weak var industryButton: UIButton!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var industryLabel: UILabel!
    
    //MARK: - Variables
    let industryDropDown = DropDown()
    let stateDropDown = DropDown()
    var viewModel: YellowPagesViewModel?
    
    //MARK: - Constant
    private let STATE_SEGUE_IDENTIFIER    = "goToStateFilter"
    private let CATEGORY_SEGUE_IDENTIFIER = "goToCategoryFilter"
    private let DEFAULT_STATE_BUTTON_TITLE    = "Select state"
    private let DEFAULT_INDUSTRY_BUTTON_TITLE = "Select industry"
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Filter"
    }
    
    override func setupObservable() {
        viewModel?
            .selectedStates
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (states) in
                if states.isEmpty {
                    self?.stateLabel?.text = self?.DEFAULT_STATE_BUTTON_TITLE
                    return
                }
                var statesName: [String] = []
                for state in states {
                    statesName.append(state.name)
                }
                let title = statesName.joined(separator: ",")
                self?.stateLabel?.text = title
            })
            .disposed(by: rx.disposeBag)
        viewModel?
            .selectedCategories
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (categories) in
                if categories.isEmpty {
                    self?.industryLabel?.text = self?.DEFAULT_INDUSTRY_BUTTON_TITLE
                    return
                }
                var categoriesName: [String] = []
                for category in categories {
                    categoriesName.append(category.name)
                }
                let title = categoriesName.joined(separator: ",")
                self?.industryLabel?.text = title
            })
            .disposed(by: rx.disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailVC = segue.destination as? YellowPageFilterDetailViewController  else { return }
        detailVC.viewModel = self.viewModel
        switch segue.identifier {
        case self.STATE_SEGUE_IDENTIFIER:
            detailVC.filterType = .state
        case self.CATEGORY_SEGUE_IDENTIFIER:
            detailVC.filterType = .category
        default:
            break
        }
    }
    
    //MARK: - Actions
    @IBAction func onApplyButton(_ sender: UIButton) {
        viewModel?.filterYellowPages(isLoadMore: true)
        navigationController?.popViewController(animated: true)
    }
    
}
