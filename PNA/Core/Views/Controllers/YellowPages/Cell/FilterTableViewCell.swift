//
//  FilterTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/18/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func clear() {
        selectedView?.isHidden = true
        nameLabel?.text = nil
    }
}
