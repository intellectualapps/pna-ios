//
//  YellowDetailTableViewCell.swift
//  PNA
//
//  Created by hien.bui on 3/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage

class YellowDetailTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var businessNameLabel: UILabel!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var businessPhoneButton: UIButton!
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var heightConstantTextView: NSLayoutConstraint!
    
    var openWebsite: ((UIButton) -> ())?
    var callPhoneNumber: ((UIButton) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        businessPhoneButton.roundView()
        addressTextView.textContainerInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        googleMap.settings.compassButton = true
        googleMap.settings.zoomGestures = true
    }
    
    func config(_ yellowPageObject: YellowPageObject?, location: Location?) {
        guard let yellowPageObject = yellowPageObject else { return }
        if let location = location {
            displayLocation(latidue: location.lat, longtidue: location.long, markerTitle: yellowPageObject.businessName, markerSnippet: yellowPageObject.businessAddress)
        }
        businessNameLabel?.text = yellowPageObject.businessName
        addressTextView.text = ""
        if yellowPageObject.businessAddress != "" {
            addressTextView.text.append(yellowPageObject.businessAddress)
        }
        if yellowPageObject.businessCategory != "" {
            addressTextView.text.append("\n\n\(yellowPageObject.businessCategory)")
        }
        if yellowPageObject.businessEmail != "" {
            addressTextView.text.append("\n\n\(yellowPageObject.businessEmail)")
        }
        if yellowPageObject.businessWebsite != "" {
            addressTextView.text.append("\n\n\(yellowPageObject.businessWebsite)")
        }
        if let businessOffers = yellowPageObject.businessOffers {
            addressTextView.text.append("\n")
            for offer in businessOffers {
                addressTextView.text.append("\n\(offer)")
            }
        }
        self.businessPhoneButton?.setTitle("Call  \(yellowPageObject.businessPhone)", for: .normal)
        
        self.logoImageView.image = UIImage.defaultAvatar()
        if let url = URL(string: yellowPageObject.businessLogo) {
            logoImageView?.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar(), options: SDWebImageOptions.cacheMemoryOnly, completed: {(image, error, type, url) in
            })
        }
    }
    
    func displayLocation(latidue: Double, longtidue: Double, markerTitle: String, markerSnippet: String) {
        let coordinate = CLLocationCoordinate2D(latitude: latidue, longitude: longtidue)
        DispatchQueue.main.async {
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,
                                                  longitude: coordinate.longitude,
                                                  zoom: 18)
            self.googleMap?.camera = camera
            let marker = GMSMarker(position: coordinate)
            marker.title = markerTitle
            marker.snippet = markerSnippet
            marker.map = self.googleMap
        }
    }
    
    @IBAction func callPhoneNumber(_ sender: UIButton) {
        self.callPhoneNumber?(sender)
    }
}
