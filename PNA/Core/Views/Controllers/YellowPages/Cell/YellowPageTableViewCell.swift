//
//  YellowPageTableViewCell.swift
//  PNA
//
//  Created by MTQ on 3/15/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import SDWebImage

class YellowPageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet private weak var logoImageView: UIImageView?
    
    var yellowPageObject: YellowPageObject?

    override func awakeFromNib() {
        super.awakeFromNib()   
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func clear() {
        logoImageView?.sd_cancelCurrentImageLoad()
        logoImageView?.image = UIImage.defaultAvatar()
        descriptionLbl?.attributedText = nil
    }
    
    func config(_ yellowPageObject: YellowPageObject) {
        self.yellowPageObject = yellowPageObject
        clear()
        if let url = URL(string: yellowPageObject.businessLogo) {
            logoImageView?.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar(), options: SDWebImageOptions.cacheMemoryOnly, completed: {[weak self] (image, error, type, url) in
            })
        }
        descriptionLbl?.attributedText = yellowPageObject.attributeText()
    }
}
