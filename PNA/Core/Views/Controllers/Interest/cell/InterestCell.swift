//
//  InterestCell.swift
//  PNA
//
//  Created by Nguyen Van Dung on 3/15/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class InterestCell: UITableViewCell {

    @IBOutlet weak var imageBgView: UIImageView!

    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var titleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func clear() {
        imageBgView?.image = nil
        titleLbl?.text = nil
    }

    func config(info: InterestInfo) {
        titleLbl?.text = info.industry
        if info.image.count > 0 {
            imageBgView?.sd_setImage(with: URL.init(string: info.image), placeholderImage: nil, options: .continueInBackground, completed: {[weak self] (image, error, type, url) in
                if let img = image {
                    self?.imageBgView?.image = img
                }
            })
        }
        overlayView?.isHidden = !info.isSelected
    }
}
