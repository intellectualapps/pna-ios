//
//  InterestVC.swift
//  PNA
//
//  Created by Nguyen Van Dung on 3/15/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class InterestVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var continueBtn: UIButton!
    let viewModel = InterestViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select Interests"
        registerCell()
        addRightBarButtonWithTitle(title: "SKIP", action: #selector(rightAction))
    }

    override func addLeftButton() {
        self.navigationItem.backBarButtonItem = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if viewModel.interest.count == 0 {
            self.showProgress()
            viewModel.getInterest {[weak self] (response, error) in
                self?.hideProgress()
                if let err = error {
                    Alert.showAlertWithErrorMessage(err.message)
                } else {
                    DispatchQueue.main.async {
                        self?.reload()
                    }
                }
            }
        }
    }

    @objc func leftAction() {
        self.showHomeScreen()
        self.dismiss(animated: true, completion: nil)
    }

    @objc func rightAction() {
        leftAction()
    }

    @IBAction func continueAction(_ sender: Any) {
        continueBtn?.isEnabled = false
        let ids = viewModel.interest.filter({$0.isSelected}).map({$0.id.toString()})
        self.showProgress()
        SetInterestService.setInterest(interestids: ids) {[weak self] (response, error) in
            self?.hideProgress()
            if let err = error {
                self?.continueBtn?.isEnabled = true
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                DispatchQueue.main.async {
                    self?.leftAction()
                }
            }
        }
    }
    
    func reload() {
        self.tableView?.reloadData()
    }

    func registerCell() {
        let xib = UINib(nibName: "InterestCell", bundle: nil)
        tableView?.register(xib, forCellReuseIdentifier: "InterestCell")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.interest.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InterestCell", for: indexPath)
        if indexPath.row < viewModel.interest.count {
            if let interestcell = cell as? InterestCell {
                let info = viewModel.interest[indexPath.row]
                interestcell.config(info: info)
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let hd = view as? UITableViewHeaderFooterView {
            hd.backgroundView?.backgroundColor = .clear
            hd.contentView.backgroundColor = .clear
        }
    }

    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let hd = view as? UITableViewHeaderFooterView {
            hd.backgroundView?.backgroundColor = .clear
            hd.contentView.backgroundColor = .clear
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let info = viewModel.interest[indexPath.row]
        info.isSelected = !info.isSelected
        tableView.reloadRows(at: [indexPath], with: .none)
        checkContinueState()
    }

    func checkContinueState() {
        let count = viewModel.interest.filter({$0.isSelected}).count
        continueBtn?.isEnabled = count > 0
    }
}
