//
//  NetworkViewSuggestionController.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

protocol NetworkSuggestionCellDelegate: class {
    func wantToShowProfile(user: SuggestionUser?)
}

class NetworkSuggestionViewController: BaseViewController, NetworkSuggestionCellDelegate {
    let dispose = DisposeBag()
    //View Model
    var viewModel = NetworkSuggestionViewModel()
    //MARK: UI elements
    
    @IBOutlet weak var networkSuggestionCollectionView: UICollectionView!
    //MARK: Local variables

    //MARK: UI view
    override func viewDidLoad() {
        super.viewDidLoad()
        networkSuggestionCollectionView?.addPullToRefresh(actionHandler: {[weak self] in
            self?.viewModel.getSuggestionList()
        })
        networkSuggestionCollectionView?.pullToRefreshView?.activityIndicatorViewStyle = .gray
        networkSuggestionCollectionView?.pullToRefreshView?.arrowColor = .gray
        networkSuggestionCollectionView?.pullToRefreshView?.titleLabel?.text = nil
        networkSuggestionCollectionView?.pullToRefreshView?.subtitleLabel?.text = nil
        viewModel.selectedUsers.asObservable()
            .subscribe(onNext: {[weak self] (users) in
                self?.networkSuggestionCollectionView?.reloadData()
            }).disposed(by: dispose)

        viewModel.suggestionUsers.asObservable()
            .subscribe(onNext: {[weak self] (users) in
                self?.networkSuggestionCollectionView?.pullToRefreshView.stopAnimating()
                DispatchQueue.main.async {
                    self?.networkSuggestionCollectionView?.reloadData()
                }
            }).disposed(by: dispose)
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getSuggestionList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: action
    
    @IBAction func addNetworkAction(_ sender: Any) {
        if viewModel.selectedUsers.value.count == 0 {
            Alert.showAlertWithErrorMessage("Please select member")
        }else {
            viewModel.addUserSlectedToMyNetwrok()
        }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        showHomeScreen()
    }
    
    override func showHomeScreen(_ animated:Bool = true) {
        DispatchQueue.main.async {
            AppDelegate.shared().setupFeedMenu()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileDetail" {
            guard let detailVC = segue.destination as? SlimProfileViewController else { return }
            if let user = sender as? SuggestionUser {
                detailVC.suggestionUser = user
                detailVC.suggestViewModel = self.viewModel
            }
        }
    }
    
}


//MARK: extension collectionviewDatasource
extension NetworkSuggestionViewController: UICollectionViewDataSource {
    func showProfile(index: Int) {
//        performSegue(withIdentifier: "profileDetail", sender: viewModel.suggestionUsers.value[index])
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let value = viewModel.suggestionUsers.value[indexPath.row]
        let email = value.email 
        if self.viewModel.selectedEmails.contains(email) {
            self.viewModel.removeUser(email: email)
            networkSuggestionCollectionView.reloadItems(at: [indexPath])
        } else {
             self.viewModel.addUser(user: value)
            networkSuggestionCollectionView.reloadItems(at: [indexPath])
        }
    }

    func wantToShowProfile(user: SuggestionUser?) {
        if let user = user {
            performSegue(withIdentifier: "profileDetail", sender: user)
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.suggestionUsers.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NetworkSuggestionViewCell.identifier, for: indexPath)
        (cell as? NetworkSuggestionViewCell)?.delegate = self
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let networkCell = cell as? NetworkSuggestionViewCell {
            let user = viewModel.suggestionUsers.value[indexPath.row]
            user.isAdded = viewModel.selectedEmails.contains(user.email)
            networkCell.userSuggestion = user
            networkCell.choiced = viewModel.selectedEmails.contains(user.email)
        }
    }
}

//MARK: extesion collectionviewDelegate
extension NetworkSuggestionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
}

//MARK: extension collectionviewFlowLayout
extension NetworkSuggestionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       // let height = (collectionView.frame.height - 20) / 3
        let width = (collectionView.frame.width - 90) / 2
        return CGSize(width: width, height: width)
    }
}

