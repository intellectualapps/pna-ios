//
//  NetworkRequestViewController.swift
//  PNA
//
//  Created by center12 on 2/22/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import Material

protocol NetworkRequestViewDelegate {
    func onRefreshNetworkRequest(refreshControl: UIRefreshControl) -> ()
}

class NetworkRequestViewController: BaseViewController {
    //MARK: Data Models
    //-----------------------
    var viewModel: NetworkRequestViewModel?
    //MARK: Local Variables
    //-----------------------
    let disposed = DisposeBag()
    var delegate: NetworkRequestViewDelegate?
    var requestDone = false
    //MARK: UI Elements
    //-----------------------
    @IBOutlet weak var networkReuquestTableView: UITableView! {
        didSet {
            networkReuquestTableView.delegate = self
            networkReuquestTableView.dataSource = self
        }
    }
    //MARK: UIView
    //-----------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = NetworkRequestViewModel()
        networkReuquestTableView?.addPullToRefresh(actionHandler: {[weak self] in
            self?.networkReuquestTableView.reloadData()
            self?.viewModel?.getNetworkRequestList({ [weak self] _ in
                self?.requestDone = true
                self?.networkReuquestTableView.reloadData()
            })
        })
        networkReuquestTableView?.pullToRefreshView?.activityIndicatorViewStyle = .gray
        networkReuquestTableView?.pullToRefreshView?.arrowColor = .gray
        networkReuquestTableView?.pullToRefreshView?.titleLabel?.text = nil
        networkReuquestTableView?.pullToRefreshView?.subtitleLabel?.text = nil
        self.title = "Network Request"
        viewModel?.networkRequestUsers.asObservable()
            .subscribe(onNext: { [weak self] (users) in
                if users.count == 0 {
                    self?.networkReuquestTableView.separatorStyle = .none
                    self?.networkReuquestTableView?.allowsSelection = false
                }else {
                    self?.networkReuquestTableView?.allowsSelection = true
                    self?.networkReuquestTableView.separatorStyle = .singleLine
                }
                self?.networkReuquestTableView.reloadData()
                self?.networkReuquestTableView?.pullToRefreshView.stopAnimating()
            }).disposed(by: disposed)

        self.networkReuquestTableView.tableFooterView = UITableViewHeaderFooterView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.getNetworkRequestList({ [weak self] _ in
            self?.requestDone = true
            self?.networkReuquestTableView.reloadData()
        })
    }
    //MARK: View Setup
    //-----------------------
    
    //MARK: Events
    //-----------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "networkInvitation" {
            guard let networkVC = segue.destination as? NetworkInvitationViewController else {
                return
            }
            if let user = sender as? NetworkUser {
                networkVC.viewModel = viewModel
                networkVC.requestUser = user
                networkVC.delegate = viewModel
            }
        }
    }

}

// MARK: - extension uitableviewDelegate
extension NetworkRequestViewController: UITableViewDelegate {
    
}
// MARK: - extension uitableviewdatasource
extension NetworkRequestViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = viewModel?.networkRequestUsers.value.count {
            let rows = (count == 0 && self.requestDone) ? 1 : count
            return rows
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let count = viewModel?.networkRequestUsers.value.count , count == 0 , self.requestDone {
            self.requestDone = false
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NetworkRequestCell.identifier, for: indexPath)
        if let networkCell = cell as? NetworkRequestCell {
            networkCell.networkRequestUser = viewModel?.networkRequestUsers.value[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = viewModel?.networkRequestUsers.value[indexPath.row]
        performSegue(withIdentifier: "networkInvitation", sender: user)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
