//
//  NetworkRequestCell.swift
//  PNA
//
//  Created by center12 on 2/22/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import SDWebImage
import Material

class NetworkRequestCell: UITableViewCell {
    static let identifier = "NetworkRequestCell"
    
    // MARK: - UIElements
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    
    // MARK: - Local variables
    var networkRequestUser: NetworkUser? {
        didSet {
            guard let user = networkRequestUser else {
                return
            }
            nameLabel?.text = user.flname
            infoLabel?.text = user.jobTitle
            if let str = user.pic, !str.isEmpty,  let url = URL(string: str) {
                avatarImage?.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar(), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupView() {
        avatarImage?.layer.cornerRadius = (avatarImage?.frame.width ?? 0) / 2
        avatarImage?.layer.masksToBounds = true
        //avatarImage?.layer.borderWidth = 1
        //avatarImage?.layer.borderColor = UIColor.green.cgColor
        
    }

}
