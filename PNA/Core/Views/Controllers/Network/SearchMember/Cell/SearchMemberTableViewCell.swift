//
//  SearchMemberTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/15/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct EdgeLength {
        static let iconImageView: CGFloat = 60
    }
    struct State {
        static let none = "none"
        static let accepted = "accepted"
        static let pending = "pending"
    }
}

class SearchMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    
    static let identifier: String = "SearchMemberTableViewCell"
    var delegate: SearchMemberProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func clear() {
        nameLabel?.text = nil
        jobLabel?.text = nil
        addImage?.isHidden = false
    }
    
    func config(member: SearchProfile) {
        nameLabel?.text = (member.firstName ?? "") + "" + (member.lastName ?? "")
        if let url = URL(string: member.pic ?? "") {
            iconImageView?.sd_setImage(with: url, placeholderImage: UIImage(), options: .cacheMemoryOnly, completed: nil)
        } else {
            iconImageView?.image = #imageLiteral(resourceName: "avatar_default")
        }
        jobLabel?.text = member.jobTitle
        addImage?.isHidden = member.membershipType == Constant.State.none ? false : true
        addButton?.isEnabled = member.membershipType == Constant.State.none ? true : false
    }
    
    @IBAction func onAddButton(_ sender: UIButton) {
        delegate?.searchMemberTableViewCell(self, didTapOnAdd: sender)
    }
}

protocol SearchMemberProtocol {
    func searchMemberTableViewCell(_ cell: SearchMemberTableViewCell, didTapOnAdd sender: UIButton)
}
