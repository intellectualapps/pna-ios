//
//  SearchMemberViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/15/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import DropDown

private struct Constant {
    struct State {
        static let none = "none"
        static let accepted = "accepted"
        static let pending = "pending"
    }
    struct Identifier {
        static let cell = "SearchMemberTableViewCell"
        static let segue = "goToProfile"
    }
}

class SearchMemberViewController: BaseViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var industryButton: UIButton!
    @IBOutlet weak var industryView: UIView!
    @IBOutlet weak var buttonHeightConstrant: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    var viewModel = SearchMemberViewModel()
    var dropViewModel = DropDownViewModel()
    let industryDropDown = DropDown()
    var selectedMembersId: [Int] = [] {
        didSet {
            if selectedMembersId.isEmpty {
                dismissAddButton()
            } else {
                presentAddButton()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Member Search"
        self.tableView?.register(UINib(nibName: Constant.Identifier.cell, bundle: nil),
                                 forCellReuseIdentifier: SearchMemberTableViewCell.identifier)
        tableView?.rowHeight = 80
        viewModel.members.asObservable()
            .subscribe(onNext: { [weak self](_) in
                self?.selectedMembersId = []
                self?.tableView?.reloadData()
            }).disposed(by: rx.disposeBag)
        searchBar?.rx.text.orEmpty
        .debounce(0.5, scheduler: MainScheduler.instance)
        .distinctUntilChanged()
            .subscribe(onNext: {[weak self] (text) in
                self?.viewModel.selectedIndustry = nil
                self?.viewModel.getMembers(text: text)
            }).disposed(by: rx.disposeBag)
        dropViewModel.industrySetup { [weak self] (industriesInfo) in
            guard let industriesInfo = industriesInfo else { return }
            self?.viewModel.industies.value = industriesInfo
            self?.didReceiveIndustries()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getMembers(text: searchBar.text ?? "")
    }
    
    override func setupUI() {
        addButton.roundCorner(radius: 5)
        tableView?.tableHeaderView = industryView
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    private func presentAddButton() {
        UIView.animate(withDuration: 0.33, animations: {
            self.addButton?.isHidden = false
            self.buttonHeightConstrant.constant = 50
            self.bottomViewHeightConstraint.constant = 100
            self.view.layoutIfNeeded()
        })
    }
    
    private func dismissAddButton() {
        UIView.animate(withDuration: 0.33, animations: {
            self.addButton?.isHidden = true
            self.buttonHeightConstrant.constant = 0
            self.bottomViewHeightConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func didReceiveIndustries() {
        self.makeIndustrySelection()
    }
    
    func makeIndustrySelection() {
        self.industryDropDown.anchorView = self.industryButton
        self.industryDropDown.dataSource = viewModel.indestriesNames
        self.industryDropDown.bottomOffset = CGPoint(x: 0, y: self.industryButton?.bounds.height ?? 0)
        
        if self.viewModel.currentIndustryId > 0 {
            let selectedIndustryObj: [IndustryInfo] = viewModel.industies.value.filter {$0.id == self.viewModel.currentIndustryId }
            let currentIdx = viewModel.industies.value.index(of: selectedIndustryObj[0]) ?? -1
            if (currentIdx >= 0) {
                let title  = viewModel.indestriesNames[currentIdx]
                self.industryButton?.setTitle(title, for: .normal)
                self.industryButton?.setTitleColor(.black, for: .normal)
                self.viewModel.industry = currentIdx
            }
        }
        
        self.industryDropDown.selectionAction = {[weak self] (index, item) in
            self?.industryButton?.setTitle(item, for: .normal)
            self?.industryButton?.setTitleColor(.black, for: .normal)
            self?.viewModel.industry = index
            self?.viewModel.getMembers(text: self?.searchBar.text ?? "")
        }
    }
    
    @IBAction func onAddButton(_ sender: UIButton) {
        viewModel.addUserSlectedToMyNetwrok(membersId: selectedMembersId) {
            self.viewModel.getMembers(text: self.searchBar?.text ?? "")
        }
    }
    @IBAction func onSelectIndustry(_ sender: UIButton) {
        industryDropDown.show()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let profileVC = segue.destination as? SearchAddMemberViewController {
            if let indexPath = tableView?.indexPathForSelectedRow {
                profileVC.searchViewModel = self.viewModel
                profileVC.user = viewModel.members.value[indexPath.row]
            }
        }
    }
    
}

extension SearchMemberViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.members.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constant.Identifier.cell, for: indexPath) as? SearchMemberTableViewCell {
            cell.config(member: viewModel.members.value[indexPath.row])
            cell.delegate = self
            if let id = viewModel.members.value[indexPath.row].id {
                cell.addImage.image = selectedMembersId.contains(id) ? #imageLiteral(resourceName: "slected") : #imageLiteral(resourceName: "add_icon")
            }
            return cell
        } else {
            return UITableViewCell()
        }
    }
}

extension SearchMemberViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constant.Identifier.segue, sender: nil)
    }
}

extension SearchMemberViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

extension SearchMemberViewController: SearchMemberProtocol {
    func searchMemberTableViewCell(_ cell: SearchMemberTableViewCell, didTapOnAdd sender: UIButton) {
        if let indexPath = tableView.indexPath(for: cell) {
            guard let id = viewModel.members.value[indexPath.row].id else { return }
            if selectedMembersId.contains(id) {
                guard let index = selectedMembersId.index(of: id) else { return }
                selectedMembersId.remove(at: index)
            } else {
                selectedMembersId.append(id)
            }
            tableView.reloadData()
        }
    }
    
    
}
