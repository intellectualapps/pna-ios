//
//  SearchAddMemberViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/18/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct State {
        static let none = "none"
        static let accepted = "accepted"
        static let pending = "pending"
    }
}

class SearchAddMemberViewController: BaseViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    var searchViewModel: SearchMemberViewModel?
    var user: SearchProfile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        loadUserToView()
    }
    
    @IBAction func onAddButton(_ sender: UIButton) {
        searchViewModel?.addUserSlectedToMyNetwrok(membersId: [user?.id ?? 0]) {[weak self] in
            self?.addButton.isEnabled = false
        }
    }
    
    func loadUserToView() {
        guard let user = user else {
            return
        }
        if let url = URL(string: user.pic ?? "") {
            avatarImageView?.sd_setImage(with: url, completed: nil)
        }
        usernameLabel?.text = (user.firstName ?? "") + "" + (user.lastName ?? "")
        jobTitleLabel?.text = user.jobTitle
        companyLabel?.text = user.company
        guard let networkState = user.membershipType else { return }
        switch networkState {
        case Constant.State.accepted:
            addButton.isHidden = true
        case Constant.State.pending:
            addButton.isEnabled = false
        default:
            break
        }
    }
    
}

// MARK: - extension tableviewDatasource
extension SearchAddMemberViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

// MARK: - extension tableviewDelegate
extension SearchAddMemberViewController: UITableViewDelegate {
    
}
