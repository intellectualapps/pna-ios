//
//  NetworkSuggestionViewCell.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import Material
import SDWebImage

class NetworkSuggestionViewCell: UICollectionViewCell {
    static let identifier = "NetworkSuggestionCellID"
    //MARK: UI elements
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var nameLbl: FlatButton!

    @IBOutlet weak var showProfileBtn: UIButton!
    //MARK: Local variables
    var delegate: NetworkSuggestionCellDelegate?
    var userSuggestion: SuggestionUser? {
        didSet {
            guard let user = userSuggestion else {
                return
            }
            nameLbl?.setTitle(user.fullname, for: .normal)
            infoLabel?.text = user.jobTitle
            if let url = URL(string: user.avatarPath) {

                avatarView?.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar(), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
            }
        }
    }
    var choiced: Bool = false {
        didSet {
            selectedImageView?.isHidden = !choiced
        }
    }
    
    //MARK: ui view
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.setupView()
    }
    
    @IBAction func showProfileAction(_ sender: Any) {
        delegate?.wantToShowProfile(user: self.userSuggestion)
    }
    
    //MARK: Setup view
    func setupView() {
        self.choiced = false
    }
}
