//
//  NetworkViewController.swift
//  PNA
//
//  Created by Admin on 1/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import SlideMenuControllerSwift

protocol NetworkCellDelegate: class {
    func network(didSelectedUsername sender: NetworkViewCell)
    
    func network(didSelectedAvatar sender: NetworkViewCell)
}

protocol NetworkViewDelegate {
    func onDidSelectedMember(index: Int) -> (identifier: String, user: NetworkUser)
}

class NetworkViewController: BaseViewController {
    
    // MARK: - data model
    var viewModel: NetworkViewModel?
    //MARK: UI elements
    
    @IBOutlet weak var networkCollectionView: UICollectionView!
    //MARK: Local variables
    let dispose = DisposeBag()
    var delegate: NetworkViewDelegate?
    
    //MARK: UI view
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.viewModel = NetworkViewModel()
        self.delegate = viewModel
        networkCollectionView?.addPullToRefresh(actionHandler: {[weak self] in
            self?.viewModel?.getNetworkList(excution: {})
        })
        networkCollectionView?.pullToRefreshView?.activityIndicatorViewStyle = .gray
        networkCollectionView?.pullToRefreshView?.arrowColor = .gray
        networkCollectionView?.pullToRefreshView?.titleLabel?.text = nil
        networkCollectionView?.pullToRefreshView?.subtitleLabel?.text = nil
        
        viewModel?.selectedUsers.asObservable()
            .subscribe(onNext: {[weak self] (users) in
                self?.networkCollectionView?.reloadData()
            }).disposed(by: dispose)
        
        viewModel?.networkUsers.asObservable()
            .subscribe(onNext: {[weak self] (users) in
                self?.networkCollectionView?.pullToRefreshView.stopAnimating()
                DispatchQueue.main.async {
                    self?.networkCollectionView?.reloadData()
                }
            }).disposed(by: dispose)
        
        
        self.title = "My Network"
        
        self.removeNavigationBarItem()
        self.setNavigationBarItem()
        let requestBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "bell_icon"), style: .plain, target: self, action: #selector(showNetworkRequestScreen))
        let addBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "addMemberIcon"), style: .plain, target: self, action: #selector(showSearchMemberScreen))
        navigationItem.rightBarButtonItems = [requestBtn, addBtn]
        navigationController?.navigationBar.tintColor = .white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.getNetworkList(excution: {})
    }

    //MARK: setup view

    
    //MARK: action
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "slimProfile" {
            guard let detailVC = segue.destination as? SlimProfileViewController else {
                return
            }
            if let user = sender as? NetworkUser {
                detailVC.suggestionUser = viewModel?.convertToSuggestionUser(user: user)
            }
        }else if segue.identifier == "profileDetail" {
            guard let detailUserVC = segue.destination as? StickyHeaderViewController else {
                return
            }
            if let user = sender as? NetworkUser {
                detailUserVC.user = user
            }
        }
    }
    
    override func backAction() {
        self.menuAction()
    }
    
    @objc func showNetworkRequestScreen() {
        let networkRequestVC = UIStoryboard.instantiate(NetworkRequestViewController.self, storyboardType: .networkRequest)
        self.navigationController?.pushViewController(networkRequestVC, animated: true)
    }
    
    @objc func showSearchMemberScreen() {
        performSegue(withIdentifier: "goToSearch", sender: nil)
    }
}

//MARK: extension NetworkCellDelegate
extension NetworkViewController: NetworkCellDelegate {
    
    func network(didSelectedUsername sender: NetworkViewCell) {
        guard let indexPath = networkCollectionView.indexPath(for: sender) else { return }
        if let (identifier, user) = delegate?.onDidSelectedMember(index: indexPath.row), !identifier.isEmpty {
            performSegue(withIdentifier: identifier, sender: user)
        }
    }
    
    func network(didSelectedAvatar sender: NetworkViewCell) {
        guard let indexPath = networkCollectionView.indexPath(for: sender) else { return }
        if let (identifier, user) = delegate?.onDidSelectedMember(index: indexPath.row), !identifier.isEmpty {
            let avatarImageView = AvatarImageView()
            avatarImageView.imageUrl = user.pic ?? ""
            present(avatarImageView, animated: true, completion: nil)
        }
    }
}

//MARK: extension collectionviewDatasource
extension NetworkViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.networkUsers.value.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NetworkViewCell.identifier, for: indexPath)
        
        if let networkCell = cell as? NetworkViewCell {
            let user = viewModel?.networkUsers.value[indexPath.row]
            networkCell.networkUser = user
            networkCell.delegate = self
            
            return networkCell
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let (identifier, user) = delegate?.onDidSelectedMember(index: indexPath.row), !identifier.isEmpty {
            performSegue(withIdentifier: identifier, sender: user)
        }
    }
}

//MARK: extesion collectionviewDelegate
extension NetworkViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
}

//MARK: extension collectionviewFlowLayout
extension NetworkViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = ((collectionView.frame.height - 20) / 4) - 18
        let width = (collectionView.frame.width - 70) / 2
        return CGSize(width: width, height: height)
    }
}
