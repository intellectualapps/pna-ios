//
//  NetworkViewCell.swift
//  PNA
//
//  Created by Admin on 1/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit

import SDWebImage

class NetworkViewCell: UICollectionViewCell {
    static let identifier = "NetworkViewCellID"
    //MARK: UI elements
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var avatarButton: FABButton!
    @IBOutlet weak var usernameButton: FlatButton!
    @IBOutlet weak var infoLabel: UILabel!
    //MARK: Local variables
    var delegate: NetworkCellDelegate?
    var networkUser: NetworkUser? {
        didSet {
            guard let user = networkUser else {
                return
            }
            usernameButton?.setTitle(user.flname , for: .normal)
            infoLabel?.text = user.jobTitle
            if let str = user.pic, !str.isEmpty , let url = URL(string: str) {
                avatarView?.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar(), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
            }
        }
    }
    
    //MARK: ui view
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.setupView()
    }
    
    //MARK: Setup view
    func setupView() {
        avatarView?.layer.masksToBounds = true
        //avatarView?.layer.borderWidth = 1
        //avatarView?.layer.borderColor = UIColor.red.cgColor
    }
    
    //MARK: action
    @IBAction func usernameAction(_ sender: Any) {
        delegate?.network(didSelectedUsername: self)
    }
    
    @IBAction func avatarAction(_ sender: Any) {
        delegate?.network(didSelectedAvatar: self)
    }
    
}
