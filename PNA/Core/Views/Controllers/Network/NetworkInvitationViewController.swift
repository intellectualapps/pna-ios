//
//  NetworkInvitationViewController.swift
//  PNA
//
//  Created by center12 on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

protocol NetworkInvitationViewDelegate: class {
    func onAcceptedTapped(user: NetworkUser?, _ excution: @escaping UpdateNetworkInvitationCompleted) -> ()
    func onDeclineTapped(user: NetworkUser?, _ excution: @escaping UpdateNetworkInvitationCompleted) -> ()
}

class NetworkInvitationViewController: BaseViewController {

    //MARK: Local Variables
    //-----------------------
    var delegate: NetworkInvitationViewDelegate?
    var requestUser: NetworkUser?
    var viewModel: NetworkRequestViewModel?
    //MARK: UI Elements
    //-----------------------
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var jobtitleLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    //MARK: UIView
    //-----------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    //MARK: View Setup
    //-----------------------
    
    //MARK: Events
    //-----------------------
    @IBAction func acceptedBtnAction(_ sender: Any) {
        delegate?.onAcceptedTapped(user: requestUser) { _ in
            self.backAction()
        }
    }
    
    @IBAction func declineBtnAction(_ sender: Any) {
        delegate?.onDeclineTapped(user: requestUser) { _ in
            self.backAction()
        }
    }
    
    func configure() {
        guard let user =  requestUser else {
            return
        }
        if let str = user.pic, !str.isEmpty ,let url = URL(string: str) {
            avatarImageView?.sd_setImage(with: url, completed: nil)
        }
        jobtitleLabel?.text = user.jobTitle
        companyLabel?.text = user.company
        namelabel?.text = "From: \(user.fullName ?? "")"
    }
}

// MARK: - extension tableviewDelegate
extension NetworkInvitationViewController: UITableViewDelegate {
    
}
// MARK: - extension tableviewDatasource
extension NetworkInvitationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    
}

