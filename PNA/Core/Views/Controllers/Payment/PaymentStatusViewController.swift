//
//  PaymentStatusViewController.swift
//  PNA
//
//  Created by bui.duy.hien on 3/12/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let unwindHomeVC = "unwindToHomeVC"
        static let unwindEventVC = "unwindToEventVCFromPaymentVC"
    }
}

class PaymentStatusViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var mailLabel: UILabel!

    var paymentType: PaymentType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupUI() {
        if let userModel = AppDelegate.shared().loggedUser {
            usernameLabel.text = userModel.fullName
            mailLabel.text = userModel.email
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        guard let paymentType = paymentType else { return }
        switch paymentType {
        case .discount:
            performSegue(withIdentifier: Constant.SegueIdentifier.unwindHomeVC, sender: self)
        case .event:
            performSegue(withIdentifier: Constant.SegueIdentifier.unwindEventVC, sender: self)
        default:
            break
        }
    }
}
