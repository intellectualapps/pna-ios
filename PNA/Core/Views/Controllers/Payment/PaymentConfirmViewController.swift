//
//  PaymentConfirmViewController.swift
//  PNA
//
//  Created by MTQ on 3/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import Paystack

private struct Constant {
    struct SegueIdentifier {
        static let showPaymentStatusVC = "showPaymentStatusVC"
    }
}

class PaymentConfirmViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet private weak var usernameTextField: UILabel!
    @IBOutlet private weak var emailTextField: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!
    @IBOutlet private weak var paymentView: UIView!
    @IBOutlet private weak var coverButton: UIButton!
    @IBOutlet private weak var cardNumberTextField: WTReTextField!
    @IBOutlet weak var expiryTextField: WTReTextField!
    @IBOutlet weak var cvvTextField: WTReTextField!
    // MARK: - View model
    private var viewModel = PaymentConfirmViewModel()
    // MARK: - Variables
    var amount = ""
    var transaction: Transaction?
    var paymentType: PaymentType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !amount.isEmpty {
            amountLabel.text = "₦\(self.amount)"
        }
        self.expiryTextField?.pattern = "^(1[0-2]|(?:0)[1-9])(?:/)\\d{2}$"
        self.cvvTextField?.pattern = "^\\d{3}$"
        self.cardNumberTextField?.pattern = "^(\\d{4}(?: )){3}\\d{4}$"
    }
    
    override func setupUI() {
        paymentView.alpha = 0
        coverButton.alpha = 0
        coverButton.backgroundColor = UIColor(white: 0, alpha: 0.5)
        if let userModel = AppDelegate.shared().loggedUser {
            usernameTextField.text = userModel.fullName
            emailTextField.text = userModel.email
        }
    }
    
    // MARK: - Actions
    @IBAction func showPaymentView(_ sender: UIButton) {
        presentPaymentView()
    }
    
    @IBAction func cancelPayment(_ sender: UIButton) {
        dismissPaymentView()
    }
    
    @IBAction func payAction(_ sender: UIButton) {
        viewModel.requestAPIGetPublicKey { publicKey in
            if (publicKey == "" || !publicKey.hasPrefix("pk_")) {
                print("You need to set your Paystack public key. You can find your public key at https://dashboard.paystack.co/#/settings/developer .")
                // You need to set your Paystack public key.
                return
            }
            Paystack.setDefaultPublicKey(publicKey)
            
            self.fetchAccessCodeAndChargeCard()
        }
    }
    
    private func presentPaymentView() {
        UIView.animate(withDuration: 0.33, animations: {
            self.paymentView.alpha = 1
            self.coverButton.alpha = 1
            self.view.layoutIfNeeded()
        })
    }
    
    private func dismissPaymentView() {
        UIView.animate(withDuration: 0.33, animations: {
            self.paymentView.alpha = 0
            self.coverButton.alpha = 0
            self.view.layoutIfNeeded()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.SegueIdentifier.showPaymentStatusVC {
            if let paymentStatusVC = segue.destination as? PaymentStatusViewController {
                paymentStatusVC.paymentType = self.paymentType
            }
        }
    }
    
    private func fetchAccessCodeAndChargeCard() {
        guard let accessCode = transaction?.accessCode else { return }
        self.chargeWithSDK(newCode: accessCode)
    }
    
    private func chargeWithSDK(newCode: String) {
        let transactionParams = PSTCKTransactionParams.init()
        transactionParams.access_code = newCode as String;
        
        // use library to create charge and get its reference
        let cardParams = PSTCKCardParams()
        cardParams.number = cardNumberTextField.text ?? ""
        let result = expiryTextField.text?.components(separatedBy: "/")
        cardParams.expMonth = UInt(result?.first ?? "0") ?? 0
        cardParams.expYear = UInt(result?.last ?? "0") ?? 0
        cardParams.cvc = cvvTextField.text ?? ""
        
        PSTCKAPIClient.shared().chargeCard(cardParams, forTransaction: transactionParams, on: self, didEndWithError: { (error, reference) in
            print("Charge errored")
            // what should I do if an error occured?
            print(error)
            if error._code == PSTCKErrorCode.PSTCKExpiredAccessCodeError.rawValue{
                // access code could not be used
                // we may as well try afresh
            }
            if error._code == PSTCKErrorCode.PSTCKConflictError.rawValue{
                // another transaction is currently being
                // processed by the SDK... please wait
            }
            if let errorDict = (error._userInfo as! NSDictionary?){
                if let errorString = errorDict.value(forKeyPath: "com.paystack.lib:ErrorMessageKey") as! String? {
                    if let reference=reference {
                        print("\(reference) : \(errorString)")
                        AlertUtils.shared.showMessage(viewController: self, title: nil, message: errorString)
                    } else {
                        print(errorString)
                        AlertUtils.shared.showMessage(viewController: self, title: nil, message: errorString)
                    }
                }
            }
//            self.chargeCardButton.isEnabled = true;
        }, didRequestValidation: { (reference) in
            print("requested validation: \(reference)")
        }, willPresentDialog: {
            // make sure dialog can show
            // if using a "processing" dialog, please hide it
            print("will show a dialog")
        }, dismissedDialog: {
            // if using a processing dialog, please make it visible again
            print("dismissed dialog")
        }) { (reference) in
            print("succeeded: \(reference)")
//            self.chargeCardButton.isEnabled = true;
            self.verifyTransaction(referenceCode: reference)
        }
        return
    }
    
    private func verifyTransaction(referenceCode: String) {
        guard let mail = AppDelegate.shared().loggedUser?.email else { return }
        viewModel.requestAPIVerifyTransaction(referenceCode: referenceCode, email: mail) { [weak self] in
                let user = AppDelegate.shared().loggedUser
                user?.membershipType = "premium"
                AppDelegate.shared().loggedUser = user
                self?.dismissPaymentView()
                self?.performSegue(withIdentifier: Constant.SegueIdentifier.showPaymentStatusVC, sender: self)
        }
    }
}
