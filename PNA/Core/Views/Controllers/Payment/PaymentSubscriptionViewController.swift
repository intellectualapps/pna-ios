//
//  PaymentSubscriptionViewController.swift
//  PNA
//
//  Created by MTQ on 2/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let paymentConfirm = "showPaymentConfirm"
    }
}

class PaymentSubscriptionViewController: BaseViewController {
    // MARK: - Outlets
    @IBOutlet private weak var usernameTextField: UILabel!
    @IBOutlet private weak var emailTextField: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!
    // MARK: - View model
    private var viewModel = PaymentSubscriptionViewModel()
    // MARK: - Variables
    private var amount = ""
    private var transaction: Transaction?
    var paymentType: PaymentType!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.requestAPIGetAmount { [weak self] (amount) in
            self?.amount = amount
            self?.amountLabel.text = "₦\(amount)"
        }
    }
    
    override func setupUI() {
        if let userModel = AppDelegate.shared().loggedUser {
            usernameTextField.text = userModel.fullName
            emailTextField.text = userModel.email
        }
    }
    
    @IBAction func proceedPaymendConfirm(_ sender: UIButton) {
        if let amountToInt = Int(amount.replacingOccurrences(of: ",", with: "")) {
            viewModel.requestAPIInitialiseTransaction(email: emailTextField.text ?? "", amount: amountToInt, completion: { [weak self] transaction in
                self?.transaction = transaction
                self?.performSegue(withIdentifier: Constant.SegueIdentifier.paymentConfirm, sender: self)
            })
        }
    }
    
    // MARK: - Prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let paymentConfirmViewController = segue.destination as? PaymentConfirmViewController else { return }
        paymentConfirmViewController.amount = self.amount
        paymentConfirmViewController.transaction = self.transaction
        paymentConfirmViewController.paymentType = self.paymentType
    }
}
