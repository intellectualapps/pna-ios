//
//  BannerSupportViewController.swift
//  PNA
//
//  Created by nguyen.van.dungb on 2/5/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
//import NotificationBannerSwift
//import TextFieldEffects

class BannerSupportViewController: UIViewController {
    let dispose = DisposeBag()
    
    private var minbarMessage: MinBarMessageView!
    var banner: NotificationBanner?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        minbarMessage = MinBarMessageView.fromNib()
        banner = NotificationBanner(customView: self.minbarMessage)
    }
    
    func showMessage(message: String){
        self.minbarMessage.messageLabel?.text = message
        if banner?.isDisplaying == false {
            banner?.show()
        }
    }
    
    func showApiErrorIfNeed(error: ErrorInfo?, response: ApiResponseBasicModel?) -> Bool {
        if let err = error {
            self.hideProgress()
            self.showMessage(message: err.message)
            return true
        } else if let result = response {
            self.hideProgress()
            if result.isError {
                self.showMessage(message: result.message)
                return true
            }
        }
        return false
    }
    
    func validateError(textField: HoshiTextField , message: String, color: UIColor) {
        self.showMessage(message: message)
        textField.borderActiveColor = color
        textField.borderInactiveColor = color
        textField.tintColor = color
        textField.becomeFirstResponder()
    }
}
