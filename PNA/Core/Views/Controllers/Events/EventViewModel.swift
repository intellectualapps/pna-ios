//
//  EventViewModel.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Alamofire

class EventViewModel: BaseViewModel {
    var events = Variable([Event]())
    
    var pageSize = 3
    var start = 0
    
    func getEvents() {
        let params = RequestParams()
        params.setValue(start, forKey: "start")
        params.setValue(pageSize, forKey: "page-size")
        let path = Path.event.path
        let service = EventService(apiPath: path,
                                   method: .get,
                                   requestParam: params,
                                   paramEncoding: Encoding.forMethod(method: .get),
                                   retryCount: 1)
        UIViewController.topMostViewController()?.showProgress()
        service.doExecute { [weak self](response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            guard let `self` = self else {return}
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
            } else {
                let events = response as? [Event] ?? []
                self.events.value = events
            }
        }
    }
}
