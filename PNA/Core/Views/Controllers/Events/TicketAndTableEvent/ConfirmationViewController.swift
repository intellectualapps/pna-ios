//
//  ConfirmationViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/12/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let goToStatus = "goToStatus"
    }
}

class ConfirmationViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    //MARK: - Local Variables
    var purchaseType: EventPuchaseType!
    var priceType: EventPriceType!
    var totalPrice: String!
    var event: Event?
    var transaction: Transaction?
    var viewModel: TicketAndTableViewModel?
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Events"
    }
    
    override func setupUI() {
        guard let event = event else { return }
        infoLabel?.text = "\(viewModel?.quantity ?? 0) \(priceType.rawValue) \(purchaseType.rawValue) for \(event.title ?? "")"
        totalPriceLabel?.text = totalPrice
        fullnameLabel?.text = AppDelegate.shared().loggedUser?.fullName
        emailLabel?.text = AppDelegate.shared().loggedUser?.email
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let statusVC = segue.destination as? EventStatusPaymentViewController {
            statusVC.event = self.event
        }
    }
    
    //MARK: - Actions
    @IBAction func onPay(_ sender: UIButton) {
        let cardPaymentView = CardPaymentView()
        cardPaymentView.transaction = transaction
        cardPaymentView.purchaseType = purchaseType
        cardPaymentView.successClousure = {[weak self] in
            self?.performSegue(withIdentifier: Constant.SegueIdentifier.goToStatus, sender: nil)
        }
        present(cardPaymentView, animated: true, completion: nil)
    }
    
}
