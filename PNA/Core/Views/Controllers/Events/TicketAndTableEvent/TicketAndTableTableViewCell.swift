//
//  TicketAndTableTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/19/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class TicketAndTableTableViewCell: UITableViewCell {

    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var imageHeightConstrant: NSLayoutConstraint!
    @IBOutlet weak var ticketView: EventOptionView!
    @IBOutlet weak var tableView: EventOptionView!
    @IBOutlet weak var ticketViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var continueButton: UIButton!
    
    var viewModel: TicketAndTableViewModel?
    var delegate: TicketAndTableTableViewCellDelegate?
    var disposeBag = DisposeBag()

    func configUI(event: Event) {
        eventTitleLabel?.text = event.title
        imageHeightConstrant.constant = event.imageHeight
        if let url = URL(string: event.picURL ?? "") {
            let img = UIImage.init(named: "pna_africa_map")
            eventImageView.sd_setImage(with: url, placeholderImage: img, options: [], completed: nil)
        }
        ticketView.viewModel = viewModel
        tableView.viewModel = viewModel
        ticketView?.purchaseTypeLabel?.text = "Purchase Ticket"
        tableView?.purchaseTypeLabel?.text = "Reserve a Table"
        ticketView?.type = .ticket
        tableView?.type = .table
        if let tables = event.tables {
            tableView?.regularPriceLabel?.text = (Int(tables["regular"] ?? "") ?? 0).commasNumber()
            tableView?.vipPriceLabel?.text = (Int(tables["vip"] ?? "") ?? 0).commasNumber()
            if event.tickets == nil {
                self.viewModel?.purchaseType.value = .table
            }
        } else {
            tableView?.isHidden = true
            tableViewHeightConstraint.constant = 0
        }
        if let tickets = event.tickets {
            ticketView?.regularPriceLabel?.text = (Int(tickets["regular"] ?? "") ?? 0).commasNumber()
            ticketView?.vipPriceLabel?.text = (Int(tickets["vip"] ?? "") ?? 0).commasNumber()
        } else {
            ticketView?.isHidden = true
            ticketViewHeightConstraint.constant = 0
        }
        continueButton?.isHidden = true
        viewModel?
            .price
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (price) in
                if price.isEmpty {
                    self?.continueButton?.isHidden = true
                } else {
                    self?.continueButton?.isHidden = false
                }
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func onContinue(_ sender: UIButton) {
        delegate?.onTapOnContinue()
    }
}

protocol TicketAndTableTableViewCellDelegate {
    func onTapOnContinue()
}
