//
//  TicketAndTableViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/12/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let goToQuantity = "goToQuantity"
    }
}

class TicketAndTableViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Local variables
    var viewModel = TicketAndTableViewModel()
    var event: Event?
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Events"
    }
    
    override func setupUI() {
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 200
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let quantityVC = segue.destination as? QuantityViewController {
            quantityVC.event = event
            quantityVC.purchaseType = viewModel.purchaseType.value
            quantityVC.priceType = viewModel.priceType.value
            quantityVC.price = viewModel.price.value.intoNumberWithoutCommas()
        }
    }
}

extension TicketAndTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(TicketAndTableTableViewCell.self, indexPath: indexPath)
        guard let event = event else {
            return UITableViewCell()
        }
        cell.viewModel = viewModel
        cell.delegate = self
        cell.configUI(event: event)
        return cell
    }
}

extension TicketAndTableViewController: TicketAndTableTableViewCellDelegate {
    func onTapOnContinue() {
        performSegue(withIdentifier: Constant.SegueIdentifier.goToQuantity, sender: nil)
    }
}
