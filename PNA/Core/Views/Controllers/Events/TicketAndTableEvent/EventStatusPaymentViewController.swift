//
//  EventStatusPaymentViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/12/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let unwindFromEventPaymentStatusVC = "unwindFromEventPaymentStatusVC"
        static let unwindFromEventPaymentStatusVCToWelcome = "unwindFromEventPaymentStatusVCToWelcome"
    }
}

class EventStatusPaymentViewController: BaseViewController {

    @IBOutlet weak var fullnameLabel: PaddingLabel!
    @IBOutlet weak var emailLabel: PaddingLabel!
    var event: Event?
    var viewModel = EventViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Event Payment Status"
        addEventToCalendar()
    }
    
    func addEventToCalendar() {
        guard let event = event else { return }
        let title = event.title ?? ""
        let notes = "\(event.organizer ?? "")"
        let address = "\(event.address ?? "")"
        let dateString = "\(event.date ?? "") - \(event.time ?? "")"
        guard let date = dateString.getDateFromString(format: "yyyy-MM-dd - hh:mm a") else { return }
        Ultilites.addEventToCalendar(title: title, notes: notes, address: address, startDate: date, endDate: date) {
            Ultilites.addReminderToReminders(title: title, date: date, completion: {})
        }
    }
    
    override func setupUI() {
        fullnameLabel?.text = AppDelegate.shared().loggedUser?.fullName
        emailLabel?.text = AppDelegate.shared().loggedUser?.email
    }

    @IBAction func onClose(_ sender: UIButton) {
        performSegue(withIdentifier: Constant.SegueIdentifier.unwindFromEventPaymentStatusVC, sender: nil)
        performSegue(withIdentifier: Constant.SegueIdentifier.unwindFromEventPaymentStatusVCToWelcome,
                     sender: nil)
    }
}
