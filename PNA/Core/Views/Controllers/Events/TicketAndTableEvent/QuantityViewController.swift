//
//  QuantityViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/12/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let goToConfirm = "goToConfirm"
    }
}

class QuantityViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var purchaseOrReserveLabel: UILabel!
    @IBOutlet weak var eventPurchaseType: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var upButton: LargeExpandButton!
    @IBOutlet weak var downButton: LargeExpandButton!
    @IBOutlet weak var quantityLabel: UILabel!
    
    //MARK: - Local variables
    var event: Event?
    var purchaseType: EventPuchaseType!
    var priceType: EventPriceType!
    var price: Int!
    private var transaction: Transaction?
    var viewModel = TicketAndTableViewModel()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Events"
    }
    
    override func setupUI() {
        if let event = event {
            eventNameLabel?.text = event.title
        }
        if purchaseType == .table {
            purchaseOrReserveLabel?.text = "Reserve"
        } else {
            purchaseOrReserveLabel?.text = "Purchase"
        }
        eventPurchaseType?.text = "\(priceType.rawValue) \(purchaseType.rawValue)"
        fullnameLabel?.text = AppDelegate.shared().loggedUser?.fullName
        emailLabel?.text = AppDelegate.shared().loggedUser?.email
        priceLabel?.text = "₦\(price.commasNumber())"
        upButton.extendedEdges = UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
        downButton.extendedEdges = UIEdgeInsets(top: 0, left: 20, bottom: 20, right: 20)
    }
    
    //MARK: - Actions
    @IBAction func onPay(_ sender: UIButton) {
        guard let purchaseType = purchaseType else { return }
        switch purchaseType {
        case .ticket:
            viewModel.requestInitializeEventTicketTransaction(eventId: event?.id ?? "",
                                                              ticketType: priceType.rawValue.lowercased())
            {[weak self] (transation) in
                self?.transaction = transation
                self?.performSegue(withIdentifier: Constant.SegueIdentifier.goToConfirm, sender: nil)
            }
        case .table:
            viewModel.requestInitializeEventTableTransaction(eventId: event?.id ?? "",
                                                             tableType: priceType.rawValue.lowercased())
            {[weak self] (transation) in
                self?.transaction = transation
                self?.performSegue(withIdentifier: Constant.SegueIdentifier.goToConfirm, sender: nil)
            }
        default:
            break
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let confirmVC = segue.destination as? ConfirmationViewController {
            confirmVC.event = self.event
            confirmVC.priceType = self.priceType
            confirmVC.purchaseType = self.purchaseType
            confirmVC.totalPrice = priceLabel?.text ?? ""
            confirmVC.viewModel = viewModel
            confirmVC.transaction = transaction
        }
    }
    @IBAction func increaseQuantity(_ sender: LargeExpandButton) {
        viewModel.quantity += 1
        let totalPrice = viewModel.quantity * price
        quantityLabel?.text = String(viewModel.quantity)
        priceLabel?.text = "₦\(totalPrice.commasNumber())"
    }
    
    @IBAction func decreaseQuantity(_ sender: LargeExpandButton) {
        viewModel.quantity -= 1
        if viewModel.quantity < 1 {
            viewModel.quantity = 1
            return
        }
        let totalPrice = viewModel.quantity * price
        quantityLabel?.text = String(viewModel.quantity)
        priceLabel?.text = "₦\(totalPrice.commasNumber())"
    }
    
}
