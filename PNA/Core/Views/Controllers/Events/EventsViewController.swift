//
//  EventsViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let goToDetail = "showDetail"
    }
}

class EventsViewController: BaseViewController {

    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var myEventsView: UIView!
    @IBOutlet weak var myEventsSwitch: UISwitch!
    
    //MARK: Variables
    var viewModel = EventViewModel()
    var events: [[Event]] = []
    
    //MARK: View Controller life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Events"
    }
    
    override func setupObservable() {
        viewModel
            .events
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                if self.myEventsSwitch.isOn {
                    self.events = [self.viewModel.myWeekEvents,
                                   self.viewModel.myMonthEvents,
                                   self.viewModel.myUpcomingEvents]
                } else {
                    self.events = [self.viewModel.weekEvents,
                                   self.viewModel.monthEvents,
                                   self.viewModel.upcomingEvents]
                }
                self.tableView.reloadData()
                self.tableView.pullToRefreshView.stopAnimating()
            }).disposed(by: rx.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getEvents()
    }
    
    override func backAction() {
        self.menuAction()
    }
    
    override func setupUI() {
        tableView?.tableFooterView = UIView()
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 140
        tableView?.addPullToRefresh(actionHandler: {[weak self] in
            self?.viewModel.getEvents()
        })
        self.view.addSubview(myEventsView)
        myEventsView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30)
        tableView?.tableHeaderView = myEventsView
        tableView?.tableFooterView = nil
        myEventsSwitch?.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        myEventsSwitch?.isOn = false
        myEventsSwitch.layer.cornerRadius = myEventsSwitch.bounds.height / 2
        tableView?.pullToRefreshView?.activityIndicatorViewStyle = .gray
        tableView?.pullToRefreshView?.arrowColor = .gray
        tableView?.pullToRefreshView?.titleLabel?.text = nil
        tableView?.pullToRefreshView?.subtitleLabel?.text = nil
        removeNavigationBarItem()
        setNavigationBarItem()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailVC = segue.destination as? EventDetailViewController else { return }
        guard let indexPath = tableView?.indexPathForSelectedRow else { return }
        detailVC.event = self.events[indexPath.section][indexPath.row]
    }
    
    //MARK: - Actions
    @IBAction func onMyEventsSwitch(_ sender: UISwitch) {
        viewModel.getEvents()
    }
    
    @IBAction func unwindFromPaymentStatusVC(_ segue: UIStoryboardSegue) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: Constant.SegueIdentifier.goToDetail, sender: nil)            
        }
    }
    
    @IBAction func unwindFromEventPaymentStatusVC(_ segue: UIStoryboardSegue) {
        print("payment success")
    }
    
}

extension EventsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as? EventTableViewCell {
            let event = events[indexPath.section][indexPath.row]
            event.indexPath = indexPath
            cell.event = event
            cell.config(event: event)
            cell.photoLoadComplete = { (idxPath) in
                guard let idxPath = idxPath else { return }
                tableView.reloadRows(at: [idxPath], with: .fade)
            }
            return cell
        } else {
            return UITableViewCell()
        }
    }
}

extension EventsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = EventsHeaderView()
        switch section {
        case EventGroupType.thisWeek.hashValue:
            view.titleLabel.text = "This Week"
        case EventGroupType.thisMonth.hashValue:
            view.titleLabel.text = "This Month"
        case EventGroupType.upcoming.hashValue:
            view.titleLabel.text = "Upcoming Events"
        default:
            break
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if events[section].isEmpty {
            return 0
        }
        return 35
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if AppDelegate.shared().loggedUser?.membershipType == MembershipType.regular.rawValue
            && events[indexPath.section][indexPath.row].eventType == MembershipType.premium.rawValue {
            let premiumVC = PremiumEventViewController()
            premiumVC.name = events[indexPath.section][indexPath.row].title
            premiumVC.onProceedClousure = {[weak self] in
                let paymentVC = UIStoryboard.instantiate(PaymentSubscriptionViewController.self,
                                                         storyboardType: .payment)
                paymentVC.paymentType = .event
                self?.navigationController?.pushViewController(paymentVC, animated: true)
            }
            present(premiumVC, animated: true, completion: nil)
        } else {
            performSegue(withIdentifier: Constant.SegueIdentifier.goToDetail, sender: nil)
        }
    }
}
