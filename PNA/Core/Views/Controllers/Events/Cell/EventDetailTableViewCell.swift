//
//  EventDetailTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct ButtonTitle {
        static let add = "Add to Calendar"
        static let remove = "Remove from Calendar"
        static let purchase = "Purchase"
    }
    struct ButtonColor {
        static let add = UIImage.from(color: .black)
        static let remove = UIImage.from(color: UIColor.init(hex: "999999"))
        static let purchase = UIImage.from(color: .black)
    }
    struct Radius {
        static let button: CGFloat = 10
    }
    struct Message {
        static let addSuccess = "Added to Calendar"
        static let removeSuccess = "Removed from Calendar"
    }
}

class EventDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var eventPhotoImageView: UIImageView!
    @IBOutlet weak var venueLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dressCodeLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var organizerLabel: UILabel!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ticketView: UIView!
    @IBOutlet weak var tableView: UIView!
    @IBOutlet weak var regularTicketPriceLabel: UILabel!
    @IBOutlet weak var vipTicketPriceLabel: UILabel!
    @IBOutlet weak var regularTablePriceLabel: UILabel!
    @IBOutlet weak var vipTablePriceLabel: UILabel!
    @IBOutlet weak var ticketViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var event: Event?
    var viewModel = EventViewModel()
    weak var delegate: EventDetailTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func config() {
        guard let event = event else { return }
        titleLabel?.text = event.title
        imageHeightConstraint.constant = event.imageHeight
        if let url = URL(string: event.picURL ?? "") {
            eventPhotoImageView?.sd_setImage(with: url, placeholderImage: UIImage(), options: .cacheMemoryOnly, completed: {[weak self] (image, _, _, _) in
                if event.imageHeight != 200 { return }
                guard let image = image else { return }
                let ratio = image.height / image.width
                event.imageHeight = UIScreen.main.bounds.width * ratio
                self?.delegate?.didReceiveImage()
            })
        }
        addButton?.roundCorner(radius: Constant.Radius.button)
        venueLabel?.text = event.venue
        addressLabel?.text = event.address
        dateLabel?.text = event.date
        timeLabel?.text = event.time
        dressCodeLabel?.text = event.dressCode
        infoLabel?.text = event.info
        addButton?.isHidden = !isValidEvent(event: event)
        organizerLabel?.text = event.organizer
        if event.isPaidEvent || event.hasTableReservation {
            addButton?.setTitle(Constant.ButtonTitle.purchase, for: .normal)
            addButton?.setBackgroundImage(Constant.ButtonColor.purchase, for: .normal)
            if event.hasRSVP {
                addButton?.isHidden = true
            }
        } else {
            if event.hasRSVP {
                setupRemoveForButton()
            } else {
                setupAddForButton()
            }
        }
        if let tickets = event.tickets {
            regularTicketPriceLabel?.text = "N\(Int(tickets["regular"] ?? "")?.commasNumber() ?? "")"
            vipTicketPriceLabel?.text = "N\(Int(tickets["vip"] ?? "")?.commasNumber() ?? "")"
        } else {
            ticketViewHeightConstraint?.constant = 0
            ticketView?.isHidden = true
        }
        if let tables = event.tables {
            regularTablePriceLabel?.text = "N\(Int(tables["regular"] ?? "")?.commasNumber() ?? "")"
            vipTablePriceLabel?.text = "N\(Int(tables["vip"] ?? "")?.commasNumber() ?? "")"
        } else {
            tableViewHeightConstraint?.constant = 0
            tableView?.isHidden = true
        }
    }
    
    private func setupAddForButton() {
        addButton?.setBackgroundImage(Constant.ButtonColor.add, for: .normal)
        addButton?.setTitle(Constant.ButtonTitle.add, for: .normal)
    }
    
    private func setupRemoveForButton() {
        addButton?.setBackgroundImage(Constant.ButtonColor.remove, for: .normal)
        addButton?.setTitle(Constant.ButtonTitle.remove, for: .normal)
    }
    
    private func isValidEvent(event: Event) -> Bool {
        if let parseDate = event.parseDate {
            if parseDate.range(of:"ago") == nil {
                return true
            }
        }
        return false
    }
    
    @IBAction func addOrRemoveEvent(_ sender: UIButton) {
        guard let event = event else { return }
        if event.isPaidEvent || event.hasTableReservation {
            delegate?.tableView(self, didTapOnPurchase: sender)
            return
        }
        if event.hasRSVP {
            let title = event.title ?? ""
            Ultilites.removeEventFromCalendar(title: title)
            Ultilites.removeReminderFromReminders(title: title)
            self.requestAPIAddOrRemoveCalendar()
        } else {
            let title = event.title ?? ""
            let notes = "\(event.organizer ?? "")"
            let address = "\(event.address ?? "")"
            let dateString = "\(event.date ?? "") - \(event.time ?? "")"
            guard let date = dateString.getDateFromString(format: "yyyy-MM-dd - hh:mm a") else { return }
            Ultilites.addEventToCalendar(title: title, notes: notes, address: address, startDate: date, endDate: date) {[weak self] in
                Ultilites.addReminderToReminders(title: title, date: date, completion: {[weak self] in
                    self?.requestAPIAddOrRemoveCalendar()
                })
            }
        }
        
    }
    
    private func requestAPIAddOrRemoveCalendar() {
        UIViewController.topMostViewController()?.showProgress()
        viewModel.addOrRemoveToCalendar(eventId: event?.id ?? "") {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let error = error {
                Alert.showAlertWithErrorMessage(error.message)
            } else {
                if let event = response as? Event {
                    self?.event = event
                    if event.hasRSVP {
                        Alert.showAlertWithErrorMessage(Constant.Message.addSuccess)
                        self?.setupRemoveForButton()
                    } else {
                        Alert.showAlertWithErrorMessage(Constant.Message.removeSuccess)
                        self?.setupAddForButton()
                    }
                }
            }
        }
    }
}

protocol EventDetailTableViewCellDelegate: class {
    func tableView(_ cell: UITableViewCell, didTapOnPurchase sender: UIButton)
    func didReceiveImage()
}
