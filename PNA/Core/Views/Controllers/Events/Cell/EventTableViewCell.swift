//
//  EventTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import SDWebImage

private struct Constant {
    struct EdgeLength {
        static let numberOfRSVPsLabel: CGFloat = 30
        static let iconImageView: CGFloat = 30
    }
}

class EventTableViewCell: UITableViewCell {

    //MARK: - Oulets
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var daysToGoLabel: PaddingLabel!
    @IBOutlet weak var timeLabel: PaddingLabel!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var logImageView: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    
    
    var photoLoadComplete: ((IndexPath?)->())?
    var isLoadedImage = false
    var event: Event?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func clear() {
        eventNameLabel?.text = nil
        eventImageView?.image = UIImage()
        daysToGoLabel?.text = nil
        timeLabel?.text = nil
//        logImageView.isHidden = true
    }
    
    func config(event: Event) {
        eventNameLabel?.text = event.title

        let image = UIImage.init(named: "pna_africa_map")
        eventImageView.image = image
        if let url = URL(string: event.picURL ?? "") {
            eventImageView.sd_setImage(with: url, placeholderImage: image, options: [], completed: { [weak self] (image, error, cacheType, imageURL) in
                guard let `self` = self else { return }
                if self.isLoadedImage { return }
                if let image = image {
                    /** Requirement 25/7/2018 fixed height of image
                     let ratio = image.size.width / image.size.height
                     self.imageHeightConstraint.constant = UIScreen.main.bounds.width / ratio
                     self.event?.imageHeight = self.imageHeightConstraint.constant
                     */
                    if let photoImageLoadComplete = self.photoLoadComplete {
                        self.isLoadedImage = true
                        //photoImageLoadComplete(event.indexPath)
                    }
                }
            })
        }
        logImageView.isHidden = event.eventType != MembershipType.premium.rawValue
        daysToGoLabel?.text = event.parseDate
        timeLabel?.text = event.time
        let date = event.date?.getDateFromString(format: "YYYY-MM-dd")
        dayLabel?.text = "\(date?.day ?? 0)"
        monthLabel?.text = "\(date?.monthName(ofStyle: .threeLetters) ?? "")."
    }

}

protocol EventTableViewCellDelegate {
    func tableView(_ tableView: UITableView, didTapOn cell: UITableViewCell)
}
