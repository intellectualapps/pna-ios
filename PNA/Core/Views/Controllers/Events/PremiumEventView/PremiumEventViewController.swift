//
//  PremiumEventViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/11/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class PremiumEventViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var contentLabel: UILabel!

    //MARK: - Variables
    var paymentViewModel = PaymentSubscriptionViewModel()
    var name: String?
    var onProceedClousure: (()->())?
    
    //MARK: - Inititials
    init() {
        super.init(nibName: "PremiumEventViewController", bundle: Bundle.main)
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentViewModel.requestAPIGetAmount {[weak self] (amount) in
            guard let `self` = self else { return }
            self.contentLabel?.text = "\(self.name ?? "") restricted to Preimium Members only. Click 'Proceed' to subscribe at N\(amount) for 12 months."
        }
    }
    
    //MARK: - Actions
    @IBAction func onProceed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        onProceedClousure?()
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
