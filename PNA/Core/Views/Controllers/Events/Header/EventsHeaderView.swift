//
//  EventsHeaderView.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/8/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class EventsHeaderView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("EventsHeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
}
