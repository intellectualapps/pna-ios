//
//  EventDetailViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/8/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct SegueIdentifier {
        static let goToPurchase = "goToPurchase"
    }
    struct CellIdentifier {
        static let eventDetailCell = "EventDetailCell"
    }
}

class EventDetailViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Local variable
    var event: Event?
    var viewModel = EventViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Event Details"
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let event = event else { return }
        if let ticketAndTableVC = segue.destination as? TicketAndTableViewController {
            ticketAndTableVC.event = event
        }
    }
}

extension EventDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifier.eventDetailCell, for: indexPath) as! EventDetailTableViewCell
        cell.event = self.event
        cell.delegate = self
        cell.config()
        return cell
    }
}

extension EventDetailViewController: EventDetailTableViewCellDelegate {
    func tableView(_ cell: UITableViewCell, didTapOnPurchase sender: UIButton) {
        performSegue(withIdentifier: Constant.SegueIdentifier.goToPurchase, sender: nil)
    }
    
    func didReceiveImage() {
        tableView.reloadData()
    }
}
