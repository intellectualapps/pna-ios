//
//  FeedComentCell.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import SDWebImage

class FeedCommentCell: UITableViewCell {

    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var contentlbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        avatarView.roundCorner(radius: 35 / 2)
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func clear() {
        avatarView?.image = UIImage.defaultAvatar()
        nameLbl?.text = nil
        timeLbl?.text = nil
        contentlbl?.text = nil
    }

    func config(commentInfo: FeedCommentInfo) {
        if let url = URL(string: commentInfo.commentAuthorPicture) {
            avatarView?.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar(), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
        }
        nameLbl?.text = commentInfo.author
        timeLbl?.text = commentInfo.commentDate?.stringWithUnixTime()
        contentlbl?.text = commentInfo.content
    }
}
