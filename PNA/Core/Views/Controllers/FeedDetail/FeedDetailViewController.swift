//
//  FeedDetailViewController.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import SDWebImage
import IQKeyboardManagerSwift

class FeedDetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var viewModel = FeedDetailViewModel()
    fileprivate let bag = DisposeBag()
    var delegate: FeedDetailProtocol?
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //`self.navigationController?.navigationBar.top= "Feed"
        
        self.tableView?.registerCell(FeedCommentCell.self)
        //observer
        viewModel.comments.asObservable()
            .subscribe(onNext: {[weak self] (comments) in
                self?.reload()
            }).disposed(by: rx.disposeBag)
        viewModel.feedInfo.asObservable()
            .subscribe(onNext: {[weak self] (feedInfo) in
                self?.reload()
            }).disposed(by: rx.disposeBag)

        //setup
        setupTableView()

        getComments(isLoadMore: false, isNeedProcess: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        viewModel.feedInfo.value.isViewHorizontalLine = true
    }

    func setupTableView() {
        tableView?.estimatedRowHeight = 200
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        tableView?.translatesAutoresizingMaskIntoConstraints = false
        tableView?.addPullToRefresh(actionHandler: {[weak self] in
            self?.getComments(isLoadMore: false, isNeedProcess: true)
        })
    }

    func getComments(isLoadMore: Bool, isNeedProcess: Bool) {
        //load comments
        let email = AppDelegate.shared().loggedUser?.email ?? ""
        let feedid = self.viewModel.feedInfo.value.feedid
        let page = !isLoadMore ? 0 : viewModel.startPage()
        viewModel.getFeedComments(email: email,
                                  feedid: feedid,
                                  isLoadMore: false,
                                  start: page,
                                  isNeedProcess: isNeedProcess)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    @IBAction func postCommentAction(_ sender: Any) {
        let pid = self.viewModel.feedInfo.value.feedid
        Dialog.showDialog(inController: self, postid: pid) {[weak self] in
            self?.getComments(isLoadMore: false, isNeedProcess: false)
        }
    }


    func reload() {
        DispatchQueue.main.async {[weak self] in
            self?.tableView?.pullToRefreshView?.stopAnimating()
            let lastoffset = self?.tableView?.contentOffset ?? .zero
            self?.tableView?.reloadData()
            self?.tableView?.contentOffset = lastoffset
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Comments"
        }
        else {
            return ""
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return viewModel.comments.value.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        } else if indexPath.section == 1 {
            let info = viewModel.comments.value[indexPath.row]
            return info.height
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.identifier, for: indexPath)
            return cell
        }
        return tableView.dequeueReusableCell(withIdentifier: "FeedCommentCell", for: indexPath)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let commentCell = cell as? FeedCommentCell {
            let comment = viewModel.comments.value[indexPath.row]
            commentCell.config(commentInfo: comment)
        } else if let feedcell = cell as? FeedCell {
            feedcell.delegate = self
            
            let info = self.viewModel.feedInfo.value
            info.isViewHorizontalLine = false
            feedcell.config(feedInfo: info)
            feedcell.largeImageLoadComplete = {
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }
    }
    
    func likeSelectedAction(feed: FeedInfo?) {
        viewModel
            .changeHasUserLikedForFeed(feed: feed)
            .subscribe(onNext: { [weak self] in
                self?.delegate?.likeButtonTapped(feedInfo: feed)
                self?.reload()
            }).disposed(by: bag)
    }
    
}

extension FeedDetailViewController: FeedCellDelegate {
    func wantToShowProfileOfFeedOwner(feed: FeedInfo?) {
        self.showProfileDetail(userid: 0)
    }
    
    func onLikedTapped(feed: FeedInfo?) {
        self.likeSelectedAction(feed: feed)
    }
}

protocol FeedDetailProtocol {
    func likeButtonTapped(feedInfo: FeedInfo?)
}
