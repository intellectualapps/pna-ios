//
//  SingUpViewController.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class SingUpViewController: BaseViewController {
    //MARK: UI Elements
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: Local variables
    var viewModel = SignUpViewModel()
    var disposed = DisposeBag();
    //MARK: UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Action
    @IBAction func continueAction(_ sender: Any) {
        let success = self.registerValidate()
        if success {
            self.registerAction()
        }
    }


    func showCreateProfileScreen(is_first_regist:Bool = true) {
        DispatchQueue.main.async {
            let controller =             UIStoryboard.instantiate(ProfileViewController.self, storyboardType: .createProfile)
            controller.is_first_regist = is_first_regist
            self.navigationController?.pushViewController(controller, animated: true)
        } 
    }
    
    //MARK: View setup
    func setupView() {
        firstNameTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.firstName = value
                }
            })
            .disposed(by: disposed)
        
        lastNameTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.lastName = value
                }
            })
            .disposed(by: disposed)
        
        emailTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.email = value
                }
            })
            .disposed(by: disposed)
        
        passwordTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.password = value
                }
            })
            .disposed(by: disposed)
        
       
        phoneTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.phone = value
                }
            })
            .disposed(by: disposed)
        
    }
   
    
    private func registerAction() {
        self.view?.endEditing(true)
        self.showProgress()
        self.viewModel.excuteRegister(completion: {[weak self] (registerResult, error) in
            self?.hideProgress()
            if let err = error {
               Alert.showAlertWithErrorMessage(err.message)
            }else{
                self?.showCreateProfileScreen(is_first_regist: true)
            }
        })
    }
    
    
    
    //MARK: validation
    
    func registerValidate() -> Bool {
        guard let firstname = firstNameTextField?.text, !firstname.isEmpty else {
            Alert.showAlertWithErrorMessage("register first name is required")
            return false
        }
        
        guard let lastname = lastNameTextField?.text, !lastname.isEmpty else {
            Alert.showAlertWithErrorMessage("register last name is required")
            return false
        }
        
        guard let email = emailTextField?.text, !email.isEmpty else {
            Alert.showAlertWithErrorMessage("register email is required")
            return false
        }
        if !email.isValidEmail() {
            Alert.showAlertWithErrorMessage("email is invalid")
            return false
        }
        
        guard let password = passwordTextField?.text, !password.isEmpty else {
            Alert.showAlertWithErrorMessage("password is required")
            return false
        }
        
        guard let phonenumber = phoneTextField?.text, !phonenumber.isEmpty else {
            Alert.showAlertWithErrorMessage("phone number is required")
            return false
        }
        
        if phonenumber.length < 11 {
            Alert.showAlertWithErrorMessage("The phone number must be at least 11 digits")
            return false
        }
        
        return true
    }
}
