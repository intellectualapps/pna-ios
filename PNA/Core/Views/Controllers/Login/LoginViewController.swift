//
//  LoginViewController.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import PKHUD
import FacebookLogin
import FBSDKLoginKit
import APESuperHUD

class LoginViewController: BaseViewController {
    //MARK: UI Elements
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var facebookLoginView: UIStackView!
    //MARK: Local variables
    var viewModel = LoginViewModel()
    var disposed = DisposeBag();
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // constraint position of forgot password and sign up button with facebook login view
        setupSignupAndForgotPasswordBtnPosition()
        
    }

    //MARK: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // constraint position of forgot password and sign up button with facebook login view
    func setupSignupAndForgotPasswordBtnPosition() {
        let fbViewPosition = facebookLoginView.frame.origin
        signupBtn.frame.origin.x = fbViewPosition.x
        forgotPasswordBtn.frame.origin.x = fbViewPosition.x + facebookLoginView.frame.width - forgotPasswordBtn.frame.width

    }
    //MARK:Action
    @IBAction func loginAction(_ sender: Any) {
        self.view?.endEditing(true)
        let validated:Bool = validate()
        if validated {
            self.showProgress()
            self.viewModel.auth_type = "email"
            self.viewModel.login (completion: {[weak self] (loginResult, error) in
                self?.hideProgress()
                if let err = error {
                    Alert.showAlertWithErrorMessage(err.message)
                }else{
                    self?.gotoHomeScreenOrInterestSelection()
                    self?.viewModel.storeDeviceToken(completion: {(_, error) in
                        if let error = error {
                            print(error)
                        }
                    })
                }
            })
        }
    }

    func gotoHomeScreenOrInterestSelection() {
        guard let user = AppDelegate.shared().loggedUser else {
            return
        }
        let isFirstTime = !Defaults.isNotFirstTime.getBool()
        if user.alreadyCreateProfile() && !isFirstTime {
            self.showHomeScreen()
        } else {
            if !user.alreadyCreateProfile() {
                let controller =             UIStoryboard.instantiate(ProfileViewController.self, storyboardType: .createProfile)
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                //show interest screen
                let storyboard = UIStoryboard.init(name: "Interest", bundle: nil)
                if let controller = storyboard.instantiateInitialViewController() {
                    let navi = UINavigationController(rootViewController: controller)
                    self.present(navi, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func facebookAuthen(_ sender: Any) {
        self.showProgress()
        self.viewModel.auth_type = "facebook"
        self.facebookSignUp()
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        Alert.showConfirmAlert("Confirm", message: "Did you forget your password?", cancelBtnTitle: "Cancel", okBtnTitle: "OK", presentController: self) { (bool) in
            if !bool { return }
            guard let email = self.emailTextField?.text, !email.isEmpty else {
                Alert.showAlertWithErrorMessage("Your email is invalid")
                return
            }
            if !email.isValidEmail() {
                Alert.showAlertWithErrorMessage("Your email is invalid")
                return
            }
            self.showProgress()
            self.viewModel.forgotPassword { (result, error) in
                self.hideProgress()
                if let result = result as? ApiResponseBasicModel {
                    Alert.showAlertWithErrorMessage(result.message)
                }
            }
        }
    }

    @IBAction func signUpAction(_ sender: Any) {
        showSignUpScreen()
    }
    
    //MARK: View setup
    func setupView() {
        emailTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.email = value
                }
            })
            .disposed(by: disposed)
        
        passwordTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.password = value
                }
            })
            .disposed(by: disposed)
        
    }
    func validate() -> Bool {
        
        guard let email = emailTextField?.text, !email.isEmpty else {
            Alert.showAlertWithErrorMessage("register first name is required")
            return false
        }
        if !email.isValidEmail() {
            Alert.showAlertWithErrorMessage("register first name is required")
            return false
        }
        
        guard let password = passwordTextField?.text, !password.isEmpty else {
            Alert.showAlertWithErrorMessage("password is required")
            return false
        }
        
        return true
    }

    
    func showSignUpScreen(_ animated:Bool = true){
        DispatchQueue.main.async {
            let controller = UIStoryboard.instantiate(SingUpViewController.self, storyboardType: .signUp)
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    func facebookSignUp() {
        APESuperHUD.show(style: .textOnly, title: "", message: "pleasewait".localized(), completion: nil)
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { [weak self] (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
                self?.showError(message: "An Error ocurred while trying to sign in, please try again ")
            case .cancelled:
                self?.showError(message: "You have cancelled access to your facebook profile, please allow access to sign in to Pentor")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                if declinedPermissions.count > 0 {
                    self?.showError(message: "You have denied permission to your facebook 'public_profile', please allow access to sign in to Pentor")
                }else if grantedPermissions.count > 0 {
                    self?.fetchFacebookUserProfile()
                }else if accessToken.expirationDate < Date() {
                    self?.showError(message: "AccessToken invalid")
                }else {
                    self?.showError(message: "Unknow error")
                }
            }
        }
    }
    
    func fetchFacebookUserProfile() {
        
        if (FBSDKAccessToken.current()) != nil {
            
            FBSDKGraphRequest(graphPath: "me",
                              parameters: ["fields":"name,email,gender,first_name,last_name,locale,location,picture.type(large)"],
                              httpMethod: "GET")
                .start(completionHandler: { [weak self] (connection,  result, err) in
                    guard let strongself = self else { return }
                    if let error = err {
                        var message = "unknow error"
                        if let meg = error as? String {
                            message = meg
                        }
                        strongself.showError(message: message )
                    }else {
                        guard let user = result as? [String: Any] else { return }
                        
                        if let email = user["email"] as? String {
                            strongself.viewModel.email = email
                            strongself.viewModel.login (completion: {(loginResult, error) in
                                self?.hideProgress()
                                if let err = error {
                                    strongself.showError(message: err.message )
                                }else{
                                    strongself.showHomeScreen()
                                }
                            })
                            
                        }else {
                            strongself.showError(message: "email not found" )
                        }
                    }
                })
        }
    }
    
    func showError(message: String) {
        APESuperHUD.show(style: HUDStyle.textOnly, title: "", message: message, completion: nil)
    }
}
