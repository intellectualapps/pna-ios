//
//  PhotoSectionController.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import IGListKit
protocol ListSectionControllerDelegate: NSObjectProtocol {
    func sectionControllerDidSelectItem(index: Int, model: Any)
    func scrollView() -> UICollectionView?
}

class  SectionController: ListSectionController {
    weak var delegate: ListSectionControllerDelegate?

    var sectionInfo: IGListSectionModel?

    override func didSelectItem(at index: Int) {
        if let current = sectionInfo?.contents[index]  {
            delegate?.sectionControllerDidSelectItem(index: index, model: current)
        }
    }

    func model(at index: Int) -> Any? {
        if let current = sectionInfo?.contents[index]  {
            return current
        }
        return nil
    }
}

class PhotoSectionController: SectionController {
    override init() {
        super.init()
        self.minimumInteritemSpacing = 1
        self.minimumLineSpacing = 10
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    override func numberOfItems() -> Int {
        return sectionInfo?.contents.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        let itemWidth = 70
        return CGSize(width: itemWidth, height: 65)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(withNibName: "PhotoCell", bundle: nil, for: self, at: index) as? PhotoCell else {
            fatalError()
        }
        cell.backgroundColor = UIColor.clear
        if let current = sectionInfo?.contents[index] as? String {
            cell.configCell(data: current)
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        self.sectionInfo = object as? IGListSectionModel
    }
}
