//
//  TextHeaderView.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class TextHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var label: UILabel!
}
