//
//  ProfileDetailSumaryHeaderView.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import IGListKit
import RxSwift
import RxCocoa
import PureLayout

class ProfileDetailSumaryHeaderView: UITableViewHeaderFooterView, ListAdapterDataSource {
    var collectionView: UICollectionView!
    @IBOutlet weak var photoContainerView: UIView!
    @IBOutlet weak var chatButton: UIButton!
    
    lazy var adapter: ListAdapter = {
        let dt =  ListAdapter(updater: ListAdapterUpdater(), viewController: nil)
        dt.collectionView = self.collectionView
        dt.dataSource = self
        return dt
    }()

    var dataSource = [Any]()
    var photos = [String]()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }

    func setup() {

        let layout = ListCollectionViewLayout(stickyHeaders: false, scrollDirection: UICollectionView.ScrollDirection.horizontal, topContentInset: 0, stretchToEdge: false)

        collectionView = UICollectionView(frame: photoContainerView.bounds, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        self.photoContainerView?.addSubview(collectionView)
        collectionView?.autoPinEdgesToSuperviewEdges()
        collectionView?.isScrollEnabled = true
        self.collectionView?.backgroundColor = .clear
        self.collectionView?.backgroundView?.backgroundColor = .clear
        backgroundColor = UIColor.clear
        self.reload()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
//        setup()
//        for _ in 0..<10 {
//            photos.append("large_avatar")
//        }
//        self.reload()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    func reload() {
        self.dataSource.removeAll()
        let listObjs = IGListSectionModel()

        let nib = UINib(nibName: "PhotoCell", bundle: nil)
        self.collectionView?.register(nib, forCellWithReuseIdentifier: "PhotoCell")
        listObjs.contents = photos
        self.dataSource.append(listObjs)
        self.adapter.reloadData { (complete) in
        }
    }

    //MARK: Actions
    func sectionController() -> PhotoSectionController {
        let sectionController = PhotoSectionController()
        return  sectionController
    }

    //MARK: IGList delegate
    func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }

    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return dataSource.map { $0 as! ListDiffable }
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let controller = self.sectionController()
        controller.delegate = self
        return controller
    }

    func didSelectItem(at index: Int) {
    }
}


extension ProfileDetailSumaryHeaderView: ListSectionControllerDelegate {
    func scrollView() -> UICollectionView? {
        return self.collectionView
    }

    func sectionControllerDidSelectItem(index: Int, model: Any) {
    }
}
