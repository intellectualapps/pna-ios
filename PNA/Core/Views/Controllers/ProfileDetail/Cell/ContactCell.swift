//
//  ContactCell.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class ContactCell: UITableViewCell {

    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var facebookbtn: UIButton!

    @IBOutlet weak var intragramBtn: UIButton!

    @IBOutlet weak var removeNetworkBtn: UIButton!

    @IBAction func twitterBtnAction(_ sender: Any) {
    }

    @IBAction func facebookBtnAction(_ sender: Any) {
    }

    @IBAction func intagramBtnAction(_ sender: Any) {
    }
    
}
