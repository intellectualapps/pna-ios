//
//  TextCell.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class TextCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
}
