//
//  PhotoCell.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import IGListKit
import RxSwift
import RxCocoa

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        imageView?.image = nil
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
    }

    func configCell(data: Any?) {
        if let name = data as? String {
            imageView?.image = UIImage(named: name)
        }
    }
}
