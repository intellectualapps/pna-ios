
//
//  StickyHeaderViewController.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import SnapKit
protocol SubScrollDelegate {
    func subScrollViewDidScroll(scrollView: UIScrollView)
}

let min_header: CGFloat = 22
let bar_offset: CGFloat = UIScreen.main.bounds.width * 0.75

class StickyHeaderViewController: BaseViewController, SubScrollDelegate {

    @IBOutlet weak var largeImageView: UIImageView!
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var headerViewHeightContraint: NSLayoutConstraint!
    
    // MARK: - local variable
    var scaleDetault = 0
    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.user == nil {
            self.user = AppDelegate.shared().loggedUser
        }
        addDefaultBackBtn()
        headerContainerView?.clipsToBounds = true
        loadUserInfo()
        self.title = "\(user?.firstName ?? "")'s Profile"
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        headerContainerView?.isHidden = true
    }

    override func backAction() {
        if self.navigationController?.popViewController(animated: true) == nil {
            self.showHomeScreen()
        }
    }

    func loadUserInfo() {
        guard let userInfo = user else { return }
        usernameLabel?.text = userInfo.fullName
        jobTitleLabel?.text = userInfo.jobTitle
        companyLabel?.text = userInfo.company
        if let urlString = userInfo.pic, !urlString.isEmpty, let url = URL(string: urlString) {
            showUserImage(url: url)
        }else {
            showUserInfoWithNoImage()
        }
    }
    
    func showUserInfoWithNoImage() {
        headerViewHeightContraint?.constant = 100
        largeImageView?.isHidden = true
    }
    
    func showUserImage(url: URL) {
        headerViewHeightContraint?.constant = 281
        largeImageView?.isHidden = false
        largeImageView?.sd_setImage(with: url, completed: nil)
    }
    
    func subScrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y + 64

        if offset < 0 {
            var headerTransform = CATransform3DIdentity
            let headerScaleFactor:CGFloat = (scaleDetault == 0 ) ? 0 : -(offset) / headerContainerView.bounds.height
            scaleDetault = 1
            let height = headerContainerView.bounds.height
            let headerSizevariation = ((height * (1.0 + headerScaleFactor)) - height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            headerContainerView.layer.transform = headerTransform
        } else {
            var headerTransform = CATransform3DIdentity
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-(headerContainerView.bounds.height-min_header), -offset), 0)
            headerContainerView.layer.transform = headerTransform
        }

        if (headerContainerView.bounds.height-min_header) < offset {
            headerContainerView.layer.zPosition = 3
        } else {
            headerContainerView.layer.zPosition = 0
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "content" {
            let profileVC = segue.destination as! ProfileDetailViewController
            profileVC.delegate = self
            profileVC.memberId = user?.memberId
        }
    }
}
