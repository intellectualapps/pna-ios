//
//  ProfileDetailViewController.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class ProfileDetailViewController: BaseViewController {

    var delegate: SubScrollDelegate?
    var sections = Variable([SectionInfo]())

    @IBOutlet weak var tbView: UITableView!

    // MARK: - local variables
    var memberId: Int?
    var viewModel = ProfileDetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
//        addDefaultBackBtn()
        self.removeNavigationBarItem()
        self.setNavigationBarItem()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let id = memberId , id > 0{
            viewModel.memberIdOfMyNetwork = id
            viewModel.getDetailUser()
        }else {
            if let loggedUser = AppDelegate.shared().loggedUser {
                viewModel.detailUser.value = loggedUser
            }
        }
        prepareDataSource()
    }
    
    override func setupObservable() {
        viewModel
            .detailUser
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (_) in
                self?.viewModel.getCountryName()
                self?.viewModel.getIndustryName()
            })
            .disposed(by: rx.disposeBag)
        viewModel
            .country
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (_) in
                self?.prepareDataSource()
                self?.tbView?.reloadData()
            })
            .disposed(by: rx.disposeBag)
        viewModel
            .state
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (_) in
                self?.prepareDataSource()
                self?.tbView?.reloadData()
            })
            .disposed(by: rx.disposeBag)
        viewModel
            .industry
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (_) in
                self?.prepareDataSource()
                self?.tbView?.reloadData()
            })
            .disposed(by: rx.disposeBag)
    }

    override func backAction() {
        self.menuAction()
    }

    func setupTableView() {
        tbView.dataSource = self
        tbView.delegate = self
        tbView.canCancelContentTouches = true
        tbView?.tableFooterView = UIView()
        tbView?.tableHeaderView = UIView()
        tbView?.translatesAutoresizingMaskIntoConstraints = false
        tbView?.sectionHeaderHeight = UITableView.automaticDimension
        tbView?.estimatedSectionHeaderHeight = 115
        tbView?.estimatedRowHeight = UITableView.automaticDimension
        tbView?.rowHeight = 40
        var hdXib = UINib(nibName: "ProfileDetailSumaryHeaderView", bundle: nil)
        tbView?.register(hdXib, forHeaderFooterViewReuseIdentifier: "ProfileDetailSumaryHeaderView")
        hdXib = UINib(nibName: "TextHeaderView", bundle: nil)
        tbView?.register(hdXib, forHeaderFooterViewReuseIdentifier: "TextHeaderView")
        var cellXib = UINib(nibName: "TextCell", bundle: nil)
        tbView?.register(cellXib, forCellReuseIdentifier: "TextCell")
        cellXib = UINib(nibName: "ContactCell", bundle: nil)
        tbView?.register(cellXib, forCellReuseIdentifier: "ContactCell")
    }

    func prepareDataSource() {
        var list = [SectionInfo]()
        let empty = SectionInfo(indentify: "", title: "", height: 0, index: 0) { () -> ([CellInfo]) in
            return []
        }
        list.append(empty)

        let sInfo = SectionInfo(indentify: "ProfileDetailSumaryHeaderView", title: "", height: 0, index: 0) { () -> ([CellInfo]) in
            return []
        }
        list.append(sInfo)

        var tsInfo = SectionInfo(indentify: "TextHeaderView", title: "Country", height: 0, index: 0) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            let cell = CellInfo(indentify: "TextCell", aCellId: 0, height: 0, content: viewModel.country.value, title: nil, cellColor: .clear, keyboardType: .default, capitalized: false)
            cells.append(cell)
            return cells
        }
        list.append(tsInfo)
        
        tsInfo = SectionInfo(indentify: "TextHeaderView", title: "Industry", height: 0, index: 0) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            let cell = CellInfo(indentify: "TextCell", aCellId: 0, height: 0, content: viewModel.industry.value, title: nil, cellColor: .clear, keyboardType: .default, capitalized: false)
            cells.append(cell)
            return cells
        }
        list.append(tsInfo)
        
        tsInfo = SectionInfo(indentify: "TextHeaderView", title: "Experience", height: 0, index: 0) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            let expString = prepareExp(with: viewModel)
            let cell = CellInfo(indentify: "TextCell", aCellId: 0, height: 0, content: expString, title: nil, cellColor: .clear, keyboardType: .default, capitalized: false)
            cells.append(cell)
            return cells
        }
        list.append(tsInfo)

        tsInfo = SectionInfo(indentify: "TextHeaderView", title: "Skills", height: 0, index: 0) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            let skills = (viewModel.detailUser.value.skills ?? "").replacingOccurrences(of: ",", with: "\n")
            let cell = CellInfo(indentify: "TextCell", aCellId: 0, height: 0, content: skills, title: nil, cellColor: .clear, keyboardType: .default, capitalized: false)
            cells.append(cell)
            return cells
        }
        list.append(tsInfo)
        
        tsInfo = SectionInfo(indentify: "TextHeaderView", title: "Phone Number", height: 0, index: 0) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            let cell = CellInfo(indentify: "TextCell", aCellId: 0, height: 0, content: viewModel.detailUser.value.phoneNumber, title: nil, cellColor: .clear, keyboardType: .default, capitalized: false)
            cells.append(cell)
            return cells
        }
        list.append(tsInfo)

        tsInfo = SectionInfo(indentify: "TextHeaderView", title: "Contact Info", height: 0, index: 0) { () -> ([CellInfo]) in
            var cells = [CellInfo]()
            let cell = CellInfo(indentify: "ContactCell", aCellId: 0, height: 0, content: nil, title: nil, cellColor: .clear, keyboardType: .default, capitalized: false)
            cells.append(cell)
            return cells
        }
        list.append(tsInfo)
        self.sections.value = list
        self.tbView?.reloadData()
    }

    func prepareExp(with viewModel: ProfileDetailViewModel) -> String {
        var string = ""
        for expDict in viewModel.detailUser.value.experience {
            let mainString = expDict["main"] as? String ?? ""
            let subString = "\n\(expDict["sub"] as? String ?? "")\n"
            string.append(mainString)
            string.append(subString)
        }
        return string
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tbView {
            self.delegate?.subScrollViewDidScroll(scrollView: scrollView)
        }
    }
}

extension ProfileDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.value.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return UIScreen.main.bounds.width * 0.75
        } else if section == 1 {
            if let _ = memberId {
                return 44
            } else {
                return 0
            }
        }
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sinfo = self.sections.value[section]
        return sinfo.cells.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sinfo = self.sections.value[section]
        let hdview = tableView.dequeueReusableHeaderFooterView(withIdentifier: sinfo.identifier)
        if let aview = hdview as? TextHeaderView {
            aview.label?.text = sinfo.title
        }
        if let aview = hdview as? ProfileDetailSumaryHeaderView {
            aview.chatButton?.isHidden = memberId == nil 
        }
        return hdview
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sinfo = self.sections.value[indexPath.section]
        let rowInfo = sinfo.cells[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: rowInfo.identifier, for: indexPath)
        if let acell = cell as? TextCell {
            acell.label?.text = rowInfo.content as? String
            return acell
        }
        if let ccell = cell as? ContactCell {
            ccell.removeNetworkBtn.isHidden = (memberId == nil || memberId == 0) ? true : false
            return ccell
        }
        return cell
    }
}
