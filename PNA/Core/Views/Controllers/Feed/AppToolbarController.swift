//
//  AppToolbarController.swift
//  PNA
//
//  Created by center12 on 1/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import Material
import SlideMenuControllerSwift

class AppToolbarController: ToolbarController {
    fileprivate var menuButton: IconButton!
    var feedVC: FeedViewController?
    
    open override func prepare() {
        super.prepare()
        prepareMenuButton()
        prepareStatusBar()
        prepareToolbar()
    }
}

fileprivate extension AppToolbarController {
    func prepareMenuButton() {
        menuButton = IconButton(image: Icon.cm.menu, tintColor: .white)
        menuButton.pulseColor = .white
        menuButton.addTarget(self, action: #selector(handleMenu(button:)), for: .touchUpInside)
    }
    
    func prepareStatusBar() {
        statusBarStyle = .lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = Color.blue.darken3
    }
    
    func prepareToolbar() {

        toolbar.backgroundColor = UIColor.init(hex: "000000")
        toolbar.leftViews = [menuButton]

        toolbar.title = "Feed"
        toolbar.titleLabel.textColor = .white
        toolbar.titleLabel.textAlignment = .left
    }
}

fileprivate extension AppToolbarController {
    @objc func handleMenu(button: UIButton) {
        guard let slideMenuController = feedVC?.slideMenuController()  else { return }
        slideMenuController.openLeft()
    }
}
