//
//  FeedCell.swift
//  PNA
//
//  Created by center12 on 1/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import SDWebImage

protocol FeedCellDelegate: NSObjectProtocol {
    func wantToShowProfileOfFeedOwner(feed: FeedInfo?)
    func onLikedTapped(feed: FeedInfo?)
}
class FeedCell: UITableViewCell {

    var feedInfo: FeedInfo?
    static var identifier = "FeedCellID"
    weak var delegate: FeedCellDelegate?
    //MARK: UI Elments

    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var largeImageView: UIImageView!
    @IBOutlet weak var feedPictureHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var avatarActionBtn: UIButton!
    @IBOutlet weak var likeLbl: UILabel!
    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var horizontalLine: UIView!
    @IBOutlet weak var heartImageView: UIImageView!
    
    @IBOutlet weak var contentTV: UITextView!
    @IBOutlet weak var jobTitleLbl: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    //MARK: Local variables
    var largeImageLoadComplete: (()->())?
    private let MAX_HEIGHT: CGFloat = 400
    var disposeBag = DisposeBag()
    //MARK: UI view
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentTV?.textContainer.lineFragmentPadding = 0
        contentTV?.textContainerInset = .zero
        likeButton?
            .rx
            .tap
            .asDriver()
            .debounce(0.5)
            .drive(onNext: {[weak self] (_) in
                self?.delegate?.onLikedTapped(feed: self?.feedInfo)
            }).disposed(by: disposeBag)
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func clear() {
        avatarView?.image = UIImage.defaultAvatar()
        largeImageView?.isHidden = true
        //largeImageView?.image = UIImage.defaultAvatar()
        contentTV?.text = nil
        likeLbl?.text = nil
        authorLbl?.text = nil
        timeLbl?.text = nil
        jobTitleLbl?.text = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(feedInfo: FeedInfo) {
        self.feedInfo = feedInfo
        clear()
        if let url = URL(string: feedInfo.feedAuthorPicture) {
            avatarView?.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar(), options: SDWebImageOptions.cacheMemoryOnly, completed: {[weak self] (image, error, type, url) in
            })
        }
        if let url = URL(string: feedInfo.feedPicUrl) {
            self.feedPictureHeightConstraint.constant = feedInfo.imageHeight
            largeImageView?.sd_setImage(with: url, placeholderImage: UIImage.defaultAvatar(), options: SDWebImageOptions.cacheMemoryOnly, completed: {[weak self] (image, error, type, url) in
                guard let `self` = self else { return }
                if feedInfo.imageHeight != 0 {
                    self.largeImageView?.isHidden = false
                    return
                }
                if let image = image {
                    let ratio = image.size.width / image.size.height
                    let constraint = self.frame.width / ratio
                    let imageHeight = constraint >= self.MAX_HEIGHT ? self.MAX_HEIGHT : constraint
                    feedInfo.imageHeight = imageHeight
                    self.feedPictureHeightConstraint.constant = constraint
                    if let largeImageLoadComplete = self.largeImageLoadComplete {
                        largeImageLoadComplete()
                    }
                    self.largeImageView?.isHidden = false
                }
            })
        }
        else {
            feedPictureHeightConstraint.constant = 0
        }
        contentTV?.text = feedInfo.content
        likeLbl?.text = feedInfo.likes.toString()
        authorLbl?.text = feedInfo.author
        timeLbl?.text = feedInfo.feedDate?.stringWithUnixTime()
        jobTitleLbl?.text = feedInfo.feedDescription
        
        if !feedInfo.isViewHorizontalLine {
            horizontalLine.isHidden = true
        }
        
        let heartImg = feedInfo.hasUserLiked ? "heart_selected" : "heart_unselected"
        if let img = UIImage(named: heartImg) {
            heartImageView?.image = img
        }
    }

    @IBAction func avatarAction(_ sender: Any) {
        //self.delegate?.wantToShowProfileOfFeedOwner(feed: self.feedInfo)  XXX Disable for now
    }
    
    @IBAction func likeBtnAction(_ sender: Any) {
        self.delegate?.onLikedTapped(feed: self.feedInfo)
    }
}
