//
//  FeedTableViewController.swift
//  PNA
//
//  Created by center12 on 1/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class FeedTableViewController: UITableViewController {
    @IBOutlet weak var avatarView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: setup view
    func setupView() {
        self.tableView.separatorStyle = .none
        avatarView?.roundCorner(radius: 45)
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 10
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.identifier, for: indexPath)


        return cell
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: Action
    
    @IBAction func menuAction(_ sender: Any) {
        guard let slideMenuController = self.slideMenuController()  else { return }
        slideMenuController.openLeft()
    }
    
}
