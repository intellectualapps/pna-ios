//
//  FeedViewController.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import RxSwift

private struct Constant {
    struct PopupHeight {
        static let showShareButton: CGFloat = 201.0
        static let hideShareButton: CGFloat = 247.0
    }
}

class FeedViewController: BaseViewController {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var postCommentView: PostCommentView!
    @IBOutlet fileprivate weak var coverButton: UIButton!
    @IBOutlet fileprivate weak var heightPopup: NSLayoutConstraint!
    @IBOutlet weak var feedTableView: UITableView! {
        didSet {
            feedTableView?.delegate = self
            feedTableView?.dataSource = self
        }
    }
    //MARK: Local variables
    var viewModel = FeedViewModel()
    var imageTmp: UIImage?
    var cloudinary = CloudinaryManage()
    fileprivate var isDismissImagePicker = false
    let bag = DisposeBag()
    var imageHeight: CGFloat = 0

    //MARK: UIview
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.setNavigationBarItem()
        if !isDismissImagePicker {
            viewModel.getFeedList(isLoadMore: false, start: 0)
        }
        //Observing
        viewModel
            .feeds
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] _ in
                self?.feedTableView.infiniteScrollingView.stopAnimating()
                self?.reloadTable()
            }).disposed(by: rx.disposeBag)
        postCommentView.didTappedCancel = { [weak self] in
            self?.dismissPostComment()
        }
        postCommentView.didTappedPost = { [weak self] (text) in
            // Call API Create Post
            guard let `self` = self else { return }
            self.viewModel.postText = text
            self.onPost()
        }
        postCommentView.didTappedSharePhoto = { [weak self] in
            self?.openGallary()
        }
    }

    func reloadTable() {
        let lastoffset = feedTableView?.contentOffset ?? .zero
        feedTableView?.reloadData()
        feedTableView?.contentOffset = lastoffset
    }

    override func backAction() {
        self.menuAction()
    }
    
    //MARK: setup view
    func setupView() {
        feedTableView?.addPullToRefresh(actionHandler: {[weak self] in
            self?.viewModel.getFeedList(isLoadMore: false, start: 0)
        })

        feedTableView?.addInfiniteScrolling(actionHandler: {[weak self] in
            if let strongself = self {
                let page = strongself.viewModel.startPage()
                strongself.viewModel.getFeedList(isLoadMore: true, start: page)
            }
        })
    }
    
    override func setupUI() {
        postCommentView.alpha = 0
        postCommentView.roundCorner(radius: 5.0)
        coverButton.alpha = 0
        coverButton.backgroundColor = UIColor(white: 0, alpha: 0.5)
        feedTableView.estimatedRowHeight = 200
        feedTableView.rowHeight = UITableView.automaticDimension
    }
    
    private func presentPostComment() {
        heightPopup.constant = Constant.PopupHeight.showShareButton
        UIView.animate(withDuration: 0.33, animations: {
            self.postCommentView.alpha = 1
            self.postCommentView.clearTextView()
            self.coverButton.alpha = 1
            self.view.layoutIfNeeded()
        })
    }
    
    private func dismissPostComment() {
        heightPopup.constant = Constant.PopupHeight.hideShareButton
        isDismissImagePicker = false
        viewModel.canUploadImage = false
        viewModel.postText = ""
        viewModel.imageURL = ""
        UIView.animate(withDuration: 0.33, animations: {
            self.postCommentView.alpha = 0
            self.coverButton.alpha = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func openGallary()
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func onPost() {
        UIViewController.topMostViewController()?.showProgress()
        if self.viewModel.canPost {
            if self.viewModel.canUploadImage {
                if let username = AppDelegate.shared().loggedUser?.fullName {
                    self.cloudinary.uploadAvatar(img: self.imageTmp, username: username, completion: { [weak self] (result, error) in
                        UIViewController.topMostViewController()?.hideProgress()
                        if let error = error {
                            Alert.showAlertWithErrorMessage(error.localizedDescription)
                            print("error \(error)")
                        } else {
                            if let result = result, let publicId = result.publicId, let version = result.version {
                                let id = "v\(version)/\(publicId)"
                                self?.viewModel.imageURL = self?.cloudinary.getAvatarUrl(public_id: id) ?? ""
                                self?.createPost()
                            }
                        }
                    })
                } else {
                    UIViewController.topMostViewController()?.hideProgress()
                }
            } else {
                UIViewController.topMostViewController()?.hideProgress()
                self.createPost()
            }
        } else {
            UIViewController.topMostViewController()?.hideProgress()
            Alert.showAlertWithErrorMessage("Empty post!")
        }
    }
    
    func createPost() {
        self.viewModel.createPost {[weak self] (feedInfo) in
            self?.viewModel.getFeedList(isLoadMore: false, start: 0)
            self?.dismissPostComment()
        }
    }
    
    //MARK: Action
    func likeSelectedAction(feed: FeedInfo?) {
        viewModel
            .changeHasUserLikedForFeed(feed: feed)
            .subscribe(onNext: { [weak self] in
                guard let feed = feed else { return }
                guard let index = self?.viewModel.feeds.value.index(of: feed) else { return }
                self?.feedTableView?.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }).disposed(by: bag)
    }
    
    @IBAction func addPostComment(_ sender: UIBarButtonItem) {
        postCommentView.showShareButton()
        presentPostComment()
    }
}

//MARK: extension uitableviewDelegate
extension FeedViewController: UITableViewDelegate {
    
}

// MARK: - UITableViewDataSource
extension FeedViewController: UITableViewDataSource {
    func showFeedDetail(feed: FeedInfo) {
        let controller = UIStoryboard.instantiate(FeedDetailViewController.self, storyboardType: .feedDetail)
        controller.viewModel.feedInfo.value = feed
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.feeds.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.identifier, for: indexPath)
        let feedinfo = viewModel.feeds.value[indexPath.row]
        if let feedcell = cell as? FeedCell {
            feedcell.delegate = self
            feedcell.config(feedInfo: feedinfo)
            feedcell.largeImageLoadComplete = {
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let feedinfo = viewModel.feeds.value[indexPath.row]
        self.showFeedDetail(feed: feedinfo)
    }
}

extension FeedViewController: FeedCellDelegate {
    func wantToShowProfileOfFeedOwner(feed: FeedInfo?) {
        self.showProfileDetail(userid: 0)
    }
    
    func onLikedTapped(feed: FeedInfo?) {
        self.likeSelectedAction(feed: feed)
    }
}

extension FeedViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            let path = imageURL.lastPathComponent.lowercased()
            if isValidJpg(imageName: path) {
                imageTmp = chosenImage
                heightPopup.constant = Constant.PopupHeight.hideShareButton
                postCommentView.hideShareButton()
                postCommentView.photoImageView.image = chosenImage
                isDismissImagePicker = true
                viewModel.canUploadImage = true
            }else {
                Alert.showAlertWithErrorMessage("validate.image.format".localized())
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isDismissImagePicker = true
        dismiss(animated: true, completion: nil)
    }
    
    func isValidJpg(imageName: String) -> Bool {
        let array = imageName.split(separator: ".")
        if array.count > 0 {
            let mime = array[array.count - 1]
            return (mime == "jpg")
        }
        return false
    }
}

extension FeedViewController: FeedDetailProtocol {
    func likeButtonTapped(feedInfo: FeedInfo?) {
        guard let feed = feedInfo else { return }
        feed.isViewHorizontalLine = true
        guard let index = viewModel.feeds.value.index(of: feed) else { return }
        feedTableView?.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
}
