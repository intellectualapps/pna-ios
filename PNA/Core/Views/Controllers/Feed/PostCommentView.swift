//
//  PostCommentView.swift
//  PNA
//
//  Created by bui.duy.hien on 3/13/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

private struct Constant {
    struct Strings {
        static let placehoderTextView = "Share what's on your mind..."
    }
}

class PostCommentView: UIView {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet private weak var sharePhotoButton: UIButton!
    @IBOutlet private weak var heightContainerView: NSLayoutConstraint!
    @IBOutlet private weak var textView: UITextView!
    
    // MARK: - Local Variables
    var didTappedCancel: (() -> ())?
    var didTappedPost: ((String) -> ())?
    var didTappedSharePhoto: (() -> ())?
    fileprivate var finalText = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func clearTextView() {
        textView.text = Constant.Strings.placehoderTextView
        textView.textColor = .lightGray
        textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("PostCommentView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        textView.text = Constant.Strings.placehoderTextView
        textView.textColor = .lightGray
        textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
    }
    
    func showShareButton() {
        heightContainerView.constant = 34
        sharePhotoButton.isHidden = false
        photoImageView.isHidden = true
        textView.becomeFirstResponder()
    }
    
    func hideShareButton() {
        heightContainerView.constant = 80
        sharePhotoButton.isHidden = true
        photoImageView.isHidden = false
    }
    
    // MARK: - Actions
    @IBAction func cancelAction(_ sender: UIButton) {
        textView.resignFirstResponder()
        didTappedCancel?()
    }
    
    @IBAction func postAction(_ sender: UIButton) {
        textView.resignFirstResponder()
        if textView.textColor == UIColor.lightGray {
            finalText = ""
        } else {
            finalText = textView.text
        }
        didTappedPost?(finalText)
    }
    
    @IBAction func sharePhotoAction(_ sender: UIButton) {
        didTappedSharePhoto?()
    }
}

extension PostCommentView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        if updatedText.isEmpty {
            textView.text = Constant.Strings.placehoderTextView
            textView.textColor = .lightGray
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)

            return false
        }
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
        }

        return true
    }
    
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if let _ = self.window {
            if textView.textColor == UIColor.lightGray {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
}
