//
//  InvitationProfileViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/4/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class InvitationProfileViewController: NetworkInvitationViewController {

    var invitationProfileViewModel: InvitationProfileViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Network Invitation"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_btn"), style: .done, target: self, action: #selector(dismissView))
        invitationProfileViewModel?.getSlimProfile()
        invitationProfileViewModel?.slimProfile.asObservable()
            .subscribe(onNext: { (profile) in
                self.configure(slimProfile: profile)
            }).disposed(by: rx.disposeBag)
    }

    override func acceptedBtnAction(_ sender: Any) {
        invitationProfileViewModel?.updateNetworkInvitation(accepted: true, {
            self.dismissView()
        })
    }
    
    override func declineBtnAction(_ sender: Any) {
        invitationProfileViewModel?.updateNetworkInvitation(accepted: false, {
            self.dismissView()
        })
    }
    
    @objc func dismissView() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func configure(slimProfile: SlimProfile) {
        if let url = URL(string: slimProfile.pic ?? "") {
            self.avatarImageView?.sd_setImage(with: url, completed: nil)
        }
        self.jobtitleLabel?.text = slimProfile.jobTitle
        self.companyLabel?.text = slimProfile.company
        self.namelabel?.text = "From: \(slimProfile.fullName ?? "")"
    }
    
}
