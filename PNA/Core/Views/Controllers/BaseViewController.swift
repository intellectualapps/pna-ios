//
//  BaseViewController.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    let refreshControl = UIRefreshControl()
    
    var viewDidAppeared = false
    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }

    func addLeftButton() {
        addDefaultBackBtn()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addLeftButton()
        setFontForTitle()
        setupUI()
        setupObservable()
        print("VIEW CONTROLLER: " + NSStringFromClass(self.classForCoder) + "." + #function)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setColorForNavigationBar(color: UIColor.black)
    }
    
    override func toggleLeft() {
        self.backAction()
    }
    
    func setFontForTitle() {
//        if let font = UIFont(name: "FiraSans-Regular", size: 25) {
//            let attrs = [ NSFontAttributeName: font]
//            self.navigationController?.navigationBar.titleTextAttributes = attrs
//        }
    }
    
    func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func menuAction() {
        guard let slideMenuController = self.slideMenuController()  else { return }
        slideMenuController.openLeft()
    }
    
    func setNavigationBarItem() {
        if let img = UIImage(named: "menu_icon") {
            self.addLeftBarButtonWithImage(buttonImage: img, tintColor: .white)
            self.navigationItem.leftBarButtonItem?.tintColor = .white
        }
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.addLeftGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
    }
    
    func showHomeScreen(_ animated:Bool = true){
        AppDelegate.shared().setupFeedMenu()
    }

    internal func setupUI() { }
    
    internal func setupObservable() { }
}
