//
//  WebViewVC.swift
//  CourseApp
//
//  Created by Nguyen Van Dung on 1/23/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import WebKit

class WebViewVC: BaseViewController, WKNavigationDelegate {
    var webView : WKWebView!
    var urlString: String?
    var titleStr: String?

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // init and load request in webview.
        webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        self.view.addSubview(webView)
        self.view.sendSubviewToBack(webView)
        addLeftButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadUrl()
        showTitle()
    }

    override func addLeftButton() {
        addMenuButton(action: #selector(leftAction))
    }

    func showTitle() {
        self.title = titleStr
    }

    func loadUrl() {
        if let str = urlString {
            if let url = URL.init(string: str) {
                let request = URLRequest(url: url)
                webView.load(request)
            }
        }
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
    }

    @objc func leftAction() {
        guard let slideMenuController = self.slideMenuController()  else { return }
        slideMenuController.openLeft()
    }
}
