//
//  PNADailyViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/2/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import WebKit

class PNADailyViewController: BaseViewController {

    var webview: WKWebView!
    var viewModel = PNADailyViewModel()
    var isMenuAction = true
    
    override func viewDidLoad() {
        webview = WKWebView(frame: view.frame)
        view.addSubview(webview)
        super.viewDidLoad()
        self.title = "PNA Daily"
        viewModel.getPNADailyContent()
    }
    
    override func setupUI() {
        if isMenuAction {
            removeNavigationBarItem()
            setNavigationBarItem()
        }
    }
    
    override func backAction() {
        if isMenuAction {
            menuAction()
        } else {
            super.backAction()
        }
    }
    
    override func setupObservable() {
        viewModel
            .htmlContent
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (htmlContent) in
                self?.webview.loadHTMLString(htmlContent, baseURL: nil)
        }).disposed(by: rx.disposeBag)
    }

}
