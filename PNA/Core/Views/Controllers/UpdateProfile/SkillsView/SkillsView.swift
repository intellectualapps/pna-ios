//
//  SkillsView.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/25/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class SkillsView: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var skillTextView: UITextView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    //MARK: - Variables
    var viewModel: UpdateExp_SkillsViewModel?
    public var addAction: (() -> Void)?
    public var cancelAction: (() -> Void)?
    
    init() {
        super.init(nibName: "SkillsView", bundle: Bundle.main)
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.roundCorner(radius: 10)
        cancelButton.roundCorner(radius: 10)
    }
    
    override func setupUI() {
        let skillValid: Observable<Bool> = skillTextView.rx.text
            .map{ text -> Bool in
                return text != ""
            }
            .share(replay: 1)
        skillValid
            .bind(to: addButton.rx.isEnabled)
            .disposed(by: rx.disposeBag)
    }
    
    //MARK: - Actions
    @IBAction func onAddButton(_ sender: UIButton) {
        guard let user = AppDelegate.shared().loggedUser else { return }
        let skillsArr = (user.skills ?? "").components(separatedBy: ",")
        if skillsArr.count >= 3 {
            Alert.showAlertWithErrorMessage(NSLocalizedString("skill.limit", comment: ""))
            return
        }
        let skills = skillTextView.text.replacingOccurrences(of: "\n", with: ",")
        dismiss(animated: true) { [weak self] in
            guard let viewModel = self?.viewModel else { return }
            viewModel.addSkill(skills: skills)
        }
    }
    
    @IBAction func onCancelButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
