//
//  ProfileViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 7/18/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class ProfileInUpdateViewController: StickyHeaderViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "editIcon"), style: .plain, target: self, action: #selector(editProfile))
        self.navigationController?.navigationBar.tintColor = .white
        removeNavigationBarItem()
        setNavigationBarItem()
    }
    
    override func backAction() {
        self.menuAction()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.user = AppDelegate.shared().loggedUser
        loadUserInfo()
    }

    @objc func editProfile() {
        let updateProfileVC = UIStoryboard.instantiate(UpdateProfileViewController.self, storyboardType: .updateProfile)
        navigationController?.pushViewController(updateProfileVC, animated: true)
    }

}
