//
//  ExpView.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/25/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import DropDown

class ExpView: BaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var positionTextField: UITextField!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var stateButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var coverButton: UIButton!
    @IBOutlet weak var periodPickerView: PeriodPickerView!
    @IBOutlet weak var pickerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var endDateButton: UIButton!
    
    //MARK: - Variables
    var viewModel: UpdateExp_SkillsViewModel?
    var dropViewModel = DropDownViewModel()
    public var addAction: (() -> ())?
    public var cancelAction: (() -> ())?
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    var disposed         = DisposeBag()
    var countries = [CountryInfo]()
    var states = [StateInfo]()
    var countrynames: [String] {
        return countries.map({$0.name})
    }
    var stateNames: [String] {
        return states.map({$0.name})
    }
    var selectedState: StateInfo?
    var selectedCountry: CountryInfo?
    var currentCountryId  = -1
    var currentStateId    = -1
    var state = Variable(-1)
    var country = Variable(-1)
    var isStartDate: Bool!
    var isOnStartDate = Variable(-1)
    var isOnEndDate = Variable(-1)
    var startMonth: Int = 0
    var endMonth: Int = 0
    var startYear: Int = 0
    var endYear: Int = 0
    
    init() {
        super.init(nibName: "ExpView", bundle: Bundle.main)
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton?.roundCorner(radius: 10)
        cancelButton?.roundCorner(radius: 10)
        setupPickerView()
        state.asObservable().subscribe(onNext: { [weak self] (value) in
            if value != -1 {
                self?.selectedState = self?.states[value]
            }
        }).disposed(by: rx.disposeBag)
        country.asObservable().subscribe(onNext: { [weak self] (value) in
            if value != -1 {
                self?.selectedCountry = self?.countries[value]
            }
        }).disposed(by: rx.disposeBag)
    }
    
    func didReceiveCountries() {
        self.makeCountrySelection()
    }
    
    func didReceiveStates(country: Int) {
        self.makeStateSelection(country: country)
        self.stateButton.isUserInteractionEnabled = true
    }
    
    func makeCountrySelection() {
        self.countryDropDown.anchorView = self.countryButton
        self.countryDropDown.dataSource = countrynames
        self.countryDropDown.bottomOffset = CGPoint(x: 0, y: self.countryButton?.bounds.height ?? 0)
        if self.currentCountryId > 0 {
            let selectedCountryObj: [CountryInfo] = countries.filter {$0.id == self.currentCountryId }
            let currentIdx = countries.index(of: selectedCountryObj[0]) ?? -1
            if (currentIdx >= 0) {
                let title  = countrynames[currentIdx]
                self.countryButton?.setTitle(title, for: .normal)
                self.country.value = currentIdx
                dropViewModel.stateSetup(countryId: currentIdx) { [weak self] (statesInfo) in
                    guard let statesInfo = statesInfo else { return }
                    self?.states = statesInfo
                    self?.didReceiveStates(country: currentIdx)
                }
            }
        }
        
        self.countryDropDown.selectionAction = { (index, item) in
            self.countryButton?.setTitle(item, for: .normal)
            self.country.value = index
            
            let selectedCountryId = self.countries[index].id
            self.stateButton.setTitle("State", for: .normal)
            self.dropViewModel.stateSetup(countryId: selectedCountryId) { [weak self] (statesInfo) in
                guard let statesInfo = statesInfo else { return }
                self?.states = statesInfo
                self?.didReceiveStates(country: selectedCountryId)
            }
        }
    }
    
    func makeStateSelection(country: Int) {
        self.stateDropDown.anchorView = self.stateButton
        self.stateDropDown.dataSource = stateNames
        self.stateDropDown.bottomOffset = CGPoint(x: 0, y: self.stateButton?.bounds.height ?? 0)
        if self.currentCountryId == country {
            let selectedStateObj: [StateInfo] = states.filter {$0.id == self.currentStateId }
            if let first = selectedStateObj.first {
                let currentIdx = states.index(of: first) ?? -1
                if (currentIdx >= 0) {
                    let title  = stateNames[currentIdx]
                    self.stateButton?.setTitle(title, for: .normal)
                    self.state.value = currentIdx
                }
            }
        }
        
        self.stateDropDown.selectionAction = { (index, item) in
            self.stateButton?.setTitle(item, for: .normal)
            self.state.value = index
        }
    }
    
    override func setupUI() {
        let companyValid: Observable<Bool> = companyTextField.rx.text
            .map{ text -> Bool in
                return text != ""
            }
            .share(replay: 1)
        let positionValid: Observable<Bool> = positionTextField.rx.text
            .map{ text -> Bool in
                return text != ""
            }
            .share(replay: 1)
        let countryValid: Observable<Bool> = country.asObservable()
            .map { (value) -> Bool in
                return value != -1
            }
            .share(replay: 1)
        let stateValid: Observable<Bool> = state.asObservable()
            .map { (value) -> Bool in
                return value != -1
            }
            .share(replay: 1)
        let startValid: Observable<Bool> = isOnStartDate.asObservable()
            .map { (value) -> Bool in
                return value != -1
            }
            .share(replay: 1)
        let endValid: Observable<Bool> = isOnEndDate.asObservable()
            .map { (value) -> Bool in
                return value != -1
            }
            .share(replay: 1)
        let everythingValid: Observable<Bool>
            = Observable.combineLatest(companyValid, positionValid, countryValid, stateValid, startValid, endValid) { $0 && $1 && $2 && $3 && $4 && $5}
        everythingValid
            .bind(to: addButton.rx.isEnabled)
            .disposed(by: rx.disposeBag)
        dropViewModel.countrySetup { [weak self] (countriesInfo) in
            guard let countriesInfo = countriesInfo else { return }
            self?.countries = countriesInfo
            self?.didReceiveCountries()
        }
    }
    
    func setupPickerView() {
        startDateButton?.roundCorner(radius: 5)
        endDateButton?.roundCorner(radius: 5)
        periodPickerView?.roundCorner(radius: 10)
        periodPickerView?.doneClosure = {[weak self] (monthNum, month, year) in
            guard let `self` = self else { return }
            let title = "\(month). \(year)"
            if self.isStartDate {
                self.startDateButton.setTitle(title, for: .normal)
                self.isOnStartDate.value = 0
                self.startMonth = monthNum
                self.startYear = year
            } else {
                self.endDateButton.setTitle(title, for: .normal)
                self.isOnEndDate.value = 0
                self.endMonth = monthNum
                self.endYear = year
            }
            self.dismissPicker()
        }
        periodPickerView?.cancelClosure = { [weak self] in
            self?.dismissPicker()
        }
    }
    
    private func presentPicker() {
        companyTextField.resignFirstResponder()
        positionTextField.resignFirstResponder()
        UIView.animate(withDuration: 0.33, animations: {
            self.pickerBottomConstraint.constant = 0
            self.coverButton.alpha = 1
            self.view.layoutIfNeeded()
        })
    }
    
    private func dismissPicker() {
        UIView.animate(withDuration: 0.33, animations: {
            self.pickerBottomConstraint.constant = self.periodPickerView.bounds.height
            self.coverButton.alpha = 0
            self.view.layoutIfNeeded()
        })
    }
    
    //MARK: - Actions
    @IBAction func onSelectCountry(_ sender: UIButton) {
        countryDropDown.show()
    }
    
    @IBAction func onSelectState(_ sender: UIButton) {
        stateDropDown.show()
    }
    
    @IBAction func onAddButton(_ sender: UIButton) {
        if endYear < startYear || (endYear == startYear && endMonth < startMonth) {
            Alert.showAlertWithErrorMessage(NSLocalizedString("date.notValid", comment: ""))
            return
        }
        if let viewModel = viewModel, viewModel.experience.value.count >= 3 {
            Alert.showAlertWithErrorMessage(NSLocalizedString("exp.limit", comment: ""))
            return
        }
        dismiss(animated: true) { [weak self] in
            let company = self?.companyTextField?.text ?? ""
            let position = self?.positionTextField?.text ?? ""
            let country = self?.selectedCountry?.name ?? ""
            let state = self?.selectedState?.name ?? ""
            let startDate = self?.startDateButton.titleLabel?.text ?? ""
            let endDate = self?.endDateButton.titleLabel?.text ?? ""
            guard let viewModel = self?.viewModel else { return }
            viewModel.addExp(position: position,
                                    company: company,
                                    state: state,
                                    country: country,
                                    startDate: startDate,
                                    endDate: endDate)
        }
    }
    
    @IBAction func onCancelButton(_ sender: UIButton) {
        dismiss(animated: true) {
            self.cancelAction?()
        }
    }
    
    @IBAction func onStartDate(_ sender: UIButton) {
        self.presentPicker()
        self.isStartDate = true
    }
    
    @IBAction func onEndDate(_ sender: UIButton) {
        self.presentPicker()
        self.isStartDate = false
    }
    
    @IBAction func onCoverButton(_ sender: UIButton) {
        self.dismissPicker()
    }
}
