//
//  UpdateExp_SkillsTableViewCell.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/2/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class UpdateExp_SkillsTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var twitterTextField: UITextField!
    @IBOutlet weak var facebookTextField: UITextField!
    @IBOutlet weak var instagramTextField: UITextField!
    @IBOutlet weak var websiteTextField: UITextField!
    @IBOutlet weak var expTextView: UITextView!
    @IBOutlet weak var skillsTextView: UITextView!
    
    //MARK: - Variables
    var delegate: UpdateExp_SkillsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        expTextView.attributedText = nil
    }
    
    func config(with viewModel: UpdateExp_SkillsViewModel) {
        twitterTextField?.text = viewModel.twitter
        facebookTextField?.text = viewModel.facebook
        instagramTextField?.text = viewModel.instagram
        websiteTextField?.text = viewModel.website
        expTextView.attributedText = self.attributedTextForTextView(with: viewModel)
        skillsTextView?.text = viewModel.skills.value
    }
    
    func attributedTextForTextView(with viewModel: UpdateExp_SkillsViewModel) -> NSMutableAttributedString {
        let string = NSMutableAttributedString()
        for expDict in viewModel.experience.value {
            let mainAttribute = [NSAttributedString.Key.font : UIFont(name: "Fira Sans", size: 20)!, NSAttributedString.Key.foregroundColor : UIColor(hex: "5E5E5E")]
            let mainString = NSMutableAttributedString(string: expDict["main"] as? String ?? "", attributes: mainAttribute)
            string.append(mainString)
            let subAttribute = [NSAttributedString.Key.font : UIFont(name: "Fira Sans", size: 17)!, NSAttributedString.Key.foregroundColor : UIColor(hex: "5E5E5E")]
            let subString = NSMutableAttributedString(string: "\n\(expDict["sub"] as? String ?? "")\n\n", attributes: subAttribute )
            string.append(subString)
        }
        return string
    }
    
    //MARK: - Actions
    @IBAction func onAddExp(_ sender: UIButton) {
        delegate?.onAddExp()
    }
    
    @IBAction func onAddSkill(_ sender: UIButton) {
        delegate?.onAddSkill()
    }
    
    @IBAction func onUpdate(_ sender: UIButton) {
        let insta = instagramTextField.text ?? ""
        let twitter = twitterTextField.text ?? ""
        let fb = facebookTextField.text ?? ""
        let web = websiteTextField.text ?? ""
        delegate?.onUpdate(insta: insta, twitter: twitter, fb: fb, web: web)
    }
}

protocol UpdateExp_SkillsTableViewCellDelegate {
    func onAddExp()
    func onAddSkill()
    func onUpdate(insta: String, twitter: String, fb: String, web: String)
}
