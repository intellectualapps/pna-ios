//
//  UpdateEx_SkillsViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/25/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class UpdateExp_SkillsViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var viewModel = UpdateExp_SkillsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Update Experience/Skills"
        UIViewController.topMostViewController()?.showProgress()
        viewModel.updateLoggedUser { [weak self] (result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let user = result as? User {
                self?.viewModel.fillFromUser(user: user)
                self?.tableView.reloadData()
            }
        }
    }
    
    override func setupUI() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 735
        viewModel.experience.asObservable()
            .subscribe(onNext: { [weak self](exp) in
                self?.tableView.reloadData()
            }).disposed(by: rx.disposeBag)
        viewModel.skills.asObservable()
            .subscribe(onNext: { [weak self](skills) in
                self?.tableView.reloadData()
            }).disposed(by: rx.disposeBag)
    }
}

extension UpdateExp_SkillsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UpdateExp_SkillsTableViewCell", for: indexPath) as! UpdateExp_SkillsTableViewCell
        cell.config(with: viewModel)
        cell.delegate = self
        return cell
    }
}

extension UpdateExp_SkillsViewController: UpdateExp_SkillsTableViewCellDelegate {
    func onAddExp() {
        let expView = ExpView()
        expView.viewModel = self.viewModel
        present(expView, animated: true, completion: nil)
    }
    
    func onAddSkill() {
        let skillView = SkillsView()
        skillView.viewModel = self.viewModel
        present(skillView, animated: true, completion: nil)
    }
    
    func onUpdate(insta: String, twitter: String, fb: String, web: String) {
        viewModel.updateSocialNetwork(insta: insta, twitter: twitter, fb: fb, web: web) { [weak self] in
            self?.tableView?.reloadData()
        }
    }
}
