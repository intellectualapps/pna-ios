//
//  UpdateProfileViewController.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/21/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit
import DropDown

class UpdateProfileViewController: BaseViewController {

    var viewModel = UpdateProfileViewModel()
    //MARK: UI elements
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var phonenumberTextField: UITextField!
    @IBOutlet weak var jobTitleTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var industryButton: UIButton!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var stateButton: UIButton!
    @IBOutlet weak var updateProfileButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var changePhotoButton: UIButton!
    
    //MARK: Local variables
    let industryDropDown = DropDown()
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    var disposed         = DisposeBag()
    var imageTmp: UIImage?
    var cloudinary = CloudinaryManage()
    
    //MARK: Constant variables
    fileprivate let ADD_TITILE_BUTTON = "Add Profile Photo"
    fileprivate let CHANGE_TITLE_BUTTON = "Change Profile Photo"
    fileprivate let TITLE = "Edit Profile"
    
    //MARK: UI view
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = TITLE

        if let user = AppDelegate.shared().loggedUser {
            if user.id == viewModel.id {
                didReceiveUserInfo(user: user)
            } else {
                viewModel.updateLoggedUser { [weak self] (result, error) in
                    if let user = result as? User {
                        DispatchQueue.main.async {
                            self?.didReceiveUserInfo(user: user)
                        }
                    }
                }
            }
        }
        setupView()
    }

    func didReceiveUserInfo(user: User) {
        viewModel.fillFromUser(user: user)
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    //MARK: Setup view
    
    func industrySetup() {
        let path = Path.industryLookup.path
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        self.showProgress()
        service.doExecute({[weak self] (result, error) in
            self?.hideProgress()
            
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let industryResult = result as? [String: Any] {
                    if let indDicts = industryResult["industries"] as? [[String: Any]] {
                        var temp = [IndustryInfo]()
                        for dict in indDicts {
                            let info = IndustryInfo(dict: dict)
                            temp.append(info)
                        }
                        self?.viewModel.industies.value = temp
                        self?.didReceiveIndustries()
                    }
                }
            }
            
        })
    }
    
    func didReceiveIndustries() {
        self.makeIndustrySelection()
    }
    
    func countrySetup() {
        let path = Path.countryLookup.path
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        
        service.doExecute({[weak self] (result, error) in
            self?.hideProgress()
            
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let countryResult = result as? [String: Any] {
                    if let cntyDict = countryResult["countries"] as? [[String: Any]] {
                        var temp = [CountryInfo]()
                        for dict in cntyDict {
                            let info = CountryInfo(dict: dict)
                            temp.append(info)
                        }
                        self?.viewModel.countries.value = temp
                        
                        self?.didReceiveCountries()
                    }
                }
            }
            
        })
    }
    
    func didReceiveCountries() {
        self.makeCountrySelection()
    }
    
    func stateSetup(countryId: Int) {
        let path = Path.stateLookup.path + "/\(countryId)"
        let params = RequestParams()
        let service = ApiService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        
        service.doExecute({[weak self] (result, error) in
            self?.hideProgress()
            
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                if let stateResult = result as? [String: Any] {
                    if let stateDict = stateResult["states"] as? [[String: Any]] {
                        var temp = [StateInfo]()
                        for dict in stateDict {
                            let info = StateInfo(dict: dict)
                            temp.append(info)
                        }
                        self?.viewModel.states.value = temp
                        self?.didReceiveStates(country: countryId)
                    }
                    
                }
            }
            
        })
    }
    func didReceiveStates(country: Int) {
        self.makeStateSelection(country: country)
        self.stateButton.isUserInteractionEnabled = true
    }
    
    func makeIndustrySelection() {
        self.industryDropDown.anchorView = self.industryButton
        self.industryDropDown.dataSource = viewModel.indestriesNames
        self.industryDropDown.bottomOffset = CGPoint(x: 0, y: self.industryButton?.bounds.height ?? 0)
        
        if self.viewModel.currentIndustryId > 0 {
            let selectedIndustryObj: [IndustryInfo] = viewModel.industies.value.filter {$0.id == self.viewModel.currentIndustryId }
            let currentIdx = viewModel.industies.value.index(of: selectedIndustryObj[0]) ?? -1
            if (currentIdx >= 0) {
                let title  = viewModel.indestriesNames[currentIdx]
                self.industryButton?.setTitle(title, for: .normal)
                self.industryButton?.setTitleColor(.black, for: .normal)
                self.viewModel.industry = currentIdx
            }
        }

        self.industryDropDown.selectionAction = {[weak self] (index, item) in
            self?.industryButton?.setTitle(item, for: .normal)
            self?.industryButton?.setTitleColor(.black, for: .normal)
            self?.viewModel.industry = index
        }
    }
    func makeCountrySelection() {
        self.countryDropDown.anchorView = self.countryButton
        self.countryDropDown.dataSource = viewModel.countrynames
        self.countryDropDown.bottomOffset = CGPoint(x: 0, y: self.countryButton?.bounds.height ?? 0)
        if self.viewModel.currentCountryId > 0 {
            let selectedCountryObj: [CountryInfo] = viewModel.countries.value.filter {$0.id == self.viewModel.currentCountryId }
            let currentIdx = viewModel.countries.value.index(of: selectedCountryObj[0]) ?? -1
            if (currentIdx >= 0) {
                let title  = viewModel.countrynames[currentIdx]
                self.countryButton?.setTitle(title, for: .normal)
                self.countryButton?.setTitleColor(.black, for: .normal)
                self.viewModel.country = currentIdx
                self.stateSetup(countryId: self.viewModel.currentCountryId)
            }
        }
        
        self.countryDropDown.selectionAction = { [weak self] (index, item) in
            self?.countryButton?.setTitle(item, for: .normal)
            self?.viewModel.country = index
            self?.countryButton?.setTitleColor(.black, for: .normal)
            self?.stateButton.setTitle("State", for: .normal)
            if let selectedCountryId = self?.viewModel.countries.value[index].id {
                self?.stateSetup(countryId: selectedCountryId)
            }
        }
    }
    
    func makeStateSelection(country: Int) {
        self.stateDropDown.anchorView = self.stateButton
        self.stateDropDown.dataSource = viewModel.stateNames
        self.stateDropDown.bottomOffset = CGPoint(x: 0, y: self.stateButton?.bounds.height ?? 0)
        if self.viewModel.currentCountryId == country {
            let selectedStateObj: [StateInfo] = viewModel.states.value.filter {$0.id == self.viewModel.currentStateId }
            if let first = selectedStateObj.first {
                let currentIdx = viewModel.states.value.index(of: first) ?? -1
                if (currentIdx >= 0) {
                    let title  = viewModel.stateNames[currentIdx]
                    self.stateButton?.setTitle(title, for: .normal)
                    self.stateButton?.setTitleColor(.black, for: .normal)
                    self.viewModel.state = currentIdx
                }
            }
        }
        
        self.stateDropDown.selectionAction = { [weak self] (index, item) in
            self?.stateButton?.setTitle(item, for: .normal)
            self?.stateButton?.setTitleColor(.black, for: .normal)
            self?.viewModel.state = index
        }
    }
    
    func setupView() {
        firstnameTextField?.text = self.viewModel.firstname
        lastnameTextField?.text = self.viewModel.lastname
        phonenumberTextField?.text = self.viewModel.phoneNumber
        jobTitleTextField?.text = self.viewModel.jobTitle
        companyTextField?.text = self.viewModel.company
        if let url = URL(string: viewModel.imageURL) {
            avatarImageView?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "avatar_default"), options: .cacheMemoryOnly, completed: nil)
        } else {
            changePhotoButton?.setTitle(self.CHANGE_TITLE_BUTTON, for: .normal)
        }
        self.stateButton?.isUserInteractionEnabled = false;
        
        firstnameTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.firstname = value
                }
            })
            .disposed(by: disposed)
        
        lastnameTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.lastname = value
                }
            })
            .disposed(by: disposed)
        
        phonenumberTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.phoneNumber = value
                }
            })
            .disposed(by: disposed)
        
        jobTitleTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.jobTitle = value
                }
            })
            .disposed(by: disposed)
        
        companyTextField?.rx.value.asObservable()
            .throttle(0.6, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] (text) in
                if let value = text {
                    self?.viewModel.company = value
                }
            })
            .disposed(by: disposed)
        
        industrySetup()
        countrySetup()
    }
    
    //MARK: Action
    @IBAction func continueAction(_ sender: Any) {
        UIViewController.topMostViewController()?.showProgress()
            if self.viewModel.isChangeAvatar {
                if let username = AppDelegate.shared().loggedUser?.fullName {
                    self.cloudinary.uploadAvatar(img: self.imageTmp, username: username, progress: nil, completion: { [weak self] (result, error) in
                        UIViewController.topMostViewController()?.hideProgress()
                        if let error = error {
                            Alert.showAlertWithErrorMessage(error.localizedDescription)
                        } else {
                            if let result = result, let publicId = result.publicId, let version = result.version {
                                let id = "v\(version)/\(publicId)"
                                self?.viewModel.imageURL = self?.cloudinary.getAvatarUrl(public_id: id) ?? ""
                                self?.updateProfile()
                            }
                        }
                    })
                } else {
                    UIViewController.topMostViewController()?.hideProgress()
                }
            } else {
                UIViewController.topMostViewController()?.hideProgress()
                self.updateProfile()
            }
    }
    
    func updateProfile() {
        UIViewController.topMostViewController()?.showProgress()
        self.viewModel.updateProfie (completion: {[weak self] (result, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            }else{
                Alert.showAlertWithErrorMessage("Profile updated")
                if let result = result as? ApiResponseBasicModel {
                    if let user = result.content as? User {
                        self?.viewModel.fillFromUser(user: user)
                        self?.setupView()
                    }
                }
            }
        })
    }
    
    @IBAction func industryAction(_ sender: Any) {
        industryDropDown.show()
    }
    
    @IBAction func countryAction(_ sender: Any) {
        countryDropDown.show()
    }
    
    @IBAction func stateAction(_ sender: Any) {
        stateDropDown.show()
    }
    
    @IBAction func onChangePhoto(_ sender: UITapGestureRecognizer) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
}

extension UpdateProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            let path = imageURL.lastPathComponent.lowercased()
            if isValidJpg(imageName: path) {
                imageTmp = chosenImage
                avatarImageView?.image = chosenImage
                changePhotoButton?.setTitle(self.CHANGE_TITLE_BUTTON, for: .normal)
                viewModel.isChangeAvatar = true
            }else {
                Alert.showAlertWithErrorMessage("validate.image.format".localized())
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func isValidJpg(imageName: String) -> Bool {
        let array = imageName.split(separator: ".")
        if array.count > 0 {
            let mime = array[array.count - 1]
            return (mime == "jpg")
        }
        return false
    }
}
