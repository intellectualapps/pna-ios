//
//  CreateCommentsDialogView.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class CreateCommentsDialogView: UIView {
    var postid = 0
    var action: DialogCompletion?
    var parentController: UIViewController?
    
    @IBOutlet weak var textView: UITextView!

    @IBOutlet weak var cancelBtn: UIButton!

    @IBOutlet weak var postBtn: UIButton!

    class func instance() -> CreateCommentsDialogView {
        return CreateCommentsDialogView.fromNib(nibNameOrNil: "CreateCommentsDialogView")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.textView?.becomeFirstResponder()
    }

    @IBAction func cancelAction(_ sender: Any) {
        self.parentController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func postAction(_ sender: Any) {
        let comments = (textView?.text ?? "").trim()
        if comments.isEmpty {
            Alert.showAlertWithErrorMessage("Please write your comments.")
            return
        }
        let email = AppDelegate.shared().loggedUser?.email ?? ""
        UIViewController.topMostViewController()?.showProgress()
        PostCommentService.postComment(comments: comments, email: email, postid: postid) {[weak self] (response, error) in
            UIViewController.topMostViewController()?.hideProgress()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                self?.textView?.text = nil
                self?.action?()
                self?.parentController?.dismiss(animated: true, completion: nil)
            }
        }
    }
}
