//
//  AvatarImageView.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class AvatarImageView: BaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - Variables
    var imageUrl = ""
    
    init() {
        super.init(nibName: "AvatarImageView", bundle: Bundle.main)
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: imageUrl) {
            imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "avatar_default"), options: .cacheMemoryOnly, completed: nil)
        }
    }
    
    //MARK: - Actions
    @IBAction func onCoverButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
