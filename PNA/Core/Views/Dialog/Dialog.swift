//
//  Diglog.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import PopupDialog
import IQKeyboardManagerSwift

class AlertViewController: TYAlertController {
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setBlurEffectWith(self.view)
        IQKeyboardManager.shared.enable = false
    }
}

typealias DialogCompletion = () -> ()
class Dialog {
    class func showDialog(inController: UIViewController, postid: Int, action: @escaping DialogCompletion) {
        DispatchQueue.main.async {
            let view = CreateCommentsDialogView.instance()
            view.action = action
            view.parentController = inController
            view.postid = postid
            let controller = AlertViewController(alert: view, preferredStyle: .alert)
            controller?.alertStyleEdging = 0
            inController.present(controller!, animated: true, completion: nil)
        }
    }
}
