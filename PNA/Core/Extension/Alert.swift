//
//  Alert.swift
//  PNA
//
//  Created by nguyen.van.dungb on 2/5/18.
//  Copyright © 2018 Dht. All rights reserved.
//


import Foundation
import UIKit
/**
 * This is Alert helper class. Will display alert view on screen and handle action callback
 */
class Alert: NSObject {
    
    /*
     Will show alert if have error infor
     */
    class func showAlertIfNeed(_ msgErr: String?, interactObject: AnyObject?, completion:(()->())? = nil) {
        if let err = msgErr, err.lenght > 0 {
            Alert.showErrorMessageAlert(err, interactObject: interactObject, completion: completion)
        }
    }
    
    class func showAlertWithErrorMessage(_ errMsg: String, title: String? = nil, completion: (()->())? = nil) {
        Alert.showAlertWithErrorMessage(errMsg, title: title, cancelBtnTitle: nil, completion: completion)
    }
    
    class func showAlertWithErrorMessage(_ errMsg: String, title: String? = nil, cancelBtnTitle: String?, completion: (()->())? = nil) {
        DispatchQueue.main.async {
            if UIAlertController.isShowingAlertController(title: title ?? "", message: errMsg) {
                return
            }
            var cancelTitle = cancelBtnTitle
            if (cancelTitle == nil) {
                cancelTitle = "button.close".localized()
            }
            let alertController = UIAlertController(title: title ?? "", message: errMsg, preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                completion?()
            })
            alertController.addAction(action)
            let rootController = UIViewController.topMostViewController()
            rootController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    /**
     * This will show alert view with 2 buttons (Ok, Cancel). It is help full incase need confirm
     */
    class func showConfirmAlert(_ title: String = "", message: String, cancelBtnTitle: String, okBtnTitle: String, presentController: UIViewController? = nil, completion: @escaping (Bool)->()) {
        DispatchQueue.main.async {
            if UIAlertController.isShowingAlertController(title: title, message: message) {
                return
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: cancelBtnTitle, style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                completion(false)
            })
            alertController.addAction(action)
            
            let okAction = UIAlertAction(title: okBtnTitle, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                completion(true)
            })
            alertController.addAction(okAction)
            let rootController = (presentController != nil) ? presentController : UIViewController.topMostViewController()
            rootController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    class func showErrorMessageAlert(_ message: String, interactObject: AnyObject?, completion:(()->())? = nil) {
        if UIAlertController.isShowingAlertController(title: "", message: message) {
            return
        }
        let alertVC = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alertVC.addAction(UIAlertAction(title: NSLocalizedString("button.close", comment: ""), style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            completion?()
        }))
        let rootController = UIViewController.topMostViewController()
        rootController?.present(alertVC, animated: true, completion: nil)
    }
}
