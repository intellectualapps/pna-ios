//
//  UIViewExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

let navigationHeaderViewDefaultHeight: CGFloat = DeviceTarget.IS_IPHONE_X ? 145.0 : 64.0

extension UIView {
    class func fromNib<T: UIView>(nibNameOrNil: String? = nil) -> T {
        let v: T? = fromNib(nibNameOrNil: nibNameOrNil)
        return v!
    }

    //Initialize instance from nib file
    class func fromNib<T: UIView>(nibNameOrNil: String? = nil) -> T? {
        var view: T?
        var name = ""
        if let nibName = nibNameOrNil {
            name = nibName
        } else {
            name = "\(T.self)".components(separatedBy: ".").last ?? ""
        }
        if let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil) {
            for v in nibViews {
                if let tog = v as? T {
                    view = tog
                }
            }
        }
        return view
    }

    func roundCorner(radius: CGFloat, borderColor: UIColor = UIColor.white, borderWidth: CGFloat = 1) {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func constraintFor(attribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint? {
        var listConstraint = [NSLayoutConstraint]()
        if attribute == .height || attribute == .width {
            listConstraint = self.constraints
        } else {
            if let constraints = self.superview?.constraints {
                listConstraint = constraints
            }
        }
        for constraint in listConstraint {
            if constraint.firstAttribute == attribute {
                return constraint
            }
        }
        return nil
    }

    public func removeAllSubviews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
    
    func removeRoundBorderColor() {
        for view in self.subviews {
            let radius = view.layer.cornerRadius
            if radius > 0 {
                view.layer.borderColor = UIColor.clear.cgColor
            } else {
                view.removeRoundBorderColor()
            }
        }
    }
    
    
    /* Calculate min of (width, height) =>> cornerRadius = min / 2
     * Sample: UIView(x: 0, y: 0, width: 100, height: 80) -> min(100, 80) = 80
     *         -> cornerRadius = 80 / 2 = 40
     *         UIView(x: 0, y: 0, width: 60, height: 80) -> min(60, 80) = 60
     *         -> cornerRadius = 60 / 2 = 30
     */
    func roundView() {
        let minSize = min(bounds.width, bounds.height)
        layer.cornerRadius = minSize / 2
        layer.masksToBounds = true
    }
}
