//
//  UIViewController+Ext.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import APESuperHUD

private struct Constant {
    struct HudView {
        static let rect = CGRect(x: UIScreen.main.bounds.width / 2 - 50,
                                 y: UIScreen.main.bounds.height / 2 - 60,
                                 width: 100,
                                 height: 120)
        static let gifName = "pna_africa_map"
    }
}

extension UIViewController {
    func showProgress() {
//        if let keywindow = UIApplication.shared.keyWindow {
//            keywindow.isUserInteractionEnabled = false
//            let image = UIImage.init(named: "pna_africa_map")
//            let hud = CustomHUD(frame: Constant.HudView.rect)
//            hud.alpha = 0
//            hud.imageView.image = image
//            keywindow.addSubview(hud)
//            UIView.animate(withDuration: 0.1, animations: {
//                hud.alpha = 1
//                keywindow.layoutIfNeeded()
//            })
//        }
    }
    
    func hideProgress() {
        if let keywindow = UIApplication.shared.keyWindow {
            keywindow.isUserInteractionEnabled = true
            for subview in keywindow.subviews {
                if let subview = subview as? CustomHUD {
                    UIView.animate(withDuration: 0.5, animations: {
                        subview.alpha = 0
                        keywindow.layoutIfNeeded()
                    }, completion: { _ in
                        subview.removeFromSuperview()
                    })
                }
            }
        }
    }

    func addMenuButton(action: Selector? = nil) {
        if let img = UIImage.init(named: "menu_icon") {
            self.addLeftBarButtonWithImage(buttonImage: img, tintColor: .white, action: action)
        }
    }

    func addDefaultBackBtn(action: Selector? = nil) {
        if let img = UIImage(named: "back_btn") {
            self.addLeftBarButtonWithImage(buttonImage: img, tintColor: .white, action: action)
        }
    }

    class func isShowingAlertController(title: String, message: String) -> Bool {
        guard let rootController = UIApplication.shared.keyWindow?.rootViewController else {
            return false
        }
        if let presentedController = UIViewController.topViewControllerWithRootController(rootController) as? UIAlertController {
            if presentedController.title == title && presentedController.message == message {
                return true
            }
        }
        return false
    }

    public func addRightBarButtonWithTitle(title: String, action: Selector) {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        let leftBtn = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = leftBtn
    }

    public func addRightBarButtonWithImage(buttonImage: UIImage, tintColor: UIColor? = nil, btnSize: CGSize = .zero) {
        var size = btnSize
        if size.width == 0 {
            size.width = 20
            size.height = 20
        }
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(buttonImage, for: .normal)
        let iOSVersion = Int(UIDevice.current.systemVersion) ?? 0
        if iOSVersion < 11 {
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        button.addTarget(self, action: #selector(self.toggleRight), for: UIControlEvents.touchUpInside)
        button.tintColor = tintColor
        let leftBtn = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = leftBtn
    }
    
    func setColorForNavigationBar(color: UIColor, font: UIFont = UIFont.navigationFont(size: 22)) {
        let img = UIImage.from(color: color)
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font: font]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }

    public func addLeftBarButtonWithImage(buttonImage: UIImage, tintColor: UIColor? = nil, btnSize: CGSize = .zero, action: Selector? = nil) {
        var size = btnSize
        if size.width == 0 {
            size.width = 20
            size.height = 20
        }
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(buttonImage, for: .normal)
        let iOSVersion = Int(UIDevice.current.systemVersion) ?? 0
        if iOSVersion < 11 {
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        if let action = action {
            button.addTarget(self, action: action, for: UIControlEvents.touchUpInside)
        } else {
            button.addTarget(self, action: #selector(self.toggleLeft), for: UIControlEvents.touchUpInside)
        }

        button.tintColor = tintColor
        let leftBtn = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = leftBtn
    }
    public class func topMostViewController() -> UIViewController? {
        let keyWindow = UIApplication.shared.keyWindow
        return UIViewController.topViewControllerWithRootController(keyWindow?.rootViewController)
    }

    public class func topViewControllerWithRootController(_ root: UIViewController?) -> UIViewController? {
        guard let rootController = root else {
            return nil
        }
        if let tabbarController = rootController as? UITabBarController {
            return UIViewController.topViewControllerWithRootController(tabbarController.selectedViewController)
        } else if let naviagationController = rootController as? UINavigationController {
            return UIViewController.topViewControllerWithRootController(naviagationController.visibleViewController)
        } else if let presentedController = rootController.presentedViewController {
            return UIViewController.topViewControllerWithRootController(presentedController)
        } else {
            for view in rootController.view.subviews {
                if let controller = view.next as? UIViewController {
                    return UIViewController.topViewControllerWithRootController(controller)
                }
            }
        }
        return root
    }
    @objc func toggleLeft() {

    }

    @objc func toggleRight() {

    }

    func showProfileDetail(userid: Int) {
        let controller = UIStoryboard.instantiate(StickyHeaderViewController.self, storyboardType: .profileDetail)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
