//
//  UIStoryboard.swift
//  PNA
//
//  Created by MTQ on 4/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

extension UIStoryboard {
    // swiftlint:disable force_cast
    class func instantiate<T: UIViewController>(_: T.Type, storyboardType: StoryboardType) -> T where T: Any {
        let storyboard = UIStoryboard(name: storyboardType.name, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: T.className) as! T
    }
}
