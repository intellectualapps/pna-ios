//
//  UICollectionView+Ext.swift
//  PNA
//
//  Created by Quang Ly Hoang on 7/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

extension UICollectionView {
    func registerCell<T: UICollectionViewCell>(_ type: T.Type) {
        let className = type.className
        let nib = UINib(nibName: className, bundle: nil)
        register(nib, forCellWithReuseIdentifier: className)
    }
}
