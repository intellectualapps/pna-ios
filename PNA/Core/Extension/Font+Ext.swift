//
//  Font+Ext.swift
//  PNA
//
//  Created by nguyen.van.dungb on 3/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

extension UIFont {
    static func navigationFont(size: CGFloat) -> UIFont {
        return UIFont(name: "FiraSans-Regular", size: size) ?? UIFont.boldSystemFont(ofSize: size)
    }

    static func fireSansRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "FiraSans-Regular", size: size) ?? UIFont.boldSystemFont(ofSize: size)
    }
    
    static func fireSansBold(size: CGFloat) -> UIFont {
        return UIFont(name: "FiraSans-Bold", size: size) ?? UIFont.boldSystemFont(ofSize: size)
    }
}
