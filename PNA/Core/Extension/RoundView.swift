//
//  SearchBarContainerView.swift
//  Craigslist_iOS
//
//  Created by Nguyen Van Dung on 8/23/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class RoundView: UIView {

    @IBInspectable internal var radius: CGFloat = 0 {
        didSet {
            self.round()
        }
    }

    @IBInspectable internal var bdolor: UIColor = UIColor.clear {
        didSet {
            self.round()
        }
    }

    @IBInspectable internal var bdWidth: CGFloat = 0 {
        didSet {
            self.round()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func round() {
        self.roundCorner(radius: self.radius, borderColor: self.bdolor, borderWidth: self.bdWidth)
    }
}
