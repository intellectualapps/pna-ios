//
//  NSObject.swift
//  PNA
//
//  Created by MTQ on 4/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

extension NSObject {
    class var className: String {
        get {
            return NSStringFromClass(self).components(separatedBy: ".").last!
        }
    }
    
    var className: String {
        get {
            return String(describing: type(of: self))
        }
    }
}
