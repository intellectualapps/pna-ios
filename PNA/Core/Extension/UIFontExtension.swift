//
//  UIFontExtension.swift
//  MobileWarehouse
//
//  Created by Nguyen Van Dung on 5/4/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import QuartzCore
import CoreText
import CoreGraphics

enum LatoFontType: Int {
    case regular = 0
    case black
    case blackItalic
    case bold
    case boldItalic
    case hairline
    case hairlineItalic
    case italic
    case light
    case lightItalic
}

extension UIFont {

    class func latoFont(size: CGFloat, type: LatoFontType) -> UIFont {
        var font = UIFont.systemFont(ofSize: size)
        switch type {
        case .regular:
            if let f = UIFont(name: "Lato-Regular", size: size) {
                font = f
            }
        case .black:
            if let f = UIFont(name: "Lato-Black", size: size) {
                font = f
            }
        case .blackItalic:
            if let f = UIFont(name: "Lato-BlackItalic", size: size) {
                font = f
            }
        case .bold:
            if let f = UIFont(name: "Lato-Bold", size: size) {
                font = f
            }
        case .boldItalic:
            if let f = UIFont(name: "Lato-BoldItalic", size: size) {
                font = f
            }
        case .hairline:
            if let f = UIFont(name: "Lato-Hairline", size: size) {
                font = f
            }
        case .hairlineItalic:
            if let f = UIFont(name: "Lato-HairlineItalic", size: size) {
                font = f
            }
        case .italic:
            if let f = UIFont(name: "Lato-Italic", size: size) {
                font = f
            }
        case .light:
            if let f = UIFont(name: "Lato-Light", size: size) {
                font = f
            }
        case .lightItalic:
            if let f = UIFont(name: "Lato-LightItalic", size: size) {
                font = f
            }
        }
        return font
    }


    class func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            let names = UIFont.fontNames(forFamilyName: familyName )
        }
    }
    
    class func loadCustomFont() {
        var paths = [String]()
        let fontFileNames = ["Play-Bold.ttf",
                             "Play-Regular.ttf",
                             "OxygenMono-Regular.otf",
                             "Lato-Regular",
                             "Lato-Black",
                             "Lato-BlackItalic",
                             "Lato-Bold",
                             "Lato-BoldItalic",
                             "Lato-Hairline",
                             "Lato-HairlineItalic",
                             "Lato-Italic",
                             "Lato-Light",
                             "Lato-LightItalic"]
        for name in fontFileNames {
            let filename = (name as NSString).deletingPathExtension
            let fExtension = (name as NSString).pathExtension
            if let path = Bundle.main.path(forResource: filename, ofType: fExtension) {
                paths.append(path)
            }
        }
        loadCustomFont(filePaths: paths)
    }

    class func loadCustomFont(filePaths: [String]) {
        for path in filePaths {
            if FileManager.default.fileExists(atPath: path) {
                let url = URL(fileURLWithPath: path)
                do {
                    let data = try Data(contentsOf: url)
                    if let provider = CGDataProvider(data: data as CFData) {
                        let font = CGFont(provider)
                        CTFontManagerRegisterGraphicsFont(font!, nil)
                    }
                } catch {

                }
            }
        }
    }
}
