//
//  Date+Ext.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
let default_date_format = "yyyy/MM/dd"

extension Date {
    func stringWithUnixTime() -> String {
        var resultString = ""
        var delta = abs(Date().timeIntervalSince1970 - self.timeIntervalSince1970)
        if delta < 60 {
            delta = 60
        }
        let oneMinue: Double = 60
        let oneHour = Double(60 * 60)
        let oneweek =  7 * 48 * oneHour
        if delta < oneMinue {
            resultString = String(Int(delta / 60)) + NSLocalizedString("just_now", comment: "")
        } else if delta < oneHour {
            resultString = String(Int(delta / 60)) + NSLocalizedString("minutes_before", comment: "")
        } else if delta < 24 * oneHour {
            resultString = String(Int(delta / 3600)) + NSLocalizedString("hours_before", comment: "")
        } else if  delta < oneweek {
            resultString =  String(Int(delta / 86400)) + NSLocalizedString("days_before", comment: "")
        } else if delta == oneweek {
            resultString =  "1w"
        } else {
            resultString =  self.dateStringWithFormat(default_date_format)
        }

        return resultString
    }

    func dateStringWithFormat(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle =  DateFormatter.Style.full
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    /// - threeLetters: 3 letter day abbreviation of day name.
    /// - oneLetter: 1 letter day abbreviation of day name.
    /// - full: Full day name.
    public enum DayNameStyle {
        /// 3 letter day abbreviation of day name.
        case threeLetters
        
        /// 1 letter day abbreviation of day name.
        case oneLetter
        
        /// Full day name.
        case full
    }
    
    /// - threeLetters: 3 letter month abbreviation of month name.
    /// - oneLetter: 1 letter month abbreviation of month name.
    /// - full: Full month name.
    public enum MonthNameStyle {
        /// 3 letter month abbreviation of month name.
        case threeLetters
        
        /// 1 letter month abbreviation of month name.
        case oneLetter
        
        /// Full month name.
        case full
    }

    ///     Date().monthName(ofStyle: .oneLetter) -> "J"
    ///     Date().monthName(ofStyle: .threeLetters) -> "Jan"
    ///     Date().monthName(ofStyle: .full) -> "January"
    ///
    /// - Parameter Style: style of month name (default is MonthNameStyle.full).
    /// - Returns: month name string (example: D, Dec, December).
    public func monthName(ofStyle style: MonthNameStyle = .full) -> String {
        let dateFormatter = DateFormatter()
        var format: String {
            switch style {
            case .oneLetter:
                return "MMMMM"
            case .threeLetters:
                return "MMM"
            case .full:
                return "MMMM"
            }
        }
        dateFormatter.setLocalizedDateFormatFromTemplate(format)
        return dateFormatter.string(from: self)
    }
    
    ///     Date().day -> 12
    ///
    ///     var someDate = Date()
    ///     someDate.day = 1 // sets someDate's day of month to 1.
    ///
    public var day: Int {
        get {
            return Calendar.current.component(.day, from: self)
        }
    }
    
}
