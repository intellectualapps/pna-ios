//
//  StringExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var lenght: Int {
        get {
            return self.count
        }
    }

    func containsNumbers() -> Bool {
        let numberRegEx  = ".*[0-9]+.*"
        let testCase     = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return testCase.evaluate(with: self)
    }

    /**
     * Auto return two space if string is empty => to ignore auto size cell table
     */
    func minLenght2() -> String {
        if isEmpty {
            return "  "
        }
        return self
    }
    /*
     func getDateUTCFromString() -> Date? {
     let formatter = DateFormatter()
     formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
     formatter.locale = Locale(identifier: "en_US_POSIX")
     return formatter.date(from: self)
     }
     */
    func getDateUTCFromString(format: String = "MM dd, yyyy") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.locale = Locale(identifier: "en-US")
        return formatter.date(from: self)
    }
    
    func getDateFromString(format: String = "MM dd, yyyy") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: self)
    }

    func stringFromIndex(_ index: Int) -> String {
        if index >= self.lenght {
            return ""
        }
        return (self as NSString).substring(with: NSRange(location: index, length: self.lenght - index))
    }

    func stringToIndex(index: Int) -> String {
        return self.substring(to: self.characters.index(self.startIndex, offsetBy: index))
    }
    
    /*
     Get string localized in Locazable file.
     */
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }

    /**
     * Calulate size for text. 
     * boundSize: This is max bound text can display in
     */
    func sizeWithBoundSize(boundSize: CGSize,
                           option: NSStringDrawingOptions,
                           lineBreakMode: NSLineBreakMode = NSLineBreakMode.byTruncatingTail,
                           font: UIFont) -> CGSize {
        if self.isEmpty {
            return CGSize.zero
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = lineBreakMode
        return (self as NSString).boundingRect(with: boundSize,
                                               options: option,
                                               attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: paragraphStyle],
                                               context: nil).size
    }

    /**
     * Get date string from date with specify format (yyyy/MM/dd ....)
     */
    static func string(fromDate: Date, format: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = format
        return dateFormater.string(from: fromDate)
    }

    /**
     * Generate udid string. This value will unique within current devive.
     * format like this: AAAA-BBBB-CCCC-DDDD....
     */
    static func newUdid() -> String {
        if let aid = CFUUIDCreate(nil) {
            return CFUUIDCreateString(nil, aid) as String
        }
        return ""
    }

    /**
     * Return path of document directory in app folder.
     */
    static func documentDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(
            .documentDirectory,
            .userDomainMask,
            true)
        return paths.first ?? ""
    }

    /**
     * Return the normal log file
     * This file will log the status of DataMan System such as : conntect, disconnect,...
     */
    static func normalLogFilePath() -> String {
        let filename = "normalLog.csv"
        return String.documentDirectoryPath() + "/" + filename
    }

    /**
     * Return Data log file path
     */
    static func dataLogFilePath() -> String {
        let filename = "dataLog.csv"
        return String.documentDirectoryPath() + "/" + filename
    }

    /**
     * Return error log file path
     */
    static func errorLogFilePath() -> String {
        let filename = "error.csv"
        return String.documentDirectoryPath() + "/" + filename
    }

    func double() -> Double {
        return Double(self) ?? 0
    }

    func trim() -> String {
        let str = (self as NSString).replacingOccurrences(of: "\u{00a0}", with: "")
        return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }

    func mosaicAmountSpecify(largefont: UIFont = UIFont.boldSystemFont(ofSize: 30),
                             smallFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                             textColor: UIColor = .white) -> NSAttributedString {
        let attributes = [NSAttributedString.Key.font: largefont,
                          NSAttributedString.Key.foregroundColor: textColor]
        let moneyText = self.trim()
        let attributesText = NSMutableAttributedString(string: moneyText,
                                                       attributes: attributes)
        let range = (moneyText as NSString).range(of: ".")
        if  range.location != NSNotFound {
            attributesText.addAttributes(attributes, range: range)
        }
        let attributes2 = [NSAttributedString.Key.font: smallFont,
                           NSAttributedString.Key.foregroundColor: textColor]
        if range.location < moneyText.lenght {
            let subRange = NSRange(location: range.location, length: moneyText.lenght - range.location)

            attributesText.addAttributes(attributes2, range: subRange)
        }
        let range$ = (moneyText as NSString).range(of: "$")
        if  range$.location != NSNotFound {
            attributesText.addAttributes(attributes2, range: range$)
        }
        return attributesText
    }

    func getUserIdFromActor() -> Int {
        let array = (self as NSString).components(separatedBy: "_")
        if let last = array.last {
            return Int(last) ?? -1
        }
        return -1
    }

    func attributeText(textColor: UIColor, font: UIFont) -> NSAttributedString {
        let attributes = [NSAttributedString.Key.font: font,
                          NSAttributedString.Key.foregroundColor: textColor]
        let attributesText = NSMutableAttributedString(string: self,
                                                       attributes: attributes)
        return attributesText
    }

    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    func isValidPass(testStr: String) -> Bool {

        let emailRegEx = "^.*(?=.{6,})(?=.*[a-zA-Z])[a-zA-Z0-9]+$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    func isValidPasscode(testStr: String) -> Bool {

        let emailRegEx = "^.*(?=.{,4})(?=.*)[0-9]+$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    func defaultLogo() -> UIImage? {
        var iconname = ""
        let stockname = self.trim().lowercased()
        if stockname.lenght > 0 {
            iconname = stockname.stringToIndex(index: 1)
        }
        return UIImage(named: iconname.lowercased())
    }

    static func errorImagePath() -> String {
        return "/images/original/missing.png"
    }

    static func appVersion() -> String {
        if let info = Bundle.main.infoDictionary {
            return (info["CFBundleShortVersionString"] as? String) ?? ""
        }
        return ""
    }

    func imageFromBase64() -> UIImage? {
        if let data = Data(base64Encoded: self) {
            if let img = UIImage(data: data) {
                return img
            }
        }
        return nil
    }

    func attributeTextIncludeImage(imgName: String, font: UIFont = UIFont.systemFont(ofSize: 13)) -> NSAttributedString {
        let attachment = NSTextAttachment()
        attachment.bounds = CGRect(x: 0, y: 0, width: 10, height: 10)
        attachment.image = UIImage(named: imgName)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string: "")
        myString.append(attachmentString)
        let emptyAttr = NSAttributedString(string: " ")
        myString.append(emptyAttr)
        let sefltAttr = NSAttributedString(string: self)
        myString.append(sefltAttr)
        return myString
    }


    func attributeTextIncludeImage(img: UIImage, font: UIFont = UIFont.systemFont(ofSize: 13)) -> NSAttributedString {
        let attachment = NSTextAttachment()
        attachment.bounds = CGRect(x: 0, y: -10, width: 30, height: 30)
        attachment.image = img
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string: "")
        myString.append(attachmentString)
        let emptyAttr = NSAttributedString(string: " ")
        myString.append(emptyAttr)
        let sefltAttr = NSAttributedString(string: self)
        myString.append(sefltAttr)
        return myString
    }

    func removeHTML() -> String {
        var newstr = self
        if self.lenght > 10000 {
            newstr = self.stringToIndex(index: 10000)
        }
        let range = NSMakeRange(0, newstr.lenght)
        let str = (newstr as NSString).replacingOccurrences(of: "<[^>]+>", with: "", options: NSString.CompareOptions.regularExpression, range: range)
        return str
    }
    
    // hien.bui
    // MARK: - Get string
    // Ex: str = "Hello"
    /*
     Use: str[0] = "H"
          str[1..<3] = "el"
          str[1...3] = "ello"
    **/
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[start..<end])
    }
    
    func removeDiacritics() -> String {
        return self.folding(options: .diacriticInsensitive, locale: .current)
    }
    
    func removeSpecialChars() -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-_".characters)
        return String(self.characters.filter {okayChars.contains($0) })
    }
    
    func handleHtmlString() -> String {
        var htmlStr = self
        htmlStr = htmlStr.replacingOccurrences(of: "&lt;", with: "<")
        htmlStr = htmlStr.replacingOccurrences(of: "&gt;", with: ">")
        htmlStr = htmlStr.replacingOccurrences(of: "&quot;", with: "\"")
        htmlStr = htmlStr.replacingOccurrences(of: "&amp;lsquo;", with: "\'")
        htmlStr = htmlStr.replacingOccurrences(of: "&amp;rsquo;", with: "\'")
        htmlStr = htmlStr.replacingOccurrences(of: "&amp;ldquo;", with: "\"")
        htmlStr = htmlStr.replacingOccurrences(of: "&amp;rdquo;", with: "\"")
        return htmlStr
    }
    
    func intoNumberWithoutCommas() -> Int {
        return Int(self.replacingOccurrences(of: ",", with: "")) ?? 0
    }
}
