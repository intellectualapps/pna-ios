//
//  CloudinaryManage.swift
//  MinBar
//
//  Created by nguyen tuan on 3/18/18.
//  Copyright © 2018 Pentor. All rights reserved.
//

import Foundation
import Cloudinary
import CryptoSwift

typealias CLDResponse = (_ response: CLDUploadResult?, _ error: NSError?) -> ()
typealias ProgressHandle = (_ progress: Progress) -> ()

class CloudinaryManage {
    
    private let width: CGFloat = 500.0
    
    var cloudinary = CLDCloudinary(configuration: CLDConfiguration(cloudName: CloudinaryConfig.name.rawValue, apiKey: CloudinaryConfig.apiKey.rawValue, apiSecret: nil))
    
    func uploadImage(imageData: Data, params: CLDUploadRequestParams?, progress: ProgressHandle?, completion: CLDResponse?) {
        self.cloudinary.createUploader().signedUpload(data: imageData, params: params, progress: progress, completionHandler: completion)
    }
    
    func uploadAvatar(img: UIImage?, username: String, progress: ProgressHandle? = nil, completion: CLDResponse?) {
        let params = self.getRequestParams(for: username)
        guard let image = img else { return }
        guard let imgresize = resizeImage(image: image, newWidth: width) else { return }
        guard let data = image.jpegData(compressionQuality: 1) else { return }

        self.uploadImage(imageData: data, params: params, progress: progress, completion: completion)
    }
    
    func getRequestParams(for username: String) -> CLDUploadRequestParams {
        let timestamp: NSNumber = NSNumber(value: Int(NSDate().timeIntervalSince1970))
        let public_id = username.lowercased().removeSpecialChars()
        let folder = "users"
        let signatureString = "folder=\(folder)&public_id=\(public_id)&timestamp=\(timestamp)\(CloudinaryConfig.secret.rawValue)".sha1()
        let signature = CLDSignature(signature: signatureString, timestamp: timestamp)
        let params = CLDUploadRequestParams()
        params.setSignature(signature)
        params.setPublicId(public_id)
        params.setFolder(folder)
        
        return params
    }
    
    func getAvatarUrl(public_id: String?) -> String? {
        guard let id = public_id else { return nil }
        let url = self.cloudinary.createUrl().generate(id)
        return url
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        let size = image.size
        
        let widthRatio  = newWidth  / size.width
        let heightRatio = widthRatio
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
