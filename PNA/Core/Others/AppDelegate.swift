//
//  AppDelegate.swift
//  Realm
//
//  Created by Nguyen Van Dung on 9/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SlideMenuControllerSwift
import FBSDKLoginKit
import Firebase
import FirebaseInstanceID
import UserNotifications
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var loggedUser: User? {
        didSet {
            if let usr = loggedUser {
                let token = usr.authToken ?? ""
                Defaults.authenToken.set(value: token)
                if token.length > 0 {
                    let userData = NSKeyedArchiver.archivedData(withRootObject: usr)
                    UserDefaults.standard.set(userData, forKey: "user")
                    UserDefaults.standard.synchronize()
                }
            }
        }
    }
    var window: UIWindow?

    class func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        //load custom font
        UIFont.loadCustomFont()
        //get user from disk
        loggedUser = User.archivedUser()
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        //face
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        if let user = loggedUser, user.token.length > 0 {
            window = UIWindow(frame: UIScreen.main.bounds)
            if user.alreadyCreateProfile() {
                setupFeedMenu()
            } else {
                showCreateProfile()
            }
        } else {
            showCustomScreen("Login")
        }
        /* Setup receive notification */

        PushNotificationService.shared.registerRemoteNotification(application)

        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotificaiton),
                                               name: Notification.Name.MessagingRegistrationTokenRefreshed, object: nil)

        /* process off notitication payload */
        if let launchOptions = launchOptions {
            if let payload = launchOptions[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
                PushNotificationService.shared.processOffNotificationPayload(application, payload)
            }
        }
        FirebaseConfigurator.setUp()

        // GoogleMap
        GMSServices.provideAPIKey(GoogleMap.apiKey)
        GMSPlacesClient.provideAPIKey(GoogleMap.apiKey)
        window?.makeKeyAndVisible()
        return true
    }

    func applicationDidFinishLaunching(_ application: UIApplication) {

    }
    private func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        return true
    }
    
    @objc func tokenRefreshNotificaiton() {
        FirebaseConfigurator.tokenRefreshNotificaiton()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]){
        PushNotificationService.shared.didReceiveRemoteNotification(application, userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        Defaults.deviceToken.set(value: deviceTokenString)
        Messaging.messaging().setAPNSToken(deviceToken, type: .unknown)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func showCreateProfile() {
        let controller =             UIStoryboard.instantiate(ProfileViewController.self, storyboardType: .createProfile)
        let navi = CustomNavigationController(rootViewController: controller)
        self.window?.rootViewController = navi
        self.window?.makeKeyAndVisible()
    }

    func showCustomScreen(_ name: String) {
        let storyboard = UIStoryboard.init(name: name, bundle: nil)
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.rootViewController = storyboard.instantiateInitialViewController()
        self.window?.makeKeyAndVisible()
    }
    
    func setupFeedMenu() {
        let welcome = UIStoryboard(name: "WelcomeScreen", bundle: nil)
        let mainViewController = welcome.instantiateViewController(withIdentifier: "WelcomeNavigation")
        let leftViewController = UIStoryboard.instantiate(LeftMenuViewController.self, storyboardType: .leftMenu)
        let slideMenuController = ExSlideMenuController(mainViewController:mainViewController, leftMenuViewController: leftViewController)
        SlideMenuOptions.contentViewScale = 1
        slideMenuController.changeLeftViewWidth(UIScreen.main.bounds.width * 0.57)
        slideMenuController.automaticallyAdjustsScrollViewInsets = false

        if let window = self.window {
            window.makeKeyAndVisible()
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                window.rootViewController = slideMenuController
            }, completion: { completed in
                // maybe do something here
            })
        }
    }
}
