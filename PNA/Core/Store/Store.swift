//
//  Store.swift
//  RealmInUse
//
//  Created by Nguyen Van Dung on 10/5/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
/* HOW TO USE.
 - Each store should have protocol and implement class => better to write unit test
 - Store contains logic to stores data to local storage, local database
 - Store is where call api request
 - Check te APIServiceBase module to see how to create store. This sample will integrate with APIServiceBase
 */

class Store: NSObject {
    
}
