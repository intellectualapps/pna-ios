//
//  PushNotificationService.swift
//  MtoM
//
//  Created by nguyen van dung on 12/16/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import Whisper

struct PushPayloadInfo {
    var payloadMessage = ""
    var payloadTitle = ""
    var notiType: NotifyPayloadType?
    var memberID = 0
    var memberName = ""
    var networkId = 0
}

class PushNotificationService {
    static let shared = PushNotificationService()

    func unRegisterRemoteNotification() {
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    func registerRemoteNotification(_ application: UIApplication) {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        application.registerForRemoteNotifications()
    }

    func didRegisterForRemoteNotificationsWithDeviceToken(deviceToken: Data) {
        let desc = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        let deviceTokenString = desc.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        let newToken = deviceTokenString.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        if !newToken.isEmpty {
            print("token = \(newToken)")
        }
    }
    
    func sentTokenDeviceToServerIfNeed(_ isNeedProgress: Bool) {
        
    }
    
    func didReceiveRemoteNotification(_ application: UIApplication, userInfo: [AnyHashable: Any]) {
        self.processContentOfRemoteNotification(application, userInfo)
    }
    
    func didFailToRegisterForRemoteNotificationsWithError(_ application: UIApplication, error: Error) {
        print("!!!!!failed to register notification!!!!! : \(error)")
    }
    
    func isAppActive() -> Bool {
        let state: UIApplication.State = UIApplication.shared.applicationState
        return state == UIApplication.State.active
    }

    func processContentOfRemoteNotification(_ application: UIApplication, _ payload: [AnyHashable: Any]) {
        guard let payload = payload as? [String: AnyObject] else {
            return
        }
        guard let aps = payload["aps"] as? [String : Any] else {
            return
        }
        guard let alert = aps["alert"] as? [String : Any] else {
            return
        }
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            var contentInfo = PushPayloadInfo()
            print(payload)
            // Parser payload
            if let message = alert["body"] as? String {
                contentInfo.payloadMessage = message
            }
            if let title = alert["title"] as? String {
                contentInfo.payloadTitle = title
            }
            contentInfo.networkId = aps.intForKey(key: "networkId")
            if let notiType = aps["notificationType"] as? String {
                contentInfo.notiType = NotifyPayloadType(rawValue: notiType)
            }
            if let acme2 = payload["acme2"] as? [String : Any] {
                contentInfo.memberID = acme2.intForKey(key: "memberId")
                if let memberName = acme2["memberName"] as? String {
                    contentInfo.memberName = memberName
                }
            }
            switch application.applicationState {
            case .active:
                self.showNotiWhenAppActive(info: contentInfo)
            case .inactive, .background:
                self.onTapNotification(info: contentInfo)
            }
        }
        
    }
    
    private func onTapNotification(info: PushPayloadInfo) {
        guard let notiTypeEnum = info.notiType else { return }
        switch notiTypeEnum {
        case .request:
            self.toNetworkInvitationScreen(requestUserID: info.networkId, memberID: info.memberID)
        default:
            break
        }
    }
    
    private func toNetworkInvitationScreen(requestUserID: Int, memberID: Int) {
        let invitationVC = UIStoryboard.instantiate(NetworkInvitationViewController.self, storyboardType: .networkInvitation)
        object_setClass(invitationVC, InvitationProfileViewController.self)
        if let notificationInvitation = invitationVC as? InvitationProfileViewController {
            notificationInvitation.invitationProfileViewModel = InvitationProfileViewModel()
            notificationInvitation.invitationProfileViewModel?.requestUserID = memberID
            notificationInvitation.invitationProfileViewModel?.networkRequestID = requestUserID
            let nav = UINavigationController(rootViewController: notificationInvitation)
            UIViewController.topMostViewController()?.present(nav, animated: true, completion: nil)
        }
    }

    private func showNotiWhenAppActive(info: PushPayloadInfo) {
        let noti = Announcement(title: info.payloadTitle, subtitle: info.payloadMessage, image: UIImage.init(named: "pna_africa_map"), duration: 5) {
            if let type = info.notiType, type == .request {
                self.onTapNotification(info: info)
            }
        }
        if let navigation = UIViewController.topMostViewController()?.navigationController {
            Whisper.show(shout: noti, to: navigation)
        } else {
            if let viewController = UIViewController.topMostViewController() {
                Whisper.show(shout: noti, to: viewController)
            }
        }
    }
    
    /*
     - This process for notification receiced when app do not open
     */
    func processOffNotificationPayload(_ application: UIApplication, _ notificationObj: [AnyHashable: Any]?) {
        if let info = notificationObj {
            self.processContentOfRemoteNotification(application, info)
        }
    }
}
