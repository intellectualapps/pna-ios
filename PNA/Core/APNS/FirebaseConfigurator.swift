//
//  FirebaseConfigurator.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/3/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Firebase

class FirebaseConfigurator {
    private init() {}
    
    class func setUp() {
        var filePath: String?
        filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
        //        #if ENVIRONMENT_DEVELOPMENT
        //            filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
        //        #else
        //            filePath = Bundle.main.path(forResource: "GoogleService-production", ofType: "plist")
        //        #endif
        if let firebaseOption = FirebaseOptions(contentsOfFile: filePath!) {
            print(firebaseOption.projectID)
            FirebaseApp.configure(options: firebaseOption)
        }
    }
    
    class func tokenRefreshNotificaiton() {
        guard let refreshedToken = InstanceID.instanceID().token() else {
            return
        }
        print("==================================")
        print("FCMToken: \(refreshedToken)")
        print("==================================")
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        Messaging.messaging().shouldEstablishDirectChannel = false
        Messaging.messaging().connect { (error) in
            if error != nil {
                NSLog("Unable to connect with FCM. \(String(describing: error))")
            } else {
                NSLog("Connected to FCM.")
            }
        }
    }
}
