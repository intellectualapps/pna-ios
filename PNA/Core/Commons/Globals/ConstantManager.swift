//
//  ConstantManager.swift
//  PNA
//
//  Created by MTQ on 3/17/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

struct FontConstant {
    enum FontType {
        case firaSansBold
        case firaSansRegular
        case regular
        case bold
    }
    
    static func font(_ type: FontType, _ fontSize: CGFloat) -> UIFont {
        switch type {
        case .firaSansBold:
            return UIFont(name: "FiraSans-Bold", size: fontSize)
                ?? FontConstant.font(.regular, fontSize)
        case .firaSansRegular:
            return UIFont(name: "FiraSans-Regular", size: fontSize)
                ?? FontConstant.font(.bold, fontSize)
        case .regular:
            return UIFont.systemFont(ofSize: fontSize)
        case .bold:
            return UIFont.boldSystemFont(ofSize: fontSize)
        }
    }
}

struct GoogleMap {
    static let apiKey = "AIzaSyAo_e0WgaaSkV850d3zcTrxbl6Q2bHk-6Y"
}
