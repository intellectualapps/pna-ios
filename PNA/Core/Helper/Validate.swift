//
//  Validate.swift
//  PNA
//
//  Created by Admin on 1/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import PKHUD

class Validate: NSObject {
    
    func required(_ textField: UITextField?, message: String) -> Bool {
        guard let email = textField?.text, !email.isEmpty else {
            HUD.flash(.labeledError(title: nil, subtitle: message), delay: 2)
            textField?.becomeFirstResponder()
            return false
        }
        return true
    }

}
