//
//  PeriodPickerView.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/2/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class PeriodPickerView: UIView {

    @IBOutlet weak var monthYearPickerView: MonthYearPickerView!
    @IBOutlet weak var contentView: UIView!
    
    var doneClosure: ((Int, String, Int)->())?
    var cancelClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("PeriodPickerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        self.cancelClosure?()
    }
    
    @IBAction func onNow(_ sender: UIButton) {
        let currentMonth = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.month, from: NSDate() as Date)
        let currentYear = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
        monthYearPickerView.selectRow(currentMonth - 1, inComponent: 0, animated: true)
        monthYearPickerView.selectRow(0, inComponent: 1, animated: true)
        monthYearPickerView.month = currentMonth
        monthYearPickerView.year = currentYear
    }
    
    @IBAction func onDone(_ sender: UIButton) {
        self.doneClosure?(monthYearPickerView.month, DateFormatter().shortMonthSymbols[monthYearPickerView.month - 1].capitalized, monthYearPickerView.year)
    }
}
