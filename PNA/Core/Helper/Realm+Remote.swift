//
//  Realm+Remote.swift
//  RealmInUse
//
//  Created by Nguyen Van Dung on 10/5/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

/**
 * HOW TO SETUP REALM SERVER: https://realm.io/docs/realm-object-server
 * HOW TO CONFIG SERVER SECURITY: https://access.redhat.com/documentation/en-us/red_hat_jboss_enterprise_application_platform/6.4/html-single/how_to_configure_server_security/
 * This extension will show you A generic interface for logging in to Realm Mobile Platform apps
*/
extension Realm {
    func setupRealm() {
        
    }
}
