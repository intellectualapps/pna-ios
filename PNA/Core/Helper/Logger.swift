//
//  Logger.swift
//  PNA
//
//  Created by Nguyen Van Dung on 3/4/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class Logger: NSObject {
    class func log(value: Any?) {
        if let value = value {
            print(value)
        }
    }
}
