//
//  Realm+Security.swift
//  RealmInUse
//
//  Created by Nguyen Van Dung on 10/5/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    /**
     *
     */
    func encryptRealmDatabase(key: Data, destinationPath: String) throws {
        let fileUrl = URL(fileURLWithPath: destinationPath)
        try self.writeCopy(toFile: fileUrl, encryptionKey: key)
    }
}
