//
//  AlertUtils.swift
//  PNA
//
//  Created by bui.duy.hien on 3/12/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import PopupDialog

class AlertUtils {
    static let shared = AlertUtils()
    private let dialogAppearance = PopupDialogDefaultView.appearance()
    private let buttonAppearance = PopupDialogButton.appearance()
    
    private init() {
        dialogAppearance.backgroundColor      = UIColor.white
        dialogAppearance.titleFont            = UIFont.boldSystemFont(ofSize: 16)
        dialogAppearance.titleColor           = UIColor(hex: "484848")
        dialogAppearance.titleTextAlignment   = .center
        dialogAppearance.messageFont          = UIFont.systemFont(ofSize: 16)
        dialogAppearance.messageColor         = UIColor(hex: "484848")
        dialogAppearance.messageTextAlignment = .center
        
        buttonAppearance.titleFont = UIFont.systemFont(ofSize: 16)
    }
    
    func showMessage(viewController: UIViewController, title: String?, message: String?, completion: (()->())? = nil) {
        // Create the dialog

        let popup = PopupDialog(title: title, message: message)
        popup.transitionStyle = .zoomIn
        
        let buttonOne = PopupDialogButton(title: "OK") {
            completion?()
        }
        popup.addButtons([buttonOne])
        
        // Present dialog
        viewController.present(popup, animated: true, completion: nil)
    }

    /**
     @objc public convenience init(
     title: String?,
     message: String?,
     image: UIImage? = nil,
     buttonAlignment: NSLayoutConstraint.Axis = .vertical,
     transitionStyle: PopupDialogTransitionStyle = .bounceUp,
     preferredWidth: CGFloat = 340,
     tapGestureDismissal: Bool = true,
     panGestureDismissal: Bool = true,
     hideStatusBar: Bool = false,
     completion: (() -> Void)? = nil) {
     */
    
    func showConfirmMessage(viewController: UIViewController, title: String, message: String, btnOkTitle: String, btnCancelTitle: String, handle: @escaping ((Bool) -> ())) {
        // Create the dialog
        let popup = PopupDialog(title: title, message: message, image: nil, buttonAlignment: NSLayoutConstraint.Axis.horizontal, transitionStyle: .zoomIn, preferredWidth: 340, tapGestureDismissal: true, panGestureDismissal: true, hideStatusBar: false) {

        }
        dialogAppearance.titleTextAlignment = .center
        
        let buttonCancel = PopupDialogButton(title: btnCancelTitle) {
            handle(false)
        }
        buttonCancel.titleColor = UIColor(hex: "484848").withAlphaComponent(0.8)
        
        let buttonOk = PopupDialogButton(title: btnOkTitle) {
            handle(true)
        }
        buttonOk.titleColor = UIColor(hex: "73d216")
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButtons([buttonCancel, buttonOk])
        
        // Present dialog
        viewController.present(popup, animated: true, completion: nil)
    }
}
