//
//  Defaults.swift
//  PNA
//
//  Created by nguyen.van.dungb on 2/5/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

enum Defaults: String {
    case didShowOnboarding = "didShowOnboarding"
    case authenToken = "authenToken"
    case deviceToken = "deviceToken"
    case isNotFirstTime = "isNotFirstTime"


    func set(value: Any?) {
        UserDefaults.standard.set(value, forKey: self.rawValue)
        UserDefaults.standard.synchronize()
    }

    func getString() -> String {
        if let str = self.get() as? String {
            return str
        }
        return ""
    }
    
    func get() -> Any? {
        return UserDefaults.standard.object(forKey: self.rawValue)
    }
    
    func getBool() -> Bool {
        return UserDefaults.standard.bool(forKey: self.rawValue)
    }
}
