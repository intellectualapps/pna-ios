//
//  RealmManager.swift
//  Realm
//
//  Created by Nguyen Van Dung on 9/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class RealmControl: NSObject {
    //share instance
    static let shared = RealmControl()
    var realm: Realm?

    func write(_ constructor: () -> ()) {
        realm?.beginWrite()
    }

    func documentPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if let path = paths.first {
            return path
        }
        return ""
    }

    /**
     * @encrypeKey : This is key encoded 128 bytes
     * @destination : This is folder will hold file .realm. App will interact with this file during working. Note: this is database file end point
     */
    func prepare(encrypeKey: String = "", destination: String = "") {
        var urlPath = ""
        if destination.isEmpty {
            urlPath = self.documentPath() + "/database.realm"
        } else {
            urlPath = destination
        }
        let url = URL(fileURLWithPath: urlPath)
        let config =  RLMRealmConfiguration.default()
        config.fileURL = url
        //set encrype key
        if let dataKey = encrypeKey.data(using: .utf8) {
            config.encryptionKey = dataKey
        }
        RLMRealmConfiguration.setDefault(config)
        realm = try? Realm()
    }

    /**
     * The way generate encrype key
     * Return tupple: $0 is key data,  $1: is status of process generate key
     */
    func generateKey() -> (Data, Bool) {
        // Generate 64 bytes of random data to serve as the encryption key
        let key = NSMutableData(length: 64)!
        let result = SecRandomCopyBytes(kSecRandomDefault, key.length, (key.mutableBytes))
        return (key as Data, result == 0)
    }
}
