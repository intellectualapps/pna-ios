//
//  LargeExpandButton.swift
//  CreatorsClick
//
//  Created by Nguyen Van Dung on 4/8/17.
//  Copyright © 2017 Framgia. All rights reserved.
//

import UIKit

class LargeExpandButton: UIButton {
    var extendedEdges: UIEdgeInsets = .zero
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let newBound = CGRect(
            x: self.bounds.origin.x - extendedEdges.left,
            y: self.bounds.origin.y - extendedEdges.top,
            width: self.bounds.width +  extendedEdges.left +  extendedEdges.right,
            height: self.bounds.height + extendedEdges.top +  extendedEdges.bottom
        )
        return newBound.contains(point)
    }

}
