//
//  LocationManager.swift
//
//
//  Created by Jimmy Jose on 14/08/14.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CON
//NECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


import UIKit
import CoreLocation
import MapKit

typealias LMReverseGeocodeCompletionHandler = ((_ reverseGecodeInfo:NSDictionary?,_ placemark:CLPlacemark?, _ error:String?)->Void)?
typealias LMGeocodeCompletionHandler = ((_ gecodeInfo:NSDictionary?,_ placemark:CLPlacemark?, _ error:String?)->Void)?
typealias LMLocationCompletionHandler = ((_ latitude:Double, _ longitude:Double, _ status:String, _ verboseMessage:String, _ error:String?)->())?

// Todo: Keep completion handler differerent for all services, otherwise only one will work
enum GeoCodingType{

    case geocoding
    case reverseGeocoding
}

class LocationManager: NSObject,CLLocationManagerDelegate {

    /* Private variables */
    fileprivate var completionHandler:LMLocationCompletionHandler

    fileprivate var reverseGeocodingCompletionHandler:LMReverseGeocodeCompletionHandler
    fileprivate var geocodingCompletionHandler:LMGeocodeCompletionHandler

    fileprivate var locationStatus : NSString = "Calibrating"// to pass in handler
    fileprivate var locationManager: CLLocationManager!
    fileprivate var verboseMessage = "Calibrating"

    fileprivate let verboseMessageDictionary = [CLAuthorizationStatus.notDetermined:"You have not yet made a choice with regards to this application.",
                                                CLAuthorizationStatus.restricted:"This application is not authorized to use location services. Due to active restrictions on location services, the user cannot change this status, and may not have personally denied authorization.",
                                                CLAuthorizationStatus.denied:"You have explicitly denied authorization for this application, or location services are disabled in Settings.",
                                                CLAuthorizationStatus.authorizedAlways:"App is Authorized to use location services.",
                                                CLAuthorizationStatus.authorizedWhenInUse:"You have granted authorization to use your location only when the app is visible to you."]


    var delegate:LocationManagerDelegate? = nil

    var latitude:Double = 0.0
    var longitude:Double = 0.0

    var latitudeAsString:String = ""
    var longitudeAsString:String = ""


    var lastKnownLatitude:Double = 0.0
    var lastKnownLongitude:Double = 0.0

    var lastKnownLatitudeAsString:String = ""
    var lastKnownLongitudeAsString:String = ""

    var keepLastKnownLocation:Bool = true
    var hasLastKnownLocation:Bool = true

    var autoUpdate:Bool = false

    var showVerboseMessage = false

    var isRunning = false


    class var sharedInstance : LocationManager {
        struct Static {
            static let instance : LocationManager = LocationManager()
        }
        return Static.instance
    }

    fileprivate override init() {
        super.init()
        if(!autoUpdate){
            autoUpdate = !CLLocationManager.significantLocationChangeMonitoringAvailable()
        }
    }

    fileprivate func resetLatLon() {
        latitude = 0.0
        longitude = 0.0
        latitudeAsString = ""
        longitudeAsString = ""
    }

    fileprivate func resetLastKnownLatLon() {
        hasLastKnownLocation = false
        lastKnownLatitude = 0.0
        lastKnownLongitude = 0.0
        lastKnownLatitudeAsString = ""
        lastKnownLongitudeAsString = ""
    }

    func startUpdatingLocationWithCompletionHandler(_ completionHandler:((_ latitude:Double, _ longitude:Double, _ status:String, _ verboseMessage:String, _ error:String?)->())? = nil) {
        self.completionHandler = completionHandler
        initLocationManager()
    }

    func startUpdatingLocation() {
        initLocationManager()
    }

    func stopUpdatingLocation() {
        if(autoUpdate) {
            locationManager.stopUpdatingLocation()
        } else {
            locationManager.stopMonitoringSignificantLocationChanges()
        }

        resetLatLon()
        if(!keepLastKnownLocation) {
            resetLastKnownLatLon()
        }
    }

    func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        // locationManager.locationServicesEnabled
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        let Device = UIDevice.current
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        let iOS8 = iosVersion >= 8
        if (iOS8) {
            locationManager.requestWhenInUseAuthorization() // add in plist NSLocationWhenInUseUsageDescription
        }
        startLocationManger()
    }

    fileprivate func startLocationManger() {
        if(autoUpdate) {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.startMonitoringSignificantLocationChanges()
        }
        isRunning = true
    }

    fileprivate func stopLocationManger() {
        if(autoUpdate) {
            locationManager.stopUpdatingLocation()
        } else {
            locationManager.stopMonitoringSignificantLocationChanges()
        }
        isRunning = false
    }

    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        stopLocationManger()
        resetLatLon()
        if(!keepLastKnownLocation) {
            resetLastKnownLatLon()
        }
        var verbose = ""
        if showVerboseMessage {verbose = verboseMessage}
        completionHandler?(0.0, 0.0, locationStatus as String, verbose,error.localizedDescription)

        if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerReceivedError(_:))))!) {
            delegate?.locationManagerReceivedError!(error.localizedDescription as NSString)
        }
    }

    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let arrayOfLocation = locations as NSArray
        let location = arrayOfLocation.lastObject as! CLLocation
        let coordLatLon = location.coordinate
        latitude  = coordLatLon.latitude
        longitude = coordLatLon.longitude
        latitudeAsString  = coordLatLon.latitude.description
        longitudeAsString = coordLatLon.longitude.description
        var verbose = ""
        if showVerboseMessage {verbose = verboseMessage}
        if(completionHandler != nil) {
            completionHandler?(latitude, longitude, locationStatus as String,verbose, nil)
        }

        lastKnownLatitude = coordLatLon.latitude
        lastKnownLongitude = coordLatLon.longitude
        lastKnownLatitudeAsString = coordLatLon.latitude.description
        lastKnownLongitudeAsString = coordLatLon.longitude.description
        hasLastKnownLocation = true

        if (delegate != nil) {
            if((delegate?.responds(to: #selector(LocationManagerDelegate.locationFoundGetAsString(_:longitude:))))!) {
                delegate?.locationFoundGetAsString!(latitudeAsString as NSString,longitude:longitudeAsString as NSString)
            }

            if((delegate?.responds(to: #selector(LocationManagerDelegate.locationFound(_:longitude:))))!){
                delegate?.locationFound(latitude,longitude:longitude)
            }
        }
    }

    internal func locationManager(_ manager: CLLocationManager,
                                  didChangeAuthorization status: CLAuthorizationStatus) {
        var hasAuthorised = false
        let verboseKey = status
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access"
        case CLAuthorizationStatus.denied:
            locationStatus = "Denied access"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Not determined"
        default:
            locationStatus = "Allowed access"
            hasAuthorised = true
        }

        verboseMessage = verboseMessageDictionary[verboseKey]!

        if (hasAuthorised == true) {
            startLocationManger()
        } else {
            resetLatLon()
            if (!locationStatus.isEqual(to: "Denied access")) {
                var verbose = ""
                if showVerboseMessage {
                    verbose = verboseMessage
                    if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerVerboseMessage(_:))))!) {
                        delegate?.locationManagerVerboseMessage!(verbose as NSString)
                    }
                }

                if(completionHandler != nil) {
                    completionHandler?(latitude, longitude, locationStatus as String, verbose,nil)
                }
            }
            if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerStatus(_:))))!) {
                delegate?.locationManagerStatus!(locationStatus)
            }
        }

    }

    func reverseGeocodeLocationWithLatLon(latitude:Double, longitude: Double,onReverseGeocodingCompletionHandler:LMReverseGeocodeCompletionHandler) {
        let location:CLLocation = CLLocation(latitude:latitude, longitude: longitude)
        reverseGeocodeLocationWithCoordinates(location,onReverseGeocodingCompletionHandler: onReverseGeocodingCompletionHandler)
    }

    func reverseGeocodeLocationWithCoordinates(_ coord:CLLocation, onReverseGeocodingCompletionHandler:LMReverseGeocodeCompletionHandler) {
        self.reverseGeocodingCompletionHandler = onReverseGeocodingCompletionHandler
        reverseGocode(coord)
    }

    fileprivate func reverseGocode(_ location:CLLocation) {
        let geocoder: CLGeocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
            if (error != nil) {
                self.reverseGeocodingCompletionHandler!(nil,nil, error!.localizedDescription)
            } else {
                if let placemark = placemarks?.first {
                    let address = AddressParser()
                    address.parseAppleLocationData(placemark)
                    let addressDict = address.getAddressDictionary()
                    self.reverseGeocodingCompletionHandler!(addressDict,placemark,nil)
                } else {
                    self.reverseGeocodingCompletionHandler!(nil,nil,"No Placemarks Found!")
                    return
                }
            }
        })
    }

    func geocodeAddressString(address:NSString, onGeocodingCompletionHandler:LMGeocodeCompletionHandler) {
        self.geocodingCompletionHandler = onGeocodingCompletionHandler
        geoCodeAddress(address)
    }

    fileprivate func geoCodeAddress(_ address:NSString) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address as String, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            if (error != nil) {
                self.geocodingCompletionHandler!(nil,nil,error!.localizedDescription)
            } else {
                if let placemark = placemarks?.first {
                    let address = AddressParser()
                    address.parseAppleLocationData(placemark)
                    let addressDict = address.getAddressDictionary()
                    self.geocodingCompletionHandler!(addressDict,placemark,nil)
                } else {
                    self.geocodingCompletionHandler!(nil,nil,"invalid address: \(address)")
                }
            }
            } as! CLGeocodeCompletionHandler)
    }

    func geocodeUsingGoogleAddressString(address:NSString, onGeocodingCompletionHandler:LMGeocodeCompletionHandler){
        self.geocodingCompletionHandler = onGeocodingCompletionHandler
        geoCodeUsignGoogleAddress(address)
    }

    fileprivate func geoCodeUsignGoogleAddress(_ address:NSString) {
        let tempUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=\(address)&sensor=true&language=ng" as NSString
        if let urlString = tempUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            performOperationForURL(urlString as NSString, type: GeoCodingType.geocoding)
        }
    }

    func reverseGeocodeLocationUsingGoogleWithLatLon(latitude:Double, longitude: Double,onReverseGeocodingCompletionHandler:LMReverseGeocodeCompletionHandler) {
        self.reverseGeocodingCompletionHandler = onReverseGeocodingCompletionHandler
        reverseGocodeUsingGoogle(latitude: latitude, longitude: longitude)
    }

    func reverseGeocodeLocationUsingGoogleWithCoordinates(_ coord:CLLocation, onReverseGeocodingCompletionHandler:LMReverseGeocodeCompletionHandler){
        reverseGeocodeLocationUsingGoogleWithLatLon(latitude: coord.coordinate.latitude, longitude: coord.coordinate.longitude, onReverseGeocodingCompletionHandler: onReverseGeocodingCompletionHandler)
    }

    fileprivate func reverseGocodeUsingGoogle(latitude:Double, longitude: Double) {
        var urlString = "http://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&sensor=false&region=jp&language=ng"
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        performOperationForURL(self.googleApiUrl(latidue: latitude, longtidue: longitude) as NSString, type: GeoCodingType.reverseGeocoding)
    }

    fileprivate func googleApiUrl(latidue: Double, longtidue: Double) -> String {
        var urlString = "http://maps.googleapis.com/maps/api/geocode/json?latlng=\(latidue),\(longtidue)&sensor=true&language=ng"
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        return urlString as String
    }

    fileprivate func performOperationForURL(_ urlString:NSString,type:GeoCodingType) {
        guard let url = URL(string:urlString as String) else {
            return
        }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                self.setCompletionHandler(responseInfo: nil, placemark: nil, error: error?.localizedDescription, type: type)
            } else {
                let kStatus = "status"
                let kOK = "ok"
                let kZeroResults = "ZERO_RESULTS"
                let kAPILimit = "OVER_QUERY_LIMIT"
                let kRequestDenied = "REQUEST_DENIED"
                let kInvalidRequest = "INVALID_REQUEST"
                let kInvalidInput =  "Invalid Input"
                let jsonResult: NSDictionary = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary

                var status = jsonResult.value(forKey: kStatus) as! NSString
                status = status.lowercased as NSString

                if(status.isEqual(to: kOK)) {
                    let result = kAddressParse.parseGoogleLocationData(jsonResult)
                    if (result.isFound == true) {
                        self.setCompletionHandler(responseInfo:result.addressInfo.getAddressDictionary(), placemark:nil, error: nil, type:type)
                    } else if (result.isFound == false && result.addressInfo.isAvailableForRetry() == true) {
                        self.performOperationForURL(self.googleApiUrl(latidue:result.addressInfo.latitude, longtidue: result.addressInfo.longitude) as NSString, type: type)
                    } else {
                        //Not found
                        self.setCompletionHandler(responseInfo:nil, placemark:nil, error:kInvalidInput, type:type)
                    }
                } else if(!status.isEqual(to: kZeroResults) && !status.isEqual(to: kAPILimit) && !status.isEqual(to: kRequestDenied) && !status.isEqual(to: kInvalidRequest)){
                    self.setCompletionHandler(responseInfo:nil, placemark:nil, error:kInvalidInput, type:type)
                } else {
                    self.setCompletionHandler(responseInfo:nil, placemark:nil, error:status as String, type:type)
                }
            }
        }
        task.resume()
    }

    fileprivate func setCompletionHandler(responseInfo:NSDictionary?,placemark:CLPlacemark?, error:String?,type:GeoCodingType) {
        if(type == GeoCodingType.geocoding) {
            self.geocodingCompletionHandler!(responseInfo,placemark,error)
        } else {
            self.reverseGeocodingCompletionHandler!(responseInfo,placemark,error)
        }
    }
}


@objc protocol LocationManagerDelegate : NSObjectProtocol
{
    func locationFound(_ latitude:Double, longitude:Double)
    @objc optional func locationFoundGetAsString(_ latitude:NSString, longitude:NSString)
    @objc optional func locationManagerStatus(_ status:NSString)
    @objc optional func locationManagerReceivedError(_ error:NSString)
    @objc optional func locationManagerVerboseMessage(_ message:NSString)
}

class kAddressParse: NSObject {
    fileprivate var latitude: Double = 0
    fileprivate var longitude: Double = 0
    fileprivate var perfecture = ""
    fileprivate var city = ""
    fileprivate var town = ""
    fileprivate var choume = ""
    fileprivate var banchi = ""
    fileprivate var gou = ""
    fileprivate var formattedAddress = ""
    fileprivate var postalCode = ""
    fileprivate var countryCode = ""
    fileprivate var name = ""
    func getAddressDictionary()-> NSDictionary {
        let addressDict = NSMutableDictionary()
        addressDict.setValue(latitude, forKey: "latitude")
        addressDict.setValue(longitude, forKey: "longitude")
        addressDict.setValue(perfecture, forKey: "perfecture")
        addressDict.setValue(city, forKey: "city")
        addressDict.setValue(town, forKey: "town")
        addressDict.setValue(choume, forKey: "choume")
        addressDict.setValue(banchi, forKey: "banchi")
        addressDict.setValue(gou, forKey: "gou")
        addressDict.setValue(formattedAddress, forKey: "formattedAddress")
        addressDict.setValue(postalCode, forKey: "postalCode")
        addressDict.setValue(name, forKey: "name")
        return addressDict
    }

    func isFoundAddress() -> Bool {
        if (self.city.length > 0
            || self.town.length > 0
            || self.choume.length > 0
            || self.banchi.length > 0
            || self.gou.length > 0) {
            return true
        }
        return false
    }

    fileprivate func isAvailableForRetry() -> Bool {
        return self.postalCode.length > 0 && self.latitude > 0 && self.longitude > 0
    }

    class func arrayOfAvailabelAddress(_ resultDict: NSDictionary) -> [kAddressParse] {
        var arrayOfAvailabelAddress: [kAddressParse] = [kAddressParse]()
        if let results = resultDict.value(forKey: "results") as? NSArray {
            for locationObj in results {
                if let locationDict = locationObj as? NSDictionary {
                    let info: kAddressParse = kAddressParse()
                    //formatted address
                    if let address = locationDict.object(forKey: "formatted_address") as? String {
                        info.formattedAddress = address
                    }
                    //coordinate
                    if let geometry = locationDict.object(forKey: "geometry") as? NSDictionary {
                        if let location = geometry.object(forKey: "location") as? NSDictionary {
                            if let latidue = location.object(forKey: "lat") {
                                info.latitude = (latidue as AnyObject).doubleValue ?? 0
                            }
                            if let lng = location.object(forKey: "lng") {
                                info.longitude = (lng as AnyObject).doubleValue ?? 0
                            }
                        }
                    }

                    //address component
                    if let addressComponents = locationDict.object(forKey: "address_components") as? NSArray {
                        //name
                        info.name = component("premise", inArray: addressComponents, ofType: "long_name") as String
                        //perfecture
                        info.perfecture = component("administrative_area_level_1", inArray: addressComponents, ofType: "long_name") as String
                        info.city = component("locality", inArray: addressComponents, ofType: "long_name") as String
                        info.town = component("sublocality_level_1", inArray: addressComponents, ofType: "long_name") as String
                        info.choume = component("sublocality_level_2", inArray: addressComponents, ofType: "long_name") as String
                        info.banchi = component("sublocality_level_3", inArray: addressComponents, ofType: "long_name") as String
                        info.gou = component("sublocality_level_4", inArray: addressComponents, ofType: "long_name") as String
                        info.postalCode = component("postal_code", inArray: addressComponents, ofType: "long_name") as String
                    }
                    arrayOfAvailabelAddress.append(info)
                }
            }
        }
        return arrayOfAvailabelAddress
    }

    class func parseGoogleLocationData(_ resultDict:NSDictionary) -> (isFound: Bool, addressInfo: kAddressParse) {
        var arrayOfAvailabelAddress: [kAddressParse] = [kAddressParse]()
        if let results = resultDict.value(forKey: "results") as? NSArray {
            for locationObj in results {
                if let locationDict = locationObj as? NSDictionary {
                    let info: kAddressParse = kAddressParse()
                    //formatted address
                    if let address = locationDict.object(forKey: "formattedAddrs") as? String {
                        info.formattedAddress = address
                    }
                    //coordinate
                    if let geometry = locationDict.object(forKey: "geometry") as? NSDictionary {
                        if let location = geometry.object(forKey: "location") as? NSDictionary {
                            if let latidue = location.object(forKey: "lat") {
                                info.latitude = (latidue as AnyObject).doubleValue ?? 0
                            }
                            if let lng = location.object(forKey: "lng") {
                                info.longitude = (lng as AnyObject).doubleValue ?? 0
                            }
                        }
                    }
                    //address component
                    if let addressComponents = locationDict.object(forKey: "address_components") as? [[String : AnyObject]] {
                        for addressComponent in addressComponents.reversed() {
                            let longname = (addressComponent["long_name"] as? String) ?? ""
                            let shortname = (addressComponent["short_name"] as? String) ?? ""
                            if let types = addressComponent["types"] as? [String] {
                                for type in types {
                                    if type == "country" {
                                        info.countryCode = shortname
                                        break
                                    }

                                    if type == "administrative_area_level_1" {
                                        info.perfecture = longname
                                        break
                                    }

                                    if type == "colloquial_area" {
                                        info.city  += longname
                                        break
                                    }

                                    if type == "locality" {
                                        info.city += longname
                                        break
                                    }

                                    if type == "ward" {
                                        info.city += longname
                                        break
                                    }

                                    if type == "sublocality_level_1" {
                                        info.town = longname
                                        break
                                    }

                                    if type == "sublocality_level_2" {
                                        info.choume = longname
                                        break
                                    }

                                    if type == "sublocality_level_3" {
                                        info.banchi = longname
                                        break
                                    }

                                    if type == "sublocality_level_4" {
                                        info.gou = longname
                                        break
                                    }

                                    if type == "postal_code" {
                                        info.postalCode = shortname
                                        break
                                    }
                                }
                            }
                        }
                    }
                    if  info.banchi.length == 0  && info.gou.length > 0 {
                        info.banchi = info.gou
                        info.gou = ""
                    }
                    arrayOfAvailabelAddress.append(info)
                }
            }
        }

        for info in arrayOfAvailabelAddress {
            if (info.isFoundAddress() == true) {
                return (true, info)
            }
        }

        //check for retry
        for info in arrayOfAvailabelAddress {
            if (info.isAvailableForRetry() == true) {
                return (false, info)
            }
        }

        //This case is do not found any address result. So, we do not need retry anymore
        return (false, kAddressParse())
    }

    class func component(_ component: String, inArray: NSArray,ofType: String) -> String {
        for infoDict in inArray {
            if let objDict = infoDict as? NSDictionary {
                if let types = objDict.object(forKey: "types") as? NSArray {
                    for type in types {
                        if  type as? String == component {
                            return (objDict.value(forKey: ofType) as? String) ?? ""
                        }
                    }
                }
            }
        }
        return ""
    }

}

class AddressParser: NSObject {

    var latitude = ""
    var longitude  = ""
    var streetNumber = ""
    var route = ""
    var locality = ""
    var subLocality = ""
    var formattedAddress = ""
    var administrativeArea = ""
    var administrativeAreaCode = ""
    var subAdministrativeArea = ""
    var postalCode = ""
    var country = ""
    var subThoroughfare = ""
    var thoroughfare = ""
    var ISOcountryCode = ""
    var state = ""

    override init(){
        super.init()
    }

    fileprivate func getAddressDictionary()-> NSDictionary {
        let addressDict = NSMutableDictionary()
        addressDict.setValue(latitude, forKey: "latitude")
        addressDict.setValue(longitude, forKey: "longitude")
        addressDict.setValue(streetNumber, forKey: "streetNumber")
        addressDict.setValue(locality, forKey: "locality")
        addressDict.setValue(subLocality, forKey: "subLocality")
        addressDict.setValue(administrativeArea, forKey: "administrativeArea")
        addressDict.setValue(postalCode, forKey: "postalCode")
        addressDict.setValue(country, forKey: "country")
        addressDict.setValue(formattedAddress, forKey: "formattedAddress")
        return addressDict
    }

    fileprivate func parseAppleLocationData(_ placemark:CLPlacemark) {
        let addressLines = placemark.addressDictionary!["FormattedAddressLines"] as! NSArray
        //self.streetNumber = placemark.subThoroughfare ? placemark.subThoroughfare : ""
        self.streetNumber = placemark.thoroughfare ?? ""
        self.locality = placemark.locality ?? ""
        self.postalCode = placemark.postalCode ?? ""
        self.subLocality = placemark.subLocality ?? ""
        self.administrativeArea = placemark.administrativeArea ?? ""
        self.country = placemark.country ?? ""
        self.longitude = placemark.location?.coordinate.longitude.description ?? ""
        self.latitude = placemark.location?.coordinate.latitude.description ?? ""
        if(addressLines.count>0){
            self.formattedAddress = addressLines.componentsJoined(by: ", ")
        }
        else{
            self.formattedAddress = ""
        }
    }

    fileprivate func parseGoogleLocationData(_ resultDict:NSDictionary) {
        if let locationDict = (resultDict.value(forKey: "results") as? NSArray)?.firstObject as? NSDictionary {
            let formattedAddrs = locationDict.object(forKey: "formatted_address") as? String
            let geometry = locationDict.object(forKey: "geometry") as! NSDictionary
            let location = geometry.object(forKey: "location") as! NSDictionary
            let lat = location.object(forKey: "lat") as! Double
            let lng = location.object(forKey: "lng") as! Double
            self.latitude = lat.description
            self.longitude = lng.description
            if let addressComponents = locationDict.object(forKey: "address_components") as? Array<Any> {
                self.subThoroughfare = component("street_number", inArray: addressComponents, ofType: "long_name")
                self.thoroughfare = component("route", inArray: addressComponents, ofType: "long_name")
                self.streetNumber = self.subThoroughfare
                self.locality = component("locality", inArray: addressComponents, ofType: "long_name")
                self.postalCode = component("postal_code", inArray: addressComponents, ofType: "long_name")
                self.route = component("route", inArray: addressComponents, ofType: "long_name")
                self.subLocality = component("subLocality", inArray: addressComponents, ofType: "long_name")
                self.administrativeArea = component("administrative_area_level_1", inArray: addressComponents, ofType: "long_name")
                self.administrativeAreaCode = component("administrative_area_level_1", inArray: addressComponents, ofType: "short_name")
                self.subAdministrativeArea = component("administrative_area_level_2", inArray: addressComponents, ofType: "long_name")
                self.country =  component("country", inArray: addressComponents, ofType: "long_name")
                self.ISOcountryCode =  component("country", inArray: addressComponents, ofType: "short_name")
                self.formattedAddress = formattedAddrs ?? ""
            }
        }
    }

    fileprivate func component(_ component: String,inArray: Array<Any>,ofType: String) -> String {
        let index = inArray.index { (obj) -> Bool in
            let objDict = obj as? NSDictionary
            let types = objDict?.object(forKey: "types") as? [String]
            let type = types?.first ?? ""
            return type.isEqual(component)
            } ?? 0
        if (index == NSNotFound) {
            return ""
        }

        if (index >= inArray.count) {
            return ""
        }

        if let dict = inArray[index] as? [String : Any] {
            if let type = dict[ofType] as? String {
                if type.length > 0 {
                    return type
                }
            }
        }
        return ""
    }

    fileprivate func getPlacemark() -> CLPlacemark {
        var addressDict = [String : Any]()
        let formattedAddressArray = self.formattedAddress.components(separatedBy: ", ") as Array
        let kSubAdministrativeArea = "SubAdministrativeArea"
        let kSubLocality           = "SubLocality"
        let kState                 = "State"
        let kStreet                = "Street"
        let kThoroughfare          = "Thoroughfare"
        let kFormattedAddressLines = "FormattedAddressLines"
        let kSubThoroughfare       = "SubThoroughfare"
        let kPostCodeExtension     = "PostCodeExtension"
        let kCity                  = "City"
        let kZIP                   = "ZIP"
        let kCountry               = "Country"
        let kCountryCode           = "CountryCode"
        addressDict[kSubAdministrativeArea] = self.subAdministrativeArea
        addressDict[kSubLocality] = self.subLocality as NSString
        addressDict[kState] = self.administrativeAreaCode
        addressDict[kStreet] = formattedAddressArray.first! as NSString
        addressDict[kThoroughfare] = self.thoroughfare
        addressDict[kFormattedAddressLines] = formattedAddressArray as AnyObject?
        addressDict[kSubThoroughfare] = self.subThoroughfare
        addressDict[kPostCodeExtension] = "" as AnyObject?
        addressDict[kCity] = self.locality
        addressDict[kZIP] = self.postalCode
        addressDict[kCountry] = self.country
        addressDict[kCountryCode] = self.ISOcountryCode
        let lat = Double(self.latitude) ?? 0
        let lng = Double(self.longitude) ?? 0
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict as [String : AnyObject]?)
        return (placemark as CLPlacemark)
    }
}
