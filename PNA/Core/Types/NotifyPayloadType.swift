//
//  NotifyPayloadType.swift
//  PNA
//
//  Created by hien.bui on 3/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

public enum NotifyPayloadType: String {
    case request = "network-request"
    case accept = "accepted-network-request"
    case decline = "declined-network-request"
}
