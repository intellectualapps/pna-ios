//
//  MembershipType.swift
//  PNA
//
//  Created by bui.duy.hien on 3/13/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

enum MembershipType: String {
    case premium = "premium"
    case regular = "regular"
}
