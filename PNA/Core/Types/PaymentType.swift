//
//  PaymentType.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/13/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

enum PaymentType {
    case discount
    case event
}
