//
//  YellowPageFilterType.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

enum YellowPageFilterType {
    case state
    case category
}
