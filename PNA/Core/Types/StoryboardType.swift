//
//  StoryboardType.swift
//  PNA
//
//  Created by MTQ on 4/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

enum StoryboardType: String {
    case discounts
    case payment
    case yellowPages
    case main
    case login
    case signUp
    case createProfile
    case networkSuggestion
    case slimProfile
    case leftMenu
    case feed
    case network
    case feedDetail
    case profileDetail
    case networkRequest
    case networkInvitation
    case events
    case searchMember
    case updateProfile
    case welcome
    case pnaDaily
    
    var name: String {
        switch self {
        case .discounts:
            return "Discounts"
        case .payment:
            return "Payment"
        case .yellowPages:
            return "YellowPages"
        case .main:
            return "Main"
        case .login:
            return "Login"
        case .signUp:
            return "SignUp"
        case .createProfile:
            return "CreateProfile"
        case .networkSuggestion:
            return "NetworkSuggestion"
        case .slimProfile:
            return "SlimProfile"
        case .leftMenu:
            return "LeftMenu"
        case .feed:
            return "Feed"
        case .network:
            return "Network"
        case .feedDetail:
            return "FeedDetail"
        case .profileDetail:
            return "ProfileDetail"
        case .networkRequest:
            return "NetworkRequest"
        case .networkInvitation:
            return "NetworkInvitation"
        case .events:
            return "Events"
        case .searchMember:
            return "SearchMember"
        case .updateProfile:
            return "UpdateProfile"
        case .welcome:
            return "WelcomeScreen"
        case .pnaDaily:
            return "PNADaily"
        }
    }
}
