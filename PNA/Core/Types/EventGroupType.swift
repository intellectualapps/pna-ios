//
//  EventGroupType.swift
//  PNA
//
//  Created by Quang Ly Hoang on 5/8/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

enum EventGroupType: String {
    case thisWeek = "thisWeek"
    case thisMonth = "thisMonth"
    case upcoming = "upcoming"
}

enum EventPuchaseType: String {
    case ticket = "Ticket"
    case table = "Table"
    case none = "none"
}

enum EventPriceType: String {
    case regular = "Regular"
    case vip = "VIP"
    case none = ""
}
