//
//  YellowPageCategory.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/18/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class YellowPageCategory: Model {
    var id = 0
    var categoryId = 0
    var name = ""
    
    override func parseContentFromDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        categoryId = dict.intForKey(key: "categoryId")
        name = dict.stringOrEmptyForKey(key: "categoryName")
    }
}
