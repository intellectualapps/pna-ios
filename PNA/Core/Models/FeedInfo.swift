//
//  FeedInfo.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class FeedInfo: Model {
    var feedid = 0
    var author = ""
    var feedDescription = ""
    var feedAuthorPicture = ""
    var feedPicUrl = ""
    var content = ""
    var alt_content = ""
    var type = ""
    var likes = 0
    var hasUserLiked = false
    var feedDateString = ""
    var feedDate: Date?
    var height: CGFloat = 0
    var imageHeight: CGFloat = 0
    var isViewHorizontalLine: Bool = true
    
    override func parseContentFromDict(dict: [String : Any]) {
        feedid = dict.intForKey(key: "feedId")
        author = dict.stringOrEmptyForKey(key: "author")
        feedDescription = dict.stringOrEmptyForKey(key: "description")
        feedAuthorPicture = dict.stringOrEmptyForKey(key: "feedAuthorPicture")
        content = dict.stringOrEmptyForKey(key: "content")
        alt_content = dict.stringOrEmptyForKey(key: "altContent") // XXX Resharing content, need to handle later phase
        let trimmedContent = content.trim()
        if trimmedContent.lenght > 0 {
            if (trimmedContent.lowercased() as NSString).range(of:"http://res.cloudinary.com").location != NSNotFound {
                feedPicUrl = trimmedContent
                content = alt_content
            }
            else {
                content = trimmedContent
            }
        }
        
        type = dict.stringOrEmptyForKey(key: "type")
        likes = dict.intForKey(key: "likes")
        hasUserLiked = dict.boolForKey(key: "hasUserLiked")
        feedDateString = dict.stringOrEmptyForKey(key: "feedDate")
        feedDate = feedDateString.getDateUTCFromString(format: "YYYY-MM-dd HH:mm:ss")
        calculate()
    }

    fileprivate func calculate() {
        let defaulfHeight: CGFloat = 136
        let screenwidth = UIScreen.main.bounds.width
        var totalHeight = defaulfHeight
        //content text
        let boundSize = CGSize(width: screenwidth - 30, height: CGFloat.greatestFiniteMagnitude)
        let size = content.sizeWithBoundSize(boundSize: boundSize, option: .usesLineFragmentOrigin, lineBreakMode: .byWordWrapping, font: UIFont.systemFont(ofSize: 14))
        totalHeight += size.height
        // Need to handle alt_content later XXX
        self.height = totalHeight
    }
}
