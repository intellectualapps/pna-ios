//
//  SearchProfile.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/15/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SearchProfile: User {
    
    override func parseContentFromDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        email = dict.stringForKey(key: "email")
        firstName = dict.stringOrEmptyForKey(key: "fname")
        lastName = dict.stringForKey(key: "lname")
        jobTitle = dict.stringOrEmptyForKey(key: "jobTitle")
        company = dict.stringOrEmptyForKey(key: "company")
        membershipType = dict.stringForKey(key: "networkState")
        url = dict.stringForKey(key: "url")
        pic = dict.stringForKey(key: "pic")
    }
}
