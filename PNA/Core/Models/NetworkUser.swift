//
//  NetworkUser.swift
//  PNA
//
//  Created by center12 on 2/21/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class NetworkUser: User {
    
    override func parseContentFromDict(dict: [String : Any]) {
        networkId = dict.intForKey(key: "networkId")
        memberId = dict.intForKey(key: "memberId")
        firstName = dict.stringOrEmptyForKey(key: "firstName")
        lastName = dict.stringOrEmptyForKey(key: "lastName")
        jobTitle = dict.stringOrEmptyForKey(key: "jobTitle")
        company = dict.stringOrEmptyForKey(key: "company")
        industry = dict.intForKey(key: "industry")
        state = dict.intForKey(key: "state")
        pic = dict.stringOrEmptyForKey(key: "profilePhotoUrl")
        hasAccepted = dict.boolForKey(key: "hasAccepted")
    }
}
