//
//  SlimProfile.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SlimProfile: User {
    
    override func parseContentFromDict(dict: [String : Any]) {
        let name = dict.stringOrEmptyForKey(key: "fullName")
        if name.length > 0 {
            var array = name.components(separatedBy: " ")
            if array.count > 0 {
                firstName = array.removeFirst()
                lastName = array.joined(separator: " ")
            }
        }
        fullName = name
        pic = dict.stringForKey(key: "profilePhoto")
        jobTitle = dict.stringOrEmptyForKey(key: "jobTitle")
        company = dict.stringOrEmptyForKey(key: "company")
        country = dict.intForKey(key: "country")
        industry = dict.intForKey(key: "industry")
        state = dict.intForKey(key: "state")
    }
}
