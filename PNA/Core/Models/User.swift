//
//  User.swift
//  PNA
//
//  Created by hien.bui on 2/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

class User: Model, NSCoding {
    var authToken:String?
    var email:String?
    var firstName:String?
    var lastName:String?
    var fullName:String?
    var phoneNumber:String?
    var jobTitle:String?
    var company:String?
    var industry:Int?
    var state:Int?
    var country:Int?
    var id: Int?
    var address: String?
    var date: Date?
    var facebook: String?
    var instagram: String?
    var joined: Date?
    var lastActive: Date?
    var membershipType: String?
    var picId: Int?
    var pic: String?
    var picLikes: Int?
    var privacy: String?
    var sex: String?
    var skills: String?
    var experience: [[String : Any]] = []
    var status: String?
    var twitter: String?
    var url: String?
    var views: Int?
    var website: String?
    var networkId = 0
    var memberId = 0
    var hasAccepted = false
    var networkStatus: String?
    var profilePics: String?
    var phone: String?
    var token: String {
        return authToken ?? ""
    }
    var flname: String {
        var fName = ""
        if let _firstName = firstName {
            fName = _firstName
        }
        if let _lastName = lastName {
            fName += " "
            fName += _lastName
        }
        return fName
    }
    
    override init () {
        super.init()
    }

    func alreadyCreateProfile() -> Bool {
        guard let jobTitle = self.jobTitle else {
            return false
        }
        if jobTitle.count > 0 {
            return true
        }
        return false
    }
    
    override func parseContentFromDict(dict: [String : Any]) {
        self.email = dict.stringOrEmptyForKey(key: "email")
        self.firstName = dict.stringOrEmptyForKey(key: "fname")
        self.lastName = dict.stringOrEmptyForKey(key: "lname")
        self.fullName = dict.stringOrEmptyForKey(key: "flname")
        self.phoneNumber = dict.stringOrEmptyForKey(key: "phone")
        self.jobTitle = dict.stringOrEmptyForKey(key: "jobTitle")
        self.company = dict.stringOrEmptyForKey(key: "company")
        self.state = dict.intForKey(key: "state")
        self.industry = dict.intForKey(key: "industry")
        self.country = dict.intForKey(key: "country")
        self.id = dict.intForKey(key: "id")
        self.pic = dict.stringForKey(key: "pic")
        self.membershipType = dict.stringOrEmptyForKey(key: "membershipType")
        self.skills = dict.stringForKey(key: "skills")
        if let expDicts = dict["experience"] as? [[String : Any]] {
            for expDict in expDicts {
                let main = "\(expDict.stringForKey(key: "position") ?? "") @ \(expDict.stringForKey(key: "employer") ?? "")"
                let sub = "\(expDict.stringForKey(key: "location") ?? "") | \(expDict.stringForKey(key: "period") ?? "")"
                var tempDict: [String : Any] = [:]
                tempDict["main"] = main
                tempDict["sub"] = sub
                experience.append(tempDict)
            }
        }
        self.twitter = dict.stringForKey(key: "twitter")
        self.instagram = dict.stringForKey(key: "instagram")
        self.facebook = dict.stringForKey(key: "facebook")
        self.website = dict.stringForKey(key: "website")
    }

    class func convert(userJson: [String: Any]?) -> User {
        let user  = User()
        if let userData = userJson?["member"] as? [String: Any] {
            user.parseContentFromDict(dict: userData)
        }
        return user
    }
    
    func encode(with aCoder: NSCoder){
        aCoder.encode(authToken, forKey: "authToken")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        aCoder.encode(jobTitle, forKey: "jobTitle")
        aCoder.encode(company, forKey: "company")
        aCoder.encode(state, forKey: "state")
        aCoder.encode(industry, forKey: "industry")
        aCoder.encode(country?.toString() ?? "", forKey: "country")
        aCoder.encode(pic, forKey: "pic")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(membershipType, forKey: "membershipType")
        aCoder.encode(skills, forKey: "skills")
        aCoder.encode(experience, forKey: "experience")
        aCoder.encode(twitter, forKey: "twitter")
        aCoder.encode(facebook, forKey: "facebook")
        aCoder.encode(instagram, forKey: "instagram")
        aCoder.encode(website, forKey: "website")
    }
    
    required init(coder aDecoder: NSCoder){
        self.authToken = aDecoder.decodeObject(forKey: "authToken") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        self.lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        self.fullName = aDecoder.decodeObject(forKey: "fullName") as? String
        self.phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String
        self.jobTitle = aDecoder.decodeObject(forKey: "jobTitle") as? String
        self.company = aDecoder.decodeObject(forKey: "company") as? String
        self.country = Int((aDecoder.decodeObject(forKey: "country") as? String) ?? "0" ) ?? 0
        self.state = aDecoder.decodeObject(forKey: "state") as? Int
        self.industry = aDecoder.decodeObject(forKey: "industry") as? Int
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.pic = aDecoder.decodeObject(forKey: "pic") as? String
        self.membershipType = aDecoder.decodeObject(forKey: "membershipType") as? String
        self.skills = aDecoder.decodeObject(forKey: "skills") as? String
        self.experience = aDecoder.decodeObject(forKey: "experience") as! [[String : Any]]
        self.twitter = aDecoder.decodeObject(forKey: "twitter") as? String
        self.facebook = aDecoder.decodeObject(forKey: "facebook") as? String
        self.instagram = aDecoder.decodeObject(forKey: "instagram") as? String
        self.website = aDecoder.decodeObject(forKey: "website") as? String
    }
    
    //load default user
    class func archivedUser() -> User? {
        if let pData = UserDefaults.standard.object(forKey: "user") as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: pData) as? User {
                return user
            }
        }
        return nil
    }
}
