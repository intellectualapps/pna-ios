//
//  InterestInfo.swift
//  PNA
//
//  Created by Nguyen Van Dung on 3/15/19.
//  Copyright © 2019 Dht. All rights reserved.
//

import Foundation

class InterestInfo: Model {
    var id = 0
    var image = ""
    var industry = ""
    var isSelected = false

    override func parseContentFromDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        image = dict.stringOrEmptyForKey(key: "image_url")
        industry = dict.stringOrEmptyForKey(key: "industry")
    }
}
