//
//  SuggestionUser.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SuggestionUser: Model {
    var id = 0
    var email = ""
    var fname = ""
    var lname = ""
    var jobTitle = ""
    var company = ""
    var url = ""
    var pic = ""
    var isAdded = false
    var avatarPath: String {
        return (url as NSString).appendingPathComponent(pic)
    }

    var fullname: String {
        return fname + " " + lname
    }

    override func parseContentFromDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        email = dict.stringOrEmptyForKey(key: "email")
        fname = dict.stringOrEmptyForKey(key: "fname")
        lname = dict.stringOrEmptyForKey(key: "lname")
        jobTitle = dict.stringOrEmptyForKey(key: "jobTitle")
        company = dict.stringOrEmptyForKey(key: "company")
        url = dict.stringOrEmptyForKey(key: "url")
        pic = dict.stringOrEmptyForKey(key: "pic")
    }
}
