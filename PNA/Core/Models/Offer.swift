//
//  Offer.swift
//  PNA
//
//  Created by MTQ on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

struct Offer {
    var id: String?
    var title: String?
    var businessName: String?
    var address: String?
    var type: String?
    var photo: String?
    var details: String?
    var status: String?
    var offerLevel: String?
    var createdAt: String?
    var startsAt: String?
    var stopsAt: String?
    var numberOfTimesRedeemed: Int?
    var imageHeight: CGFloat = 200.0
    var imageWelcomeUrl: String = ""
    var indexPath: IndexPath?
    var hasContent = false
}

extension Offer: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id    <- map["id"]
        title    <- map["title"]
        businessName    <- map["businessName"]
        address    <- map["address"]
        type    <- map["type"]
        photo    <- map["photo"]
        details    <- map["details"]
        status    <- map["status"]
        offerLevel    <- map["offerLevel"]
        createdAt    <- map["createdAt"]
        startsAt    <- map["startsAt"]
        stopsAt    <- map["stopsAt"]
        numberOfTimesRedeemed <- map["numberOfTimesRedeemed"]
        imageWelcomeUrl <- map["imageUrl"]
        hasContent = true
    }
}
