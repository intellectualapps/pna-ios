//
//  PNADaily.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

struct PNADaily {
    var title: String?
    var imageUrl: String?
    var contentUrl: String?
    var hasContent: Bool = false
}

extension PNADaily: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        title    <- map["title"]
        imageUrl <- map["imageUrl"]
        contentUrl <- map["contentUrl"]
        hasContent = true
    }
}
