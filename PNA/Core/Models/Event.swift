//
//  Event.swift
//  PNA
//
//  Created by Quang Ly Hoang on 3/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

final class Event {
    var id: String?
    var title: String?
    var address: String?
    var parseDate: String?
    var date: String?
    var time: String?
    var venue: String?
    var organizer: String?
    var picURL: String?
    var dressCode: String?
    var info: String?
    var hasRSVP: Bool = true
    var numberOfRSVPs: Int = 0
    var imageHeight: CGFloat = 200.0
    var imageWelcomeUrl: String = ""
    var eventGroup: String = ""
    var indexPath: IndexPath?
    var eventType: String = ""
    var isPaidEvent: Bool = false
    var hasTableReservation: Bool = false
    var hasReservedTable: Bool = false
    var tickets: Dictionary<String, String>?
    var tables: Dictionary<String, String>?
    var hasContent = false
}

extension Event: Mappable {
    convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        address <- map["address"]
        parseDate <- map["parsedDate"]
        date <- map["date"]
        time <- map["time"]
        venue <- map["venue"]
        organizer <- map["organizer"]
        picURL <- map["picUrl"]
        dressCode <- map["dressCode"]
        info <- map["info"]
        hasRSVP <- map["hasRSVP"]
        numberOfRSVPs <- map["numberOfRSVPs"]
        imageWelcomeUrl <- map["imageUrl"]
        eventGroup <- map["eventGroup"]
        isPaidEvent <- map["isPaidEvent"]
        hasTableReservation <- map["hasTableReservation"]
        hasReservedTable <- map["hasReservedTable"]
        tickets <- map["tickets"]
        tables <- map["tables"]
        eventType <- map["eventType"]
        hasContent = true
    }
}
