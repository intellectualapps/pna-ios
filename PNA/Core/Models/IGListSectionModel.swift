//
//  IGListSectionModel.swift
//  PNA
//
//  Created by Nguyen Van Dung on 1/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import IGListKit

class IGListSectionModel: NSObject, ListDiffable {
    var contents: [Any] = []
    var itemCount: Int {
        get {
            return contents.count
        }
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }

    func diffIdentifier() -> NSObjectProtocol {
        return self
    }
}
