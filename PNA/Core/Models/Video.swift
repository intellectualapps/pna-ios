//
//  Video.swift
//  PNA
//
//  Created by Quang Ly Hoang on 4/28/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

struct Video {
    var title: String?
    var videoUrl: String?
    var hasContent = false
    var imageUrl: String {
        return getYoutubeThumb(url: videoUrl ?? "")
    }
    
    private func getYoutubeThumb(url: String) -> String {
        guard let id = getYoutubeId(url: url) else { return ""}
        return "http://img.youtube.com/vi/\(id)/0.jpg"
    }
    
    private func getYoutubeId(url: String) -> String? {
        return URLComponents(string: url)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
}

extension Video: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        title    <- map["title"]
        videoUrl <- map["videoUrl"]
        hasContent = true
    }
}
