//
//  WelcomeObject.swift
//  PNA
//
//  Created by QuangLH on 7/19/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

struct WelcomeObject {
    var pnaDaily: PNADaily
    var event: Event
    var offer: Offer
    var video: Video
    var welcomeUser: [WelcomeUser]
}
