//
//  Transaction.swift
//  PNA
//
//  Created by MTQ on 3/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

struct Transaction {
    var status: Bool?
    var accessCode: String?
    var referenceCode: String?
    var responseMessage: String?
}

extension Transaction: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        status    <- map["status"]
        accessCode    <- map["accessCode"]
        referenceCode    <- map["referenceCode"]
        responseMessage <- map["responseMessage"]
    }
}
