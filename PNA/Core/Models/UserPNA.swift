//
//  UserPNA.swift
//  PNA
//
//  Created by nguyen.van.dungb on 2/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class UserPNA: Mappable {
    var identifier: String {
        return email ?? ""
    }
    var username: String?
    var avatar: String?
    var info: String?
    var email: String?
    var jobTitle: String?
    var company: String?
    var industry: String?
    var country: String?
    var state: String?
    var firstName: String?
    var lastName: String?
    var isAdded: Bool = false
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        username    <- map["username"]
        avatar      <- map["avatar"]
        info      <- map["info"]
        email <- map["email"]
        jobTitle <- map["jobTitle"]
        company <- map["company"]
        industry <- map["industry"]
        country <- map["country"]
        state <- map["state"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        isAdded <- map["isAdded"]
    }
}

