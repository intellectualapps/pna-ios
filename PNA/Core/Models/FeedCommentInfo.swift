//
//  FeedCommentInfo.swift
//  PNA
//
//  Created by Nguyen Van Dung on 2/7/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class FeedCommentInfo: Model {
    var feedId = 0
    var commentId = 0
    var userId = 0
    var author = ""
    var commentAuthorPicture = ""
    var content = ""
    var commentDateString = ""
    var commentDate: Date?
    var height: CGFloat = 0

    override func parseContentFromDict(dict: [String : Any]) {
        commentId = dict.intForKey(key: "commentId")
        userId = dict.intForKey(key: "userId")
        author = dict.stringOrEmptyForKey(key: "author")
        commentAuthorPicture  = dict.stringOrEmptyForKey(key: "commentAuthorPicture")
        content = dict.stringOrEmptyForKey(key: "content")
        commentDateString = dict.stringOrEmptyForKey(key: "commentDate")
        commentDate = commentDateString.getDateUTCFromString(format: "YYYY-MM-dd HH:mm:ss")
        calculate()
    }

    fileprivate func calculate() {
        let defaulfHeight: CGFloat = 77
        let screenwidth = UIScreen.main.bounds.width
        var totalHeight = defaulfHeight
        //content text
        let boundSize = CGSize(width: screenwidth - 85, height: CGFloat.greatestFiniteMagnitude)
        let size = content.sizeWithBoundSize(boundSize: boundSize, option: .usesLineFragmentOrigin, lineBreakMode: .byWordWrapping, font: UIFont.systemFont(ofSize: 14))
        totalHeight += size.height
        self.height = totalHeight
    }
}
