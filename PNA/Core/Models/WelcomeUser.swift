//
//  WelcomeUser.swift
//  PNA
//
//  Created by Quang Ly Hoang on 7/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class WelcomeUser: User {
    
    override func parseContentFromDict(dict: [String : Any]) {
        memberId = dict.intForKey(key: "memberId")
        networkStatus = dict.stringOrEmptyForKey(key: "networkState")
        pic = dict.stringOrEmptyForKey(key: "imageUrl")
    }
}
