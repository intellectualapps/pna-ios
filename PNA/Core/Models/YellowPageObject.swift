//
//  YellowPageObject.swift
//  PNA
//
//  Created by hien.bui on 3/15/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

struct YellowPageObject {
    var id = 0
    var businessLogo = ""
    var businessName = ""
    var businessAddress = ""
    var businessCategory = ""
    var businessEmail = ""
    var businessWebsite = ""
    var businessOffers: [String]?
    var businessPhone = ""
    var businessState = ""

    var height: CGFloat = 0

    mutating func calculate() {
        let txt = attributeText().string
        let font = UIFont.fireSansBold(size: 15)
        let boundSize = CGSize(width: UIScreen.main.bounds.width - 114, height: CGFloat.greatestFiniteMagnitude)
        let size = txt.sizeWithBoundSize(boundSize: boundSize, option: NSStringDrawingOptions.usesLineFragmentOrigin, lineBreakMode: NSLineBreakMode.byWordWrapping, font: font)
        let totalHeight =  size.height + 20
        height = max(75, totalHeight)
    }

    func attributeText() -> NSAttributedString {
        let mutableAttri = NSMutableAttributedString()
        if self.businessName.lenght > 0 {
            let font = UIFont.fireSansBold(size: 18)
            let bnameAttr = self.businessName.attributeText(textColor: .black, font: font)
            mutableAttri.append(bnameAttr)
        }
        if self.businessAddress.lenght > 0 {
            let txt = "\n" + self.businessAddress
            let font = UIFont.fireSansRegular(size: 15)
            let bnameAttr = txt.attributeText(textColor: .black, font: font)
            mutableAttri.append(bnameAttr)
        }
        if self.businessCategory.lenght > 0 {
            let txt = "\n" + self.businessCategory
            let font = UIFont.fireSansRegular(size: 15)
            let bnameAttr = txt.attributeText(textColor: .black, font: font)
            mutableAttri.append(bnameAttr)
        }
        return mutableAttri
    }
}

extension YellowPageObject: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        businessLogo <- map["businessLogo"]
        businessName <- map["businessName"]
        businessAddress <- map["businessAddress"]

        businessCategory <- map["businessCategory"]
        businessPhone <- map["businessPhone"]
        businessEmail <- map["businessEmail"]
        businessWebsite <- map["businessWebsite"]
        businessOffers <- map["businessOffers"]
        businessState <- map["businessState"]
        calculate()
    }
}
