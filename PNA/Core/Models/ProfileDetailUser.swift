//
//  ProfileDetailUser.swift
//  PNA
//
//  Created by center12 on 2/23/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class ProfileDetailUser: User {
    
    override func parseContentFromDict(dict: [String : Any]) {
        id = dict.intForKey(key: "id")
        email = dict.stringOrEmptyForKey(key: "email")
        firstName = dict.stringOrEmptyForKey(key: "fname")
        lastName = dict.stringOrEmptyForKey(key: "lname")
        fullName = dict.stringOrEmptyForKey(key: "flname")
        phone = dict.stringOrEmptyForKey(key: "phone")
        sex = dict.stringOrEmptyForKey(key: "sex")
        address = dict.stringOrEmptyForKey(key: "address")
        state = dict.intForKey(key: "state")
        country = dict.intForKey(key: "country")
        url = dict.stringOrEmptyForKey(key: "url")
        pic = dict.stringOrEmptyForKey(key: "pic")
        picId = dict.intForKey(key: "picId")
        picLikes = dict.intForKey(key: "picLikes")
        jobTitle = dict.stringOrEmptyForKey(key: "jobTitle")
        company = dict.stringOrEmptyForKey(key: "company")
        industry = dict.intForKey(key: "industry")
        skills = dict.stringOrEmptyForKey(key: "skills")
        website = dict.stringOrEmptyForKey(key: "website")
        facebook = dict.stringOrEmptyForKey(key: "facebook")
        twitter = dict.stringOrEmptyForKey(key: "twitter")
        instagram = dict.stringOrEmptyForKey(key: "instagram")
        views = dict.intForKey(key: "views")
        let lastActiveString = dict.stringOrEmptyForKey(key: "lastActive")
        if let utcDate = lastActiveString.getDateUTCFromString(format: "yyyy-MM-dd'T'HH:mm:ss") {
            lastActive = utcDate
        }
        status = dict.stringOrEmptyForKey(key: "status")
        privacy = dict.stringOrEmptyForKey(key: "privacy")
        let dateString = dict.stringOrEmptyForKey(key: "date")
        if let utcDate = dateString.getDateUTCFromString(format: "yyyy-MM-dd'T'HH:mm:ss") {
            date = utcDate
        }
        let joinedString = dict.stringOrEmptyForKey(key: "joined")
        if let utcDate = joinedString.getDateUTCFromString(format: "yyyy-MM-dd'T'HH:mm:ss") {
            joined = utcDate
        }
        profilePics = dict.stringOrEmptyForKey(key: "profilePics")
        if let expDicts = dict["experience"] as? [[String : Any]] {
            for expDict in expDicts {
                let main = "\(expDict.stringForKey(key: "position") ?? "") @ \(expDict.stringForKey(key: "employer") ?? "")"
                let sub = "\(expDict.stringForKey(key: "location") ?? "") | \(expDict.stringForKey(key: "period") ?? "")"
                var tempDict: [String : Any] = [:]
                tempDict["main"] = main
                tempDict["sub"] = sub
                experience.append(tempDict)
            }
        }
        networkStatus = dict.stringOrEmptyForKey(key: "networkStatus")
    }
}

