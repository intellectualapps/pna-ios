//
//  ViewController.swift
//  Realm
//
//  Created by Nguyen Van Dung on 9/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    fileprivate var model: LoginViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        model = LoginViewModel()
        usingRealmApiService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func usingRealmApiService() {
    }

    /*
     * User: is realm object. So, if you want to store to database only need call add
     * If User is not realm object => you can mapping to realm model object and save
     */
    func didLoginSuccess(listUsers: [User]) {
        //Note: This is example save. You need check logic existed or not first ...
        guard let realm = RealmControl.shared.realm else {
            return
        }
        do {
            try realm.write({
                realm.add(listUsers)
            })
        } catch {

        }
    }
}

